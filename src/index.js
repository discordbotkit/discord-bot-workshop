const { app, BrowserWindow, webContents, session, ipcMain, dialog, shell } = require('electron');
const path = require('path');
const RPC = require("discord-rich-presence")("802949058065989663");
const child_process = require("child_process");
const fs = require('fs');
const fse = require('fs-extra');
let botpath;
let child;
app.disableHardwareAcceleration()
// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}
let mainWindow;
const createWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({

    titleBarStyle:'hidden',
    icon: path.join(__dirname, 'DBW.ico'),
    width: 1400,
    height: 720,
    minWidth: 1400,
    minHeight: 740,
    backgroundColor: '#222',
    show:false,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation:false,
    },
  });
  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, 'index.html'));
  mainWindow.webContents.on("devtools-opened", () => {  mainWindow.webContents.closeDevTools(); });
  mainWindow.setMenu(null);
if (fs.existsSync("./path.txt")) {
  botpath = fs.readFileSync('./path.txt')
  if(fs.existsSync(`${botpath}\\Workroom\\Settings\\name.txt`)){
    updatePresence(`Working on: ${fs.readFileSync(`${botpath}\\Workroom\\Settings\\name.txt`)}`)
    }else{
      updatePresence(`Working on: unnamed`)
    }
}else{
  updatePresence(`Making a new bot!`)
}
mainWindow.once('ready-to-show', () => {
  mainWindow.show()
})
mainWindow.on('closed',()=>{
  if(child){
    child.send({status:'OFFLINE'})
  }
})
mainWindow.webContents.on('render-process-gone', async function (event, detailed) {
  reportError(JSON.stringify(detailed))
})
mainWindow.webContents.on("new-window", function (event, url) {
  event.preventDefault()
  shell.openExternal(url)
})
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);
// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.


ipcMain.handle('chooseloc', () => {
  
  dialog.showOpenDialog({properties: ['openDirectory'] }).then(async function (response) {
    if (!response.canceled) {
    
  try {
    if(!fs.existsSync(response.filePaths[0] + "\\Workroom"))
    {
      botpath = response.filePaths[0];
      updatePresence(`Making a new bot!`)
     await mainWindow.webContents.send('display', 'Creating bot...')
     try{
  await  fse.copy("./Workroom", response.filePaths[0]);
    fs.mkdirSync(`${response.filePaths[0]}\\Workroom\\Settings`);
  await fse.copy("./UserData",`${response.filePaths[0]}\\Workroom\\UserData`);
  await fse.copy("./Shandler",`${response.filePaths[0]}\\Workroom\\Shandler`);
  await fse.copy("./Music",`${response.filePaths[0]}\\Workroom\\Music`);
await updateModules(response.filePaths[0])
     }catch(e){     
      reportError(e)
      await  mainWindow.webContents.send('display1', " ")
     }
  fs.writeFileSync('./path.txt', response.filePaths[0]);
    }else if(fs.existsSync(response.filePaths[0] + "\\Workroom")){
      botpath = response.filePaths[0];
      fs.writeFileSync('./path.txt', response.filePaths[0]);
      if(fs.existsSync(`${response.filePaths[0]}\\Workroom\\Settings\\name.txt`)){
      updatePresence(`Working on: ${fs.readFileSync(`${response.filePaths[0]}\\Workroom\\Settings\\name.txt`)}`)
      }else{
        updatePresence(`Working on: unnamed`)
      }
      if(!fs.existsSync(`${response.filePaths[0]}\\Workroom\\UserData`)){
      fs.mkdirSync(`${response.filePaths[0]}\\Workroom\\UserData`);
      }
      if(!fs.existsSync(`${response.filePaths[0]}\\Workroom\\Shandler`)){
        fs.mkdirSync(`${response.filePaths[0]}\\Workroom\\Shandler`);
        }
      if(!fs.existsSync(`${response.filePaths[0]}\\Workroom\\Music`)){
        fs.mkdirSync(`${response.filePaths[0]}\\Workroom\\Music`);
        }
      await mainWindow.webContents.send('display', 'Loading bot...')
      try{
      await fse.unlink(`${response.filePaths[0]}\\Workroom\\package-lock.json`);
      await fse.emptyDir(`${response.filePaths[0]}\\Workroom\\UserData`)
      await fse.copy("./UserData",`${response.filePaths[0]}\\Workroom\\UserData`);
      await fse.emptyDir(`${response.filePaths[0]}\\Workroom\\Shandler`)
      await fse.copy("./Shandler",`${response.filePaths[0]}\\Workroom\\Shandler`);
      await fse.emptyDir(`${response.filePaths[0]}\\Workroom\\Music`)
      await fse.copy("./Music",`${response.filePaths[0]}\\Workroom\\Music`);
     await fse.copy("./Workroom\\Workroom\\package.json",`${response.filePaths[0]}\\Workroom\\package.json`);
     await fse.copy("./Workroom\\Workroom\\bot.js",`${response.filePaths[0]}\\Workroom\\bot.js`)
     await fse.copy("./Workroom\\Workroom\\Functions.js",`${response.filePaths[0]}\\Workroom\\Functions.js`)
     await fse.copy("./Workroom\\Workroom\\handlers",`${response.filePaths[0]}\\Workroom\\handlers`)
     await fse.copy("./Workroom\\Workroom\\index.js",`${response.filePaths[0]}\\Workroom\\index.js`)
   await updateModules(response.filePaths[0])
      }catch(e){
        reportError(e)
        await  mainWindow.webContents.send('display1', " ")
      }
  }
  } catch (error) {
    reportError(error)
  }
    }
});
});
process.on('uncaughtException', function (err) {
console.log(err);fs.writeFileSync(`${botpath}\\Workroom\\exception.txt`,err.message)
})
ipcMain.handle('clearCache',()=>{
  session.defaultSession.clearCache()
})
ipcMain.handle('runbot', async () => {
  if(fs.existsSync("./path.txt")){
    if(!child){
      let options = {}
      options.stdio = options.silent ? ['pipe','pipe','pipe','ipc']:[0,1,2,'ipc']
      options.cwd = fs.readFileSync("./path.txt")+"\\Workroom\\"
      child = child_process.spawn("./node_modules\\node\\bin\\node.exe", ['index.js'],options);
      child.on('message', data=>{
        if(data.err){
          mainWindow.webContents.send('alert', data.err)
        }else{
          mainWindow.webContents.send('posterr', data)
        }
      })
    }else{
      mainWindow.webContents.send('botrunning',null) 
    }
  }
  
})
ipcMain.handle('stopbot', async () => {
  if(fs.existsSync("./path.txt")){
    if(child){
      child.send({status:'OFFLINE'})
      child = null
    }else{
      mainWindow.webContents.send('botstopped',null) 
    }
  }
  
})
ipcMain.handle('update', async () => {
    if(!child){
      mainWindow.webContents.send('update',{status:"Stopped", color:"rgb(200, 0, 0)"})
    }else{
      mainWindow.webContents.send('update',{status:"Running", color:"rgb(0, 200, 0)"})
    }
})
ipcMain.on("updatePresence", (event, data) => {
  updatePresence(data);
});
function updatePresence(details) {
  RPC.updatePresence({
    details: details,
    startTimestamp: Date.now(),
    largeImageKey: "dbw1",
    instance: true,
    largeImageText : "Discord Bot Workshop"
  });
}
async function updateModules(path){
  if(!fs.existsSync(`${path}\\Workroom\\slashcommands`)){
    await fs.mkdirSync(`${path}\\Workroom\\slashcommands`)
  }
  if(!fs.existsSync(`${path}\\Workroom\\components`)){
    await fs.mkdirSync(`${path}\\Workroom\\components`)
  }
  if(!fs.existsSync(`${path}\\Workroom\\contextmenus`)){
    await fs.mkdirSync(`${path}\\Workroom\\contextmenus`)
  }
  if(!fs.existsSync(`${path}\\Workroom\\contextmenususer`)){
    await fs.mkdirSync(`${path}\\Workroom\\contextmenususer`)
  }
  new Promise(async (resolve,reject)=>{
    await child_process.exec("npm cache clean --force", {cwd:`${path}\\Workroom`},async (err, stdout, stderr)=>{
      if(err){
        reportError(err)
        await  mainWindow.webContents.send('display1', " ")
        resolve()
      }else{
        await child_process.exec("npm i", {cwd:`${path}\\Workroom`},async (err, stdout, stderr)=>{
          if(err){
            reportError(err)
            fse.copy("./Workroom\\Workroom\\node_modules",`${path}\\Workroom\\node_modules`,async ()=>{
              resolve()
              await  mainWindow.webContents.send('display1', " ")
            })
          }else{
            resolve()
            await  mainWindow.webContents.send('display1', " ")
          }
        });
      }
  })
})
}
function reportError(err){
  fs.writeFileSync(`${botpath}\\Workroom\\crash.txt`,err)
}

const fs = require('fs');

appendFileSync = (file, text)=>{
    if(global.commandsdata.find(c=>c.name==file)){
        global.commandsdata.find(c=>c.name==file).data += text;
    }else{
        writeFileSync(file,text)
    }
}
writeFileSync = (file, text)=>{
    if(global.commandsdata.find(c=>c.name==file)){
        global.commandsdata[global.commandsdata.indexOf(global.commandsdata.find(c=>c.name==file))] = {name:file,data:text}
    }else{
        global.commandsdata.push({name:file,data:text})
    }
}
(function(global) {
    let LiteGraph = global.LiteGraph;

    function trycatch(){
        this.addOutput("try", LiteGraph.EVENT);
        this.addOutput("catch", LiteGraph.EVENT);
        this.addProperty("Save error to variable", "err");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    trycatch.title = "Try Catch";
    trycatch.prototype.onAction = function()
    {   
            this.setOutputData(0, this.getInputData(0));  
            this.setOutputData(1, this.getInputData(0));  


  
            if(this.getOutputData(0)){
                
               LiteGraph.save_var(this.properties["Save error to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'error',[{name:"name",value:"name"},{name:"message",value:"message"},{name:"stack",value:"stack"}])
            appendFileSync(this.getInputData(0),`\ntry{`);
            this.trigger("try");
            appendFileSync(this.getInputData(0),`\n}`);
            appendFileSync(this.getInputData(0),`\ncatch(e){
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save error to variable"].replaceAll("${","").replaceAll("}","").replaceAll("global.","")} = e;`);
            this.trigger("catch");
            appendFileSync(this.getInputData(0),`\n}`);
            }
    }
    LiteGraph.registerNodeType("Bot Action/Try Catch", trycatch );

    function setstatus()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Status", "");
        this.addProperty("Type", "<option selected='selected'>Playing</option><option>Streaming</option><option>Listening</option><option>Watching</option><option>Competing</option>","dropdown");
        this.addProperty("URL(only for STREAMING)", "");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
    }
    
    //name to show
    setstatus.title = "Set Status";
    
    //function to call when the node is executed
    setstatus.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



           
            if(this.getOutputData(0)){
            
            
            if(LiteGraph.parse(this.properties["URL(only for STREAMING)"].trim())){
            appendFileSync(this.getInputData(0),`\nawait client.user.setActivity(`+"`"+LiteGraph.parse(this.properties["Status"])+"`"+`, { type: `+"String(`"+`${LiteGraph.parse_dropdown(this.properties["Type"])}`+"`).trim().toUpperCase()"+`, url: `+"`"+LiteGraph.parse(this.properties["URL(only for STREAMING)"])+"`"+` });`);   
            }else{
                appendFileSync(this.getInputData(0),`\nawait client.user.setActivity(`+"`"+LiteGraph.parse(this.properties["Status"])+"`"+`, { type: `+"String(`"+`${LiteGraph.parse_dropdown(this.properties["Type"])}`+"`).trim().toUpperCase()"+` });`);
            }
        }
            this.trigger("Action");
 
    }
    
    //register in the system
    LiteGraph.registerNodeType("Bot Action/Set Status", setstatus );


    function setstatust()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Type", "<option selected='selected'>Online</option><option>Invisible</option><option>Idle</option><option>Do Not Disturb</option>","dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
    }
    
    //name to show
    setstatust.title = "Set Status Type";
    
    //function to call when the node is executed
    setstatust.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



           
            if(this.getOutputData(0)){
            
            
            appendFileSync(this.getInputData(0),`\nawait client.user.setStatus(String(\`${LiteGraph.parse_dropdown(this.properties["Type"]).replace("Do Not Disturb","dnd")}\`).toLowerCase())`);   
        }
            this.trigger("Action");
 
    }
    
    //register in the system
    LiteGraph.registerNodeType("Bot Action/Set Status Type", setstatust );



    function setpr()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Prefix", "new prefix");
        this.widgets_up = true;
    }
    
    //name to show
    setpr.title = "Set Guild Prefix";
    
    //function to call when the node is executed
    setpr.prototype.onAction = function()
    {
         
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`\nawait client.userdata.set(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`prefix`+"`"+`,`+"`"+`${LiteGraph.parse(this.properties["Prefix"])}`+"`"+`);`);
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Bot Action/Set Guild Prefix", setpr );


    function lb(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID","")
        this.addProperty("User Data Field", "");
        this.addProperty("Save members to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    lb.title = "Leaderboard";
    lb.prototype.onAction = function(){
       
        this.setOutputData(0, this.getInputData(0));  




        if(this.getOutputData(0)){
   
   
        appendFileSync(this.getInputData(0),`\n${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save members to variable"].replaceAll("${","").replaceAll("}","")} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).members.cache.sort((a,b)=>client.userdata.fetch(\`${LiteGraph.parse(this.properties["Guild ID"])}_${LiteGraph.parse(this.properties["User Data Field"])}_\${b.id}\`)-client.userdata.fetch(\`${LiteGraph.parse(this.properties["Guild ID"])}_${LiteGraph.parse(this.properties["User Data Field"])}_\${a.id}\`)).toJSON()`);
            LiteGraph.save_var(this.properties["Save members to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Array',[{name:"size",value:"length"}]) 
    
        }
        this.trigger("Action");
    }
    LiteGraph.registerNodeType("Bot Action/Leaderboard", lb );



    
    function createchannel()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Name", "name of channel");
        this.addProperty("Type", "<select><option selected='selected'>Text</option><option>Voice</option><option>Stage</option><option>News</option><option>Category</option>", "dropdown");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
    }
    
    //name to show
    createchannel.title = "Create Channel";
    
    //function to call when the node is executed
    createchannel.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  
 



           
            if(this.getOutputData(0)){
            
            
            
            if(this.properties["Save to variable"].trim()){
                appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).channels.create(`+"`"+LiteGraph.parse(this.properties["Name"])+"`"+`, `+"{type:`"+`${LiteGraph.parse_dropdown(this.properties["Type"])}`+"`.trim().toUpperCase().replace('TEXT','GUILD_TEXT').replace('VOICE','GUILD_VOICE').replace('CATEGORY','GUILD_CATEGORY').replace('STAGE','GUILD_STAGE_VOICE').replace('NEWS','GUILD_NEWS')"+`}).then(ch =>{
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = ch;
                });`);
                LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Channel",[{name:"id",value:"id"},{name:"name",value:"name"},{name:"guild id",value:"guild.id"},{name:"category id",value:"parent.id"}]) 
            }else{
                appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).channels.create(`+"`"+LiteGraph.parse(this.properties["Name"])+"`"+`, `+"{type:`"+`${LiteGraph.parse_dropdown(this.properties["Type"])}`+"`.trim().toUpperCase().replace('TEXT','GUILD_TEXT').replace('VOICE','GUILD_VOICE').replace('CATEGORY','GUILD_CATEGORY').replace('STAGE','GUILD_STAGE_VOICE').replace('NEWS','GUILD_NEWS')"+`});`);
            }
            }
            this.trigger("Action");
                
      
    }
    
    //register in the system
    LiteGraph.registerNodeType("Channel Action/Create Channel", createchannel );

    function clchannel()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Channel", "channel to clone");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
    }
    
    //name to show
    clchannel.title = "Clone Channel";
    
    //function to call when the node is executed
    clchannel.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  
 



           
            if(this.getOutputData(0)){
            
            
            
            if(this.properties["Save to variable"].trim()){
                appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).channels.cache.find(ch=>ch.name == \`${this.properties["Channel"]}\`||ch.id == \`${this.properties["Channel"]}\`).clone().then(ch =>{
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = ch;
                })};`);
                LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Channel",[{name:"id",value:"id"},{name:"name",value:"name"},{name:"guild id",value:"guild.id"},{name:"category id",value:"parent.id"}])  
            }else{
                appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).channels.cache.find(ch=>ch.name == \`${this.properties["Channel"]}\`||ch.id == \`${this.properties["Channel"]}\`).clone()};`);
            }
            }
            this.trigger("Action");
                
      
    }
    
    //register in the system
    LiteGraph.registerNodeType("Channel Action/Clone Channel", clchannel );


  



    function setuserpermsch()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Channel", "channel to set permissions to");
        this.addProperty("User Or Role", "${dbwVars.CommandAuthor?.id}");
        this.addProperty("Permission", "<option selected='selected'>View Channel</option><option>Manage Channel</option><option>Manage Permissions</option><option>Manage Webhooks</option><option>Create Invite</option><option>Connect</option><option>Speak</option><option>Video</option><option>Use Voice Activity</option><option>Priority Speaker</option><option>Mute Members</option><option>Deafen Members</option><option>Move Members</option><option>Send Messages</option><option>Embed Links</option><option>Attach Files</option><option>Add Reactions</option><option>Use External Emoji</option><option>Mention Everyone</option><option>Manage Messages</option><option>Read Message History</option><option>Send TTS Messages</option>","dropdown");
        this.addProperty("", `<div class='property'><span class='property_name'>Value</span><select class='property_value' id='Value'><option value='false'>false</option><option value='null'>default</option><option value='true' selected='selected'>true</option></select></div>`,"html");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
        this.size = [170, 27];
    }
    
    //name to show
    setuserpermsch.title = "Edit Channel Perms";
    
    //function to call when the node is executed
    setuserpermsch.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  
 



           
            if(this.getOutputData(0)){
            
            
            
           
                appendFileSync(this.getInputData(0),`\n`+`await client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).channels.cache.find(ch=>ch.name == `+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+` || ch.id == `+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).permissionOverwrites.create(client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).members.cache.get(\`${LiteGraph.parse(this.properties["User Or Role"])}\`)||client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).roles.cache.find(r=>r.name==\`${LiteGraph.parse(this.properties["User Or Role"])}\`||r.id==\`${LiteGraph.parse(this.properties["User Or Role"])}\`),`+`{ \"${LiteGraph.parse_dropdown(this.properties["Permission"]).trim().replaceAll(/\s/g,"_").toUpperCase()}\": ${this.actions["Value"]}})`); 
            
            }
            this.trigger("Action");
                
      
    }
    
    //register in the system
    LiteGraph.registerNodeType("Channel Action/Edit Channel Perms", setuserpermsch );

    

    function assignchannel()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Channel", "channel to assign");
        this.addProperty("Category", "assign the channel to this category");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
    }
    
    //name to show
    assignchannel.title = "Assign Channel";
    
    //function to call when the node is executed
    assignchannel.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  
 



            if(this.getOutputData(0)){
            
            
            
                appendFileSync(this.getInputData(0),`\n await client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).channels.cache.find( ch =>`+"ch.name == `"+LiteGraph.parse(this.properties["Channel"])+"` || "+"ch.id == `"+LiteGraph.parse(this.properties["Channel"])+"`"+`).setParent(client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).channels.cache.find( ct => ct.name ==`+"`"+LiteGraph.parse(this.properties["Category"])+"`"+" || ct.id == "+"`"+LiteGraph.parse(this.properties["Category"])+"`"+`));`);

            }
                
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Channel Action/Assign Channel", assignchannel );

    function renamechannel()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Channel", "channel to rename");
        this.addProperty("Name", "new name");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
        this.size = [150, 27]
    }
    
    //name to show
    renamechannel.title = "Rename Channel";
    
    //function to call when the node is executed
    renamechannel.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  
 



            if(this.getOutputData(0)){
            
            
            
                appendFileSync(this.getInputData(0),`\n await client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).channels.cache.find( ch =>`+"ch.name == `"+LiteGraph.parse(this.properties["Channel"])+"` || "+"ch.id == `"+LiteGraph.parse(this.properties["Channel"])+"`"+`).setName(`+"`"+LiteGraph.parse(this.properties["Name"])+"`"+`);`);

            }
                
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Channel Action/Rename Channel", renamechannel );


    function settopic()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Channel", "channel to set topic to");
        this.addProperty("Topic", "topic to set");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
    }
    
    //name to show
    settopic.title = "Set Topic";
    
    //function to call when the node is executed
    settopic.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  
 



            if(this.getOutputData(0)){
            
            
            
                appendFileSync(this.getInputData(0),`\n await client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).channels.cache.find( ch =>`+"ch.name == `"+LiteGraph.parse(this.properties["Channel"])+"` || "+"ch.id == `"+LiteGraph.parse(this.properties["Channel"])+"`"+`).setTopic(`+"`"+LiteGraph.parse(this.properties["Topic"])+"`"+`);`);

            }
                
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Channel Action/Set Topic", settopic );




    function deletechannel()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Channel", "channel to delete");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
    }
    
    //name to show
    deletechannel.title = "Delete Channel";
    
    //function to call when the node is executed
    deletechannel.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  




            if(this.getOutputData(0)){
            
            
            
                appendFileSync(this.getInputData(0),`\n await client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).channels.cache.find(ch=>`+"ch.name == `"+LiteGraph.parse(this.properties["Channel"])+"`||ch.id=="+"`"+LiteGraph.parse(this.properties["Channel"])+"`"+`).delete();`);

            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Channel Action/Delete Channel", deletechannel );


    function deletemessages()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Channel", "channel to delete messages in");
        this.addProperty("Number", "Maximum of 100");
        this.addProperty("User ID", "all");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.size = [170, 27];
        this.widgets_up = true;
    }
    
    //name to show
    deletemessages.title = "Delete Messages";
    
    //function to call when the node is executed
    deletemessages.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  




            if(this.getOutputData(0)){
            
            
           
            
                appendFileSync(this.getInputData(0),`\nif(\`${this.properties["User ID"]}\`.toLowerCase().trim() == 'all'){
                    await client.channels.cache.find(ch=> ch.name==`+"`"+LiteGraph.parse(this.properties["Channel"])+"`|| ch.id == "+"`"+LiteGraph.parse(this.properties["Channel"])+"`"+`).bulkDelete(Number(\`${LiteGraph.parse(this.properties["Number"])}\`));
                }else{
                    await client.channels.cache.find(ch=> ch.name==`+"`"+LiteGraph.parse(this.properties["Channel"])+"`|| ch.id == "+"`"+LiteGraph.parse(this.properties["Channel"])+"`"+`).messages.fetch().then(async(messages)=>{
                        let messagestodelete = [];
                        let i = 0;
                        await messages.filter(m => m.author.id == \`${this.properties["User ID"]}\`).forEach(async msg => {if(i < Number(\`${LiteGraph.parse(this.properties["Number"])}\`)){await messagestodelete.push(msg);i++}})
                        await client.channels.cache.find(ch=> ch.name==`+"`"+LiteGraph.parse(this.properties["Channel"])+"`|| ch.id == "+"`"+LiteGraph.parse(this.properties["Channel"])+"`"+`).bulkDelete(messagestodelete);
                    }).catch(err=>client.consolelog(err.stack))
                }`);
            
        }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Channel Action/Delete Messages", deletemessages );

    function addcommand()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Command Name", "name of the command");
        this.addProperty("Description", "none");
        this.addHTML("<div class='property'><span class='property_name'>Allow bots to use this command?</span><input id='Allow bots to use this command?' type='checkbox'></div>");
        this.widgets_up = true;
       
    }
    //name to show
    addcommand.title = "Add Command";

    //function to call when the node is executed
    addcommand.prototype.onExecute = function(action, param)
    {
        if(!global.commandsdata.find(c=>c.name== `${LiteGraph.botpath}\\commands\\`+this.properties["Command Name"].replaceAll(' ','').toLowerCase()+".js")){
        if(this.isOutputConnected(0)){
        if(this.properties["Command Name"].replaceAll(' ','') ){
           
            writeFileSync( `${LiteGraph.botpath}\\commands\\`+this.properties["Command Name"].replaceAll(' ','').toLowerCase()+".js", `module.exports = {
                name: `+"`"+this.properties["Command Name"].replaceAll(' ','').toLowerCase()+"`"+`,
                description:  `+"`"+LiteGraph.parse(this.properties["Description"])+"`"+`,
                slash:'false',
                contextmenu:'false',
                async execute(client, message, args, Discord) {
                if(message.author.bot != ${this.actions["Allow bots to use this command?"]}) return;
                let server
                let temp = {}
                if(message.guild){
                    if(!client.servers.get(message.guild.id)){
                        client.servers.set(message.guild.id,{id:message.guild.id})
                    }
                    server = client.servers.get(message.guild.id)
                }else{
                    if(!client.servers.get(message.channel.id)){
                        client.servers.set(message.channel.id,{id:message.channel.id})
                    }
                    server = client.servers.get(message.channel.id)
                }
                let dbwVars = {}
                dbwVars["CommandAuthor"] = message.author;
                dbwVars["CommandChannel"] = message.channel;
                dbwVars["CommandGuild"] = message.guild;
                dbwVars["Commands"] = client.commands.toJSON();
                dbwVars["SlashCommands"] = client.slashcommands.toJSON();\ndbwVars["ContextMenus"] = client.contextmenus.toJSON();
                dbwVars["Command"] = this;
                dbwVars["Bot"] = client.user;
                try{
                    dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.guild.id}prefix\`)||client.prefix
                }catch{
                    dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.channel.id}prefix\`)||client.prefix
                }
            `);
            this.setOutputData( 0, `${LiteGraph.botpath}\\commands\\`+this.properties["Command Name"].replaceAll(' ','').toLowerCase()+".js");
            this.trigger("Action");
       
    }else{
        this.setOutputData(0, null);  
    }
    }
    }else{
        alert(`You already have a command named <b>${this.properties["Command Name"]}</b>!`)
    }
    }
    
    //register in the system
    LiteGraph.registerNodeType("Command Action/Add Command", addcommand );

    
    function addscommand()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addHTML("<style>#h::after {content: \" \";position: absolute;top: 50%;right: 100%;margin-top: -7px;border-width: 7px;border-style: solid;border-color: transparent rgba(0,0,0,0.9) transparent transparent;}</style><div class='property'><span class='property_name'>Command Name</span><span class='property_value' contenteditable='plaintext-only' onblur=\"document.getElementById('h').style.display='none'\" onfocus=\"document.getElementById('h').style.display='block'\" id='Command Name'>name of the command</span><h id='h' style='display:none;border:1px solid black;font-size:15px;font-family:Arial;background-color:rgba(0,0,0,0.5);padding:7px;border-radius:4px;margin-top:-10%;position:absolute;left:101%;width:fit-content;color:#cccccc;backdrop-filter: blur(5px);'><i class=\"fa fa-info-circle\" style=\"font-size:18px;color:#cccccc;background:rgba(0,0,0,0.5);\"></i> Slash commands take up to an hour to register globally but up to 5 mins in dms</h></div>")
        this.addProperty("Description", "none");
        this.size = [180,27]
        this.widgets_up = true;
    }
    //name to show
    addscommand.title = "Add Slash Command";

    //function to call when the node is executed
    addscommand.prototype.onExecute = function(action, param)
    {
        if(!global.commandsdata.find(c=>c.name== `${LiteGraph.botpath}\\slashcommands\\`+this.actions["Command Name"].replaceAll(' ','').toLowerCase()+".js")){
        if(this.isOutputConnected(0)){
        if(this.actions["Command Name"].replaceAll(' ','') ){
           
            writeFileSync( `${LiteGraph.botpath}\\slashcommands\\`+this.actions["Command Name"].replaceAll(' ','').toLowerCase()+".js", `module.exports = {
                name: `+"`"+this.actions["Command Name"].replaceAll(' ','').toLowerCase()+"`"+`,
                description:  `+"`"+LiteGraph.parse(this.properties["Description"])+"`"+`,
                slash:'true',
                contextmenu:'false',
                async execute(client, message, args, Discord) {
                let server
                let temp = {}
                if(message.guild){
                    if(!client.servers.get(message.guild.id)){
                        client.servers.set(message.guild.id,{id:message.guild.id})
                    }
                    server = client.servers.get(message.guild.id)
                }else{
                    if(!client.servers.get(message.channel.id)){
                        client.servers.set(message.channel.id,{id:message.channel.id})
                    }
                    server = client.servers.get(message.channel.id)
                }
                let dbwVars = {}
                dbwVars["CommandAuthor"] = message.author;
                dbwVars["CommandChannel"] = message.channel;
                dbwVars["CommandGuild"] = message.guild;
                dbwVars["Commands"] = client.commands.toJSON();
                dbwVars["SlashCommands"] = client.slashcommands.toJSON();\ndbwVars["ContextMenus"] = client.contextmenus.toJSON();
                dbwVars["Command"] = this;
                dbwVars["Bot"] = client.user;
                try{
                    dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.guild.id}prefix\`)||client.prefix
                }catch{
                    dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.channel.id}prefix\`)||client.prefix
                }
            `);
            this.setOutputData( 0, `${LiteGraph.botpath}\\slashcommands\\`+this.actions["Command Name"].replaceAll(' ','').toLowerCase()+".js");
            this.trigger("Action");
       
    }else{
        this.setOutputData(0, null);  
    }
    }
    }else{
        alert(`You already have a slash command named <b>${this.actions["Command Name"]}</b>!`)
    }
    }
    
    //register in the system
    LiteGraph.registerNodeType("Command Action/Add Slash Command", addscommand );


    function addcontextmenu()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addHTML("<style>#h::after {content: \" \";position: absolute;top: 50%;right: 100%;margin-top: -7px;border-width: 7px;border-style: solid;border-color: transparent rgba(0,0,0,0.9) transparent transparent;}</style><div class='property'><span class='property_name'>Name</span><span class='property_value' contenteditable='plaintext-only' onblur=\"document.getElementById('h').style.display='none'\" onfocus=\"document.getElementById('h').style.display='block'\" id='Name'>name of the context menu</span><h id='h' style='display:none;border:1px solid black;font-size:15px;font-family:Arial;background-color:rgba(0,0,0,0.5);padding:7px;border-radius:4px;margin-top:-10%;position:absolute;left:101%;width:fit-content;color:#cccccc;backdrop-filter: blur(5px);'><i class=\"fa fa-info-circle\" style=\"font-size:18px;color:#cccccc;background:rgba(0,0,0,0.5);\"></i> Context menus take up to an hour to register globally but up to 5 mins in dms</h></div><div class='property'><span class='property_name'>Type</span><select class='property_value' id='Type'><option value='3' selected='selected'>Message</option><option value='2'>User</option></select></div><div class='property'><span class='property_name'>Save to variable</span><span class='property_value' id='Save to variable' contenteditable='plaintext-only'>target</span></div>")
        this.addProperty("Variable Type","<option selected='selected'>temp</option><option>server</option><option>global</option>","dropdown")
        this.size = [170,27]
        this.widgets_up = true;
    }
    //name to show
    addcontextmenu.title = "Add Context Menu";

    //function to call when the node is executed
    addcontextmenu.prototype.onExecute = function(action, param)
    {
        if(this.actions['Type'][0] == '3'){
        if(!global.commandsdata.find(c=>c.name== `${LiteGraph.botpath}\\contextmenus\\`+this.actions["Name"].replaceAll(' ','')+".js")){
        if(this.isOutputConnected(0)){
        if(this.actions["Name"].replaceAll(' ','') ){
           
            writeFileSync( `${LiteGraph.botpath}\\contextmenus\\`+this.actions["Name"].replaceAll(' ','')+".js", `module.exports = {
                name: `+"`"+this.actions["Name"].replaceAll(' ','')+"`"+`,
                type:Number(\`${this.actions['Type'][0]}\`),
                slash:'false',
                contextmenu:'true',
                async execute(client, message, args, Discord) {
                let server
                let temp = {}
                if(message.guild){
                    if(!client.servers.get(message.guild.id)){
                        client.servers.set(message.guild.id,{id:message.guild.id})
                    }
                    server = client.servers.get(message.guild.id)
                }else{
                    if(!client.servers.get(message.channel.id)){
                        client.servers.set(message.channel.id,{id:message.channel.id})
                    }
                    server = client.servers.get(message.channel.id)
                }
                let dbwVars = {}
                dbwVars["CommandAuthor"] = message.author;
                dbwVars["CommandChannel"] = message.channel;
                dbwVars["CommandGuild"] = message.guild;
                dbwVars["Commands"] = client.commands.toJSON();
                dbwVars["SlashCommands"] = client.slashcommands.toJSON();\ndbwVars["ContextMenus"] = client.contextmenus.toJSON();
                dbwVars["Command"] = this;
                dbwVars["Bot"] = client.user;
                try{
                    dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.guild.id}prefix\`)||client.prefix
                }catch{
                    dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.channel.id}prefix\`)||client.prefix
                }
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.actions["Save to variable"]} = message.data.resolved.messages[message.data.target_id];
            `);
            this.setOutputData( 0, `${LiteGraph.botpath}\\contextmenus\\`+this.actions["Name"].replaceAll(' ','')+".js");
            this.trigger("Action");
            LiteGraph.save_var(this.actions["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Message",[{name:"id",value:"id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}])
    }else{
        this.setOutputData(0, null);  
    }
    }
    }else{
        alert(`You already have a context menu named <b>${this.actions["Name"]}</b>!`)
    }
}else if(this.actions['Type'][0] == '2'){
    if(!global.commandsdata.find(c=>c.name== `${LiteGraph.botpath}\\contextmenususer\\`+this.actions["Name"].replaceAll(' ','')+".js")){
        if(this.isOutputConnected(0)){
        if(this.actions["Name"].replaceAll(' ','') ){
           
            writeFileSync( `${LiteGraph.botpath}\\contextmenususer\\`+this.actions["Name"].replaceAll(' ','')+".js", `module.exports = {
                name: `+"`"+this.actions["Name"].replaceAll(' ','')+"`"+`,
                type:Number(\`${this.actions['Type'][0]}\`),
                slash:'false',
                contextmenu:'true',
                async execute(client, message, args, Discord) {
                let server
                let temp = {}
                if(message.guild){
                    if(!client.servers.get(message.guild.id)){
                        client.servers.set(message.guild.id,{id:message.guild.id})
                    }
                    server = client.servers.get(message.guild.id)
                }else{
                    if(!client.servers.get(message.channel.id)){
                        client.servers.set(message.channel.id,{id:message.channel.id})
                    }
                    server = client.servers.get(message.channel.id)
                }
                let dbwVars = {}
                dbwVars["CommandAuthor"] = message.author;
                dbwVars["CommandChannel"] = message.channel;
                dbwVars["CommandGuild"] = message.guild;
                dbwVars["Commands"] = client.commands.toJSON();
                dbwVars["SlashCommands"] = client.slashcommands.toJSON();\ndbwVars["ContextMenus"] = client.contextmenus.toJSON();
                dbwVars["Command"] = this;
                dbwVars["Bot"] = client.user;
                try{
                    dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.guild.id}prefix\`)||client.prefix
                }catch{
                    dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.channel.id}prefix\`)||client.prefix
                }
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.actions["Save to variable"]} = message.data.resolved.users[message.data.target_id];
            `);
            this.setOutputData( 0, `${LiteGraph.botpath}\\contextmenususer\\`+this.actions["Name"].replaceAll(' ','')+".js");
            this.trigger("Action");
            LiteGraph.save_var(this.actions["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"User",[{name:"id",value:"id"},{name:"name",value:"username"},{name:"tag",value:"tag"},{name:"discriminator",value:"discriminator"},{name:"last message id",value:"lastMessageId"}])
    }else{
        this.setOutputData(0, null);  
    }
    }
    }else{
        alert(`You already have a context menu named <b>${this.actions["Name"]}</b>!`)
    }
}
    }
    
    //register in the system
    LiteGraph.registerNodeType("Command Action/Add Context Menu", addcontextmenu );



    function triggercommand(){
        this.addProperty("Command Name","");
        this.addHTML("<div class='property'><span class='property_name'>Is Slash Command?</span><input type='checkbox' id='Is Slash Command?'></div>")
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
        this.size = [162,27]
    }
    triggercommand.title = "Execute Command";
    triggercommand.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`
            if(\`${this.actions["Is Slash Command?"]}\` == 'false'){
                client.commands.get(\`${this.properties["Command Name"]}\`.trim()).execute(client, message, args, Discord)
            }else{
                client.slashcommands.get(\`${this.properties["Command Name"]}\`.trim()).execute(client, message, args, Discord)
            }
            `);
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Command Action/Execute Command", triggercommand );


    function addoption(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Name", "");
        this.addProperty("Description", "none");
        this.addProperty("", "<div class='property'><span class='property_name'>Required</span><input id='Required' type='checkbox' checked='true'></div>","html");
        this.addProperty("Type", "<option selected='selected'>Text</option><option>Number</option><option>Integer</option><option>True or false</option><option>User</option><option>Channel</option><option>Role</option><option>Mentionable</option>", "dropdown");
        this.addProperty("Save to variable","option");
        this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    addoption.title = "Add Option";
    addoption.prototype.onAction = function(){
        this.setOutputData(0, this.getInputData(0));    


  
            if(this.getOutputData(0)){
                    const path = require('path')
                    
                    if(!global.obj){
                        global.obj = {}
                    }
                    let obj = global.obj
                    if(!obj[this.getInputData(0).replaceAll(`${LiteGraph.botpath}\\events\\`,'').replaceAll(`${LiteGraph.botpath}\\slashcommands\\`,'')]){
                        obj[this.getInputData(0).replaceAll(`${LiteGraph.botpath}\\events\\`,'').replaceAll(`${LiteGraph.botpath}\\slashcommands\\`,'')] = [];
                    }
                    if(this.properties["Description"].trim()){
                    obj[this.getInputData(0).replaceAll(`${LiteGraph.botpath}\\events\\`,'').replaceAll(`${LiteGraph.botpath}\\slashcommands\\`,'')].push({
                    name:`${this.properties["Name"].toLowerCase()}`,
                    description:`${this.properties["Description"]}`,
                    required:`${this.actions["Required"]}`,
                    type:`${LiteGraph.parse_dropdown(this.properties["Type"].replace("Text","3").replace("Number","10").replace("Integer","4").replace("True or false","5").replace("User","6").replace("Channel","7").replace("Role","8").replace("Mentionable","9"))}`})
                    }else{
                        obj[this.getInputData(0).replaceAll(`${LiteGraph.botpath}\\events\\`,'').replaceAll(`${LiteGraph.botpath}\\slashcommands\\`,'')].push({
                            name:`${this.properties["Name"].toLowerCase()}`,
                            description:`none`,
                            required:`${this.actions["Required"]}`,
                            type:`${LiteGraph.parse_dropdown(this.properties["Type"].replace("Text","3").replace("Number","10").replace("Integer","4").replace("True or false","5").replace("User","6").replace("Channel","7").replace("Role","8").replace("Mentionable","9"))}`})
                    }
                fs.writeFileSync(`${LiteGraph.botpath}\\Options_collector.json`,JSON.stringify(obj))
               if(LiteGraph.parse_dropdown(this.properties["Type"]) == 'User'){
                appendFileSync(this.getInputData(0),`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","").replaceAll("global.","")} = client.users.cache.get(message.data.options.find(o=>o.name==\`${this.properties["Name"]}\`).value);`)
                LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'User',[{name:"id",value:"id"},{name:"name",value:"username"},{name:"tag",value:"tag"},{name:"discriminator",value:"discriminator"},{name:"last message id",value:"lastMessageId"}])  
               }else if(LiteGraph.parse_dropdown(this.properties["Type"]) == 'Channel'){
                appendFileSync(this.getInputData(0),`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","").replaceAll("global.","")} = client.channels.cache.get(message.data.options.find(o=>o.name==\`${this.properties["Name"]}\`).value);`)
                LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Channel",[{name:"id",value:"id"},{name:"name",value:"name"},{name:"guild id",value:"guild.id"},{name:"category id",value:"parent.id"}])  
               }else if(LiteGraph.parse_dropdown(this.properties["Type"]) == 'Role'){
                appendFileSync(this.getInputData(0),`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","").replaceAll("global.","")} = message.guild.roles.cache.get(message.data.options.find(o=>o.name==\`${this.properties["Name"]}\`).value);`)
                LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Role',[{name:"id",value:"id"},{name:"name",value:"name"},{name:"guild id",value:"guild.id"}]) 
               }else if(LiteGraph.parse_dropdown(this.properties["Type"]) == 'Mentionable'){
                appendFileSync(this.getInputData(0),`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","").replaceAll("global.","")} = client.users.cache.get(message.data.options.find(o=>o.name==\`${this.properties["Name"]}\`).value)||client.channels.cache.get(message.data.options.find(o=>o.name==\`${this.properties["Name"]}\`).value)||message.guild.roles.cache.get(message.data.options.find(o=>o.name==\`${this.properties["Name"]}\`).value);`)
                LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Variable') 
               }else{
                appendFileSync(this.getInputData(0),`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","").replaceAll("global.","")} = message.data.options.find(o=>o.name==\`${this.properties["Name"]}\`).value;`)
                LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Variable') 
               }
            this.trigger("Action");
            }
    }
    LiteGraph.registerNodeType("Command Action/Add Option", addoption );


    function addchoice(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Option Name", "");
        this.addProperty("Choice Name", "");
        this.addProperty("Choice Value", "");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    addchoice.title = "Add Choice";
    addchoice.prototype.onAction = function(){
        this.setOutputData(0, this.getInputData(0));    


  
            if(this.getOutputData(0)){
                
               
                let obj = global.obj
                if(obj[this.getInputData(0).replaceAll(`${LiteGraph.botpath}\\events\\`,'').replaceAll(`${LiteGraph.botpath}\\slashcommands\\`,'')].find(op=>op.name == this.properties["Option Name"])){
                    if(!obj[this.getInputData(0).replaceAll(`${LiteGraph.botpath}\\events\\`,'').replaceAll(`${LiteGraph.botpath}\\slashcommands\\`,'')].find(op=>op.name == this.properties["Option Name"]).choices){
                        obj[this.getInputData(0).replaceAll(`${LiteGraph.botpath}\\events\\`,'').replaceAll(`${LiteGraph.botpath}\\slashcommands\\`,'')].find(op=>op.name == this.properties["Option Name"]).choices = []
                    }
                    obj[this.getInputData(0).replaceAll(`${LiteGraph.botpath}\\events\\`,'').replaceAll(`${LiteGraph.botpath}\\slashcommands\\`,'')].find(op=>op.name == this.properties["Option Name"]).choices.push({
                        name:this.properties["Choice Name"],
                        value:this.properties["Choice Value"]
                    })
                fs.writeFileSync(`${LiteGraph.botpath}\\Options_collector.json`,JSON.stringify(obj))
                }
            this.trigger("Action");
            }
    }
    LiteGraph.registerNodeType("Command Action/Add Choice", addchoice );



    function bstopevent()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    bstopevent.title = "Bot Stop";

    //function to call when the node is executed
    bstopevent.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\onExit.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\onExit.js`, `
            
           
            module.exports = async (Discord, client) => {
                let server = global;
                let temp = {}
            `);
            }
    }
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Bot Stop", bstopevent );


    function bjoinevent()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save guild to variable", "joinguild");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    bjoinevent.title = "Bot Join";

    //function to call when the node is executed
    bjoinevent.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\guildCreate.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\guildCreate.js`, `
            
           
            module.exports = async (Discord, client, guild) => {
                let server = client.servers.get(guild.id);
                let temp = {}
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save guild to variable"].replaceAll("${","").replaceAll("}","")} = guild;
            `);
           LiteGraph.save_var(this.properties["Save guild to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Guild',[{name:"id",value:"id"},{name:"name",value:"name"},{name:"owner id",value:"ownerId"},{name:"member count",value:"memberCount"}]) 
            }
    }
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Bot Join", bjoinevent );


    function dfunc(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Function Name","my_test_function")
        this.widgets_up = true; 
    }
    dfunc.title = "Define Function";
    dfunc.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\Functions.js`);
       
        if(this.getOutputData(0)){
            appendFileSync( `${LiteGraph.botpath}\\Functions.js`, `
                
               global.${this.properties["Function Name"].replaceAll("${","").replaceAll("}","").replaceAll("global.","")} = async function (Discord, client, message, server, temp){
                    let dbwVars = {};
                    if(message){
                        dbwVars["CommandAuthor"] = message.author;
                        dbwVars["CommandChannel"] = message.channel;
                        dbwVars["CommandGuild"] = message.guild;
                        dbwVars["Commands"] = client.commands.toJSON();
                        dbwVars["SlashCommands"] = client.slashcommands.toJSON();\ndbwVars["ContextMenus"] = client.contextmenus.toJSON();
                        dbwVars["Bot"] = client.user;
                        try{
                            dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.guild.id}prefix\`)||client.prefix
                        }catch{
                            dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.channel.id}prefix\`)||client.prefix
                        }
                    }
            `);
           LiteGraph.save_func(this.properties["Function Name"],"Function")
    }
    this.trigger("Action");
    appendFileSync( `${LiteGraph.botpath}\\Functions.js`, `\n}`)
    }
}
    LiteGraph.registerNodeType("Functions/Define Function", dfunc );
    function cfunc(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addProperty("Function","function to call")
        this.widgets_up = true;
    }
    cfunc.title = "Call Function";
    cfunc.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait global.${this.properties["Function"].replaceAll("${","").replaceAll("global.","").replaceAll("}","").replaceAll("(","").replaceAll(")","")}(Discord,client,message,server,temp)`);
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Functions/Call Function", cfunc );

    function crimg()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Background", "background of image");
        this.addProperty("Width", "width of image");
        this.addProperty("Height", "height of image");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    crimg.title = "Create Image";
    
    //function to call when the node is executed
    crimg.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  




             
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = await client.canvas.createCanvas(${LiteGraph.parse(this.properties["Width"]).replaceAll("${","").replaceAll("}","")}, ${LiteGraph.parse(this.properties["Height"]).replaceAll("${","").replaceAll("}","")});
        await ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")}.getContext('2d').drawImage(await client.canvas.loadImage(`+"`"+this.properties["Background"].replaceAll("avatarURL()","avatarURL({format: 'png'})")+"`"+`), 0, 0, ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")}.width, ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")}.height);`);
            }
            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Image Variable',[{name:"width",value:"width"},{name:"height",value:"height"}]) 
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Image Action/Create Image", crimg );

    function cratt()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Name", "img.png");
        this.addProperty("Image", "");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.size = [170,27]
    }
    
    //name to show
    cratt.title = "Create Attachment";
    
    //function to call when the node is executed
    cratt.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  




             
            if(this.getOutputData(0)){
        


                if(!this.properties["Image"].includes("://")){
                    appendFileSync(this.getInputData(0),`
                    if(await ${this.properties["Image"].replaceAll('${','').replaceAll('}','')}.toBuffer){
                        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = new Discord.MessageAttachment(await ${this.properties["Image"].replaceAll('${','').replaceAll('}','')}.toBuffer(),\`${LiteGraph.parse(this.properties["Name"])}\`);
                    }else{
                        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = new Discord.MessageAttachment(await ${this.properties["Image"].replaceAll('${','').replaceAll('}','')},\`${LiteGraph.parse(this.properties["Name"])}\`);
                    }`);
                }else{
                    appendFileSync(this.getInputData(0),`
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = new Discord.MessageAttachment(\`${this.properties["Image"]}\`,\`${LiteGraph.parse(this.properties["Name"])}\`);`);
                }
            }
            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Attachment',[{name:"id",value:"id"},{name:"name",value:"name"},{name:"width",value:"width"},{name:"height",value:"height"}]) 
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Image Action/Create Attachment", cratt );

    

    function ginvr(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "");
        this.addProperty("Invite Code", "");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    ginvr.title = "Get Inviter";
    ginvr.prototype.onAction = function(){
        this.setOutputData(0, this.getInputData(0));    


  
            if(this.getOutputData(0)){
                
                        appendFileSync(this.getInputData(0),`
                        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll('${','').replaceAll('}','')} = await (await client.guilds.cache.get(\`${this.properties["Guild ID"]}\`).invites.fetch()).find(i=>i.code==\`${this.properties['Invite Code']}\`).inviter}`);
                LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'User',[{name:"id",value:"id"},{name:"name",value:"username"},{name:"tag",value:"tag"},{name:"discriminator",value:"discriminator"},{name:"last message id",value:"lastMessageId"}]) 
            this.trigger("Action");
            }
    }
    LiteGraph.registerNodeType("Invite Action/Get Inviter", ginvr );

    function ginvg(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Invite URL", "");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    ginvg.title = "Get Invite Guild";
    ginvg.prototype.onAction = function(){
        this.setOutputData(0, this.getInputData(0));    


  
            if(this.getOutputData(0)){
                
                        appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.forEach(guild => {
                            guild.invites.fetch().then(invites => {
                                invites.forEach(invite => {
                                    if (invite.url == \`${LiteGraph.parse(this.properties["Invite URL"])}\`) {
                                        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"]} = guild
                                    }
                                })
                            })
                        });`);
                LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Guild',[{name:"id",value:"id"},{name:"name",value:"name"},{name:"owner id",value:"ownerId"},{name:"member count",value:"memberCount"}]) 
            this.trigger("Action");
            }
    }
    LiteGraph.registerNodeType("Invite Action/Get Invite Guild", ginvg );

    function ginvuses(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "");
        this.addProperty("Invite Code", "");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    ginvuses.title = "Get Invite Uses";
    ginvuses.prototype.onAction = function(){
        this.setOutputData(0, this.getInputData(0));    


  
            if(this.getOutputData(0)){
                
                        appendFileSync(this.getInputData(0),`
                        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll('${','').replaceAll('}','')} = await (await client.guilds.cache.get(\`${this.properties["Guild ID"]}\`).invites.fetch()).find(i=>i.code==\`${this.properties['Invite Code']}\`).uses}`);
                LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Invite Uses')
            this.trigger("Action");
            }
    }
    LiteGraph.registerNodeType("Invite Action/Get Invite Uses", ginvuses );
    
    function bleaveevent()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save guild to variable", "leaveguild");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    bleaveevent.title = "Bot Leave";

    //function to call when the node is executed
    bleaveevent.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\guildDelete.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\guildDelete.js`, `
            
           
            module.exports = async (Discord, client, guild) => {
                let server = client.servers.get(guild.id)
                let temp = {}
                let dbwVars;
                let message;
                if(client.message){
                    message = client.message;
                    if(client.dbwVars){
                        dbwVars = client.dbwVars;
                    }
                }
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save guild to variable"].replaceAll("${","").replaceAll("}","").replaceAll("global.","")} = guild;
            `);
           LiteGraph.save_var(this.properties["Save guild to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Guild',[{name:"id",value:"id"},{name:"name",value:"name"},{name:"owner id",value:"ownerId"},{name:"member count",value:"memberCount"}]) 
            }
    }
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Bot Leave", bleaveevent );


    function readyevent()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    readyevent.title = "Bot Ready";

    //function to call when the node is executed
    readyevent.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\ready.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\ready.js`, `
            
           
            module.exports = async (Discord, client) => {
                
                client.consolelog(\`\${client.user.tag} is now online!\`)
                let server = global;
                let temp = {}
                client.invites = new Map()
                try{
                    await client.guilds.cache.forEach(async guild=>{
                        await client.fetch(guild.id)
                         .then(invites=>client.invites.set(guild.id,invites))
                         .catch(function (err) {
                             return
                         })
                })
                }catch{
                    return
                }
                if(client.slashcommands.size > 0){
                    client.consolelog(\`Loading slash commands...\`);
                }
                let data = [];
                for (let index = 0; index < await client.slashcommands.size; index++) {
                    const element = await client.slashcommands.toJSON()[index];
                    data.push({
                        name:element.name,
                        description:element.description,
                        options:JSON.parse(require('fs').readFileSync('./Options_collector.json'))[element.name+'.js']
                    })
                }

                for (let index = 0; index < await (await client.api.applications(await client.user.id).commands.get()).length; index++) {
                    const element = await (await client.api.applications(await client.user.id).commands.get())[index];
                    if(!client.slashcommands.find(cmd=>cmd.name == element.name)){
                        await client.api.applications(await client.user.id).commands(element.id).delete();
                    }
                }
                
                if(client.slashcommands.size > 0){
                    client.consolelog(\`Done!\`);
                }    
                if(client.contextmenus.size > 0){
                    client.consolelog(\`Loading context menus...\`);
                }
                for (let index = 0; index < client.contextmenus.size; index++) {
                    const element = client.contextmenus.toJSON()[index];
                    data.push({
                        name:element.name,
                        type:element.type
                    })
                }

                for (let index = 0; index < (await client.api.applications(client.user.id).commands.get()).length; index++) {
                    const element = (await client.api.applications(client.user.id).commands.get())[index];
                    if(!client.contextmenus.find(cmd=>cmd.name == element.name) && element.type == (2||3)){
                        await client.api.applications(client.user.id).commands(element.id).delete();
                    }
                }

                if(client.contextmenus.size > 0 && client.slashcommands.size > 0){
                    client.consolelog(\`Done!\`);
                    await client.api.applications(client.user.id).commands.put({data:data})
                }else if(client.contextmenus.size > 0){
                    client.consolelog(\`Done!\`);
                    await client.api.applications(client.user.id).commands.put({data:data})
                }else if(client.slashcommands.size > 0){
                    await client.api.applications(client.user.id).commands.put({data:data})
                }`);
            }
    }
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Bot Ready", readyevent );

    function cinviteevent()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save invite to variable", "cinvite");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    cinviteevent.title = "Invite Create";

    //function to call when the node is executed
    cinviteevent.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\inviteCreate.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\inviteCreate.js`, `
            
           
            module.exports = async (Discord, client, invite) => {
                if(!client.servers.get(invite.guild.id)){
                    client.servers.set(invite.guild.id,{id:invite.guild.id})
                }
                let server = client.servers.get(invite.guild.id)
                let temp = {}
                let dbwVars;
                let message;
                if(client.message){
                    message = client.message;
                    if(client.dbwVars){
                        dbwVars = client.dbwVars;
                    }
                }
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save invite to variable"].replaceAll("${","").replaceAll("}","")} = invite;
                client.invites.set(invite.guild.id,await client.fetch(invite.guild.id))
            `);
            LiteGraph.save_var(this.properties["Save invite to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Invite',[{name:"code",value:"code"},{name:"url",value:"url"},{name:"uses",value:"uses"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}]) 
            }
    }
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Invite Create", cinviteevent );



    function dinviteevent()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save invite to variable", "dinvite");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    dinviteevent.title = "Invite Delete";

    //function to call when the node is executed
    dinviteevent.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\inviteDelete.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\inviteDelete.js`, `
            
           
            module.exports = async (Discord, client, invite) => {
                if(!client.servers.get(invite.guild.id)){
                    client.servers.set(invite.guild.id,{id:invite.guild.id})
                }
                let server = client.servers.get(invite.guild.id)
                let temp = {}
                let dbwVars;
                let message;
                if(client.message){
                    message = client.message;
                    if(client.dbwVars){
                        dbwVars = client.dbwVars;
                    }
                }
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save invite to variable"].replaceAll("${","").replaceAll("}","")} = invite;
            `);
            LiteGraph.save_var(this.properties["Save invite to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Invite',[{name:"code",value:"code"},{name:"url",value:"url"},{name:"uses",value:"uses"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}]) 
            }
    }
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Invite Delete", dinviteevent );

    function joinevent()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save to variable", "joinmember");
        this.addProperty("Save invite to variable", "inv");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    joinevent.title = "Member Join";

    //function to call when the node is executed
    joinevent.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\guildMemberAdd.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\guildMemberAdd.js`, `
            
           
            module.exports = async (Discord, client, member) => {
                if(!client.servers.get(member.guild.id)){
                    client.servers.set(member.guild.id,{id:member.guild.id})
                }
                let server = client.servers.get(member.guild.id)
                let temp = {}
                let dbwVars;
                let message;
                if(client.message){
                    message = client.message;
                    if(client.dbwVars){
                        dbwVars = client.dbwVars;
                    }
                }
                try{
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = member;
                client.fetchedInvites = client.invites.get(member.guild.id)
            client.newInvites = await client.fetch(member.guild.id)
            await client.invites.set(client.guilds.cache.get(\`\${member.guild.id}\`).id, client.newInvites)
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save invite to variable"].replaceAll("${","").replaceAll("}","")} = client.newInvites.find(inv => client.fetchedInvites.get(inv.code).uses < inv.uses)
                }catch{
                    return;
                }
            `);
    LiteGraph.save_var(this.properties["Save invite to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Invite',[{name:"code",value:"code"},{name:"url",value:"url"},{name:"uses",value:"uses"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}]) 
            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Member',[{name:"id",value:"id"},{name:"name",value:"displayName"},{name:"tag",value:"user.tag"},{name:"discriminator",value:"user.discriminator"},{name:"guild id",value:"guild.id"},{name:"last message id",value:"lastMessageId"}]) 
            }
    }
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Member Join", joinevent );
    
    function sendmessage()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Message", "message to send");
        this.addProperty("Channel", "channel to send to");
        this.addProperty("Embeds", "")
        this.addProperty("Button Rows", "")
        this.addProperty("Attachments", "");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
    }
    
    //name to show
    sendmessage.title = "Send Message";
    
    //function to call when the node is executed
    sendmessage.prototype.onAction = function(action, param)
    {
 
            this.setOutputData(0, this.getInputData(0));  




            if(this.getOutputData(0)){
            
            
            if(this.properties["Save to variable"].trim()){
                appendFileSync(this.getInputData(0),`\n
                await client.channels.cache.find(ch=>ch.name==\``+LiteGraph.parse(this.properties["Channel"])+"`|| ch.id == `"+LiteGraph.parse(this.properties["Channel"])+"`).send({content:`"+LiteGraph.parse(this.properties["Message"]||` `)+`\`,embeds:[${this.properties["Embeds"].replaceAll("${","").replaceAll("}","")}],files:[${this.properties["Attachments"].replaceAll("${","").replaceAll("}","")}],components:[${this.properties["Button Rows"].replaceAll("${","").replaceAll("}","")}]}`+`).then( msg => {
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = msg; 
                })`);
                LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Message',[{name:"id",value:"id"},{name:"author id",value:"author.id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}])
            }else{
                   appendFileSync(this.getInputData(0),`\n await client.channels.cache.find(ch=>ch.name==\``+LiteGraph.parse(this.properties["Channel"])+"`|| ch.id == `"+LiteGraph.parse(this.properties["Channel"])+"`).send("+`{content:\`${LiteGraph.parse(this.properties["Message"]||` `)}\`,embeds:[${this.properties["Embeds"].replaceAll("${","").replaceAll("}","")}],files:[${this.properties["Attachments"].replaceAll("${","").replaceAll("}","")}],components:[${this.properties["Button Rows"].replaceAll("${","").replaceAll("}","")}]}`+`) `);
            }
                }
                this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Message Action/Send Message", sendmessage );
    function reply(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Message", "Hi there!");
        this.addProperty("", "<div class='property'><span class='property_name'>Author Only?(for slash commands)</span><input id='Author Only?(for slash commands)' type='checkbox' ></div>","html");
        this.addProperty("Embeds", ``);
        this.addProperty("Button Rows", ``);
        this.addProperty("Attachments", ``);
        this.addProperty('Save to variable','')
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    reply.title = "Reply";
    reply.prototype.onAction = function(){
        this.setOutputData(0, this.getInputData(0));    


  
            if(this.getOutputData(0)){
                

                    if(this.properties["Save to variable"].trim()){
                       
                            appendFileSync(this.getInputData(0),`\n
                            try{await message.reply({files:[${this.properties["Attachments"].replaceAll("${","").replaceAll("}","")}],content:\`${LiteGraph.parse(this.properties["Message"]||' ')}\`,allowedMentions: { repliedUser: false },components:[${this.properties["Button Rows"].replaceAll("${","").replaceAll("}","")}],embeds:[${this.properties["Embeds"].replaceAll('${','').replaceAll('}','')}]}).then(m=>{${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"]} = m;})}catch(e){await message.reply(\`${LiteGraph.parse(this.properties["Message"]||' ')}\`,{files:[${this.properties["Attachments"].replaceAll("${","").replaceAll("}","")}],flags:Number(\`${this.actions["Author Only?(for slash commands)"]}\`.replace("true",64)),components:[${this.properties["Button Rows"].replaceAll("${","").replaceAll("}","")}],embeds:[${this.properties["Embeds"].replaceAll('${','').replaceAll('}','')}]}).then(m=>{${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"]} = m;})}`)
                            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Message",[{name:"id",value:"id"},{name:"author id",value:"author.id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}]) 
                        
                    }else{
                       
                        
                            appendFileSync(this.getInputData(0),`\ntry{await message.reply({files:[${this.properties["Attachments"].replaceAll("${","").replaceAll("}","")}],content:\`${LiteGraph.parse(this.properties["Message"]|| ' ')}\`,allowedMentions: { repliedUser: false }, components:[${this.properties["Button Rows"].replaceAll("${","").replaceAll("}","")}], embeds:[${this.properties["Embeds"].replaceAll('${','').replaceAll('}','')}]})}catch(e){await message.reply(\`${LiteGraph.parse(this.properties["Message"]||' ')}\`,{files:[${this.properties["Attachments"].replaceAll("${","").replaceAll("}","")}],flags:Number(\`${this.actions["Author Only?(for slash commands)"]}\`.replace("true",64)),components:[${this.properties["Button Rows"].replaceAll("${","").replaceAll("}","")}], embeds:[${this.properties["Embeds"].replaceAll('${','').replaceAll('}','')}]})}`)
                        
                    }
                
               


                        
                
            this.trigger("Action");
            }
    }
    LiteGraph.registerNodeType("Message Action/Reply", reply );

    function editreply(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Message", "Hi there!");
        this.addProperty("Embeds", ``);
        this.addProperty("Button Rows", ``);
        this.addProperty("Attachments", ``);
        this.addProperty("Save to variable", ``);
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    editreply.title = "Update Reply";
    editreply.prototype.onAction = function(){
        this.setOutputData(0, this.getInputData(0));    


  
            if(this.getOutputData(0)){
                

                 
                       
                        if(this.properties["Save to variable"].trim()){
                            appendFileSync(this.getInputData(0),`\nawait message.reply(\`${LiteGraph.parse(this.properties["Message"]||' ')}\`,{type:7,files:[${this.properties["Attachments"].replaceAll("${","").replaceAll("}","")}],components:[${this.properties["Button Rows"].replaceAll("${","").replaceAll("}","")}], embeds:[${this.properties["Embeds"].replaceAll('${','').replaceAll('}','')}]}).then(m=>{${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = m;})`)
                        }else{
                            appendFileSync(this.getInputData(0),`\nawait message.reply(\`${LiteGraph.parse(this.properties["Message"]||' ')}\`,{type:7,files:[${this.properties["Attachments"].replaceAll("${","").replaceAll("}","")}],components:[${this.properties["Button Rows"].replaceAll("${","").replaceAll("}","")}], embeds:[${this.properties["Embeds"].replaceAll('${','').replaceAll('}','')}]})`)
                        }
                        
                        LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Message",[{name:"id",value:"id"},{name:"author id",value:"author.id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}])
                
               


                        
                
            this.trigger("Action");
            }
    }
    LiteGraph.registerNodeType("Message Action/Update Reply", editreply );

function parsetruefalse(value){
 if(value == 'true'){
     value = 64
 }
 return value
}



    function svideo(){
        this.addProperty("Video","");
        this.addProperty("Save to variable","");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    svideo.title = "Search Video";
    svideo.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`
            if((await require('scrape-youtube').default.search(\`${LiteGraph.parse(this.properties["Video"])}\`)).videos[0]){
                (await require('scrape-youtube').default.search(\`${LiteGraph.parse(this.properties["Video"])}\`)).videos.forEach(e=>{
                    e.constructor.prototype.toString = () => {
                        return require('lossless-json').stringify(e, client.circularReplacer())
                    }
                })
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = (await require('scrape-youtube').default.search(\`${LiteGraph.parse(this.properties["Video"])}\`)).videos
            }else{
                (await require('scrape-youtube').default.search((await require('spotify-url-info').getPreview(\`${LiteGraph.parse(this.properties["Video"])}\`)).title + ' by ' + (await require('spotify-url-info').getPreview(\`${LiteGraph.parse(this.properties["Video"])}\`)).artists[0].name)).videos.forEach(e=>{
                    e.constructor.prototype.toString = () => {
                        return require('lossless-json').stringify(e, client.circularReplacer())
                    }
                })
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = (await require('scrape-youtube').default.search((await require('spotify-url-info').getPreview(\`${LiteGraph.parse(this.properties["Video"])}\`)).title + ' by ' + (await require('spotify-url-info').getPreview(\`${LiteGraph.parse(this.properties["Video"])}\`)).artists[0].name)).videos
            }`);
            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Array',[{name:"size",value:"length"}]) 
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Search Video", svideo );


    function svideol(){
        this.addProperty("Video","");
        this.addProperty("Save to variable","");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    svideol.title = "Get Video Link";
    svideol.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = ${LiteGraph.parse(this.properties["Video"]).replaceAll('${','').replaceAll("}",'')}.url || ${LiteGraph.parse(this.properties["Video"]).replaceAll('${','').replaceAll("}",'')}.link;`);
            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Video Link') 
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Get Video Link", svideol );

    function videotitle(){
        this.addProperty("Link","");
        this.addProperty("Save to variable","");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    videotitle.title = "Get Video Title";
    videotitle.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait require('ytdl-core').getInfo(\`${LiteGraph.parse(this.properties["Link"])}\`).then(a=>{
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} =  a.videoDetails.title;
            });`);
          LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`, "Video Title")
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Get Video Title", videotitle );

    function videodesc(){
        this.addProperty("Link","");
        this.addProperty("Save to variable","");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    videodesc.title = "Get Video Desc";
    videodesc.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait require('ytdl-core').getInfo(\`${LiteGraph.parse(this.properties["Link"])}\`).then(a=>{
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} =  a.videoDetails.description;
            });`);
            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`, "Video Desc")
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Get Video Desc", videodesc );

    function videoaut(){
        this.addProperty("Link","");
        this.addProperty("Save to variable","");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.size = [170,27];
        this.widgets_up = true; 
    }
    videoaut.title = "Get Song Author";
    videoaut.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
         
              appendFileSync(this.getInputData(0),`\nawait require('ytdl-core').getInfo(\`${this.properties["Link"]}\`).then(a=>{
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} =  a.videoDetails.author.name;
            });`);
          LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`, "Song Author")
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Get Song Author", videoaut );

    function videolyrics(){
        this.addProperty("Song title","");
        this.addProperty("Song author","");
        this.addProperty("Save to variable","");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.size = [170,27];
        this.widgets_up = true; 
    }
    videolyrics.title = "Get Song Lyrics";
    videolyrics.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"]} = await require('lyrics-finder')(\`${this.properties["Song author"]}\`,\`${this.properties["Song title"]}\`)`);
          LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`, "Song Lyrics")
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Get Song Lyrics", videolyrics );


    function playvideo(){
        this.addProperty("Link","");
        this.addProperty("Guild ID","");
        this.addProperty("Channel","");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    playvideo.title = "Play Web Video";
    playvideo.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  
  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait client.player.play(message, {
                search: \`${LiteGraph.parse(this.properties["Link"])}\`
            }, \`${this.properties["Guild ID"]}\`, \`${this.properties["Channel"]}\`)`);
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Play Web Video", playvideo );


    function playplaylist(){
        this.addProperty("Link","");
        this.addProperty("Guild ID","");
        this.addProperty("Channel","");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    playplaylist.title = "Play Web List";
    playplaylist.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  
  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait client.player.playlist(message, {
                search: \`${LiteGraph.parse(this.properties["Link"])}\`
            }, \`${this.properties["Guild ID"]}\`, \`${this.properties["Channel"]}\`)`);
            }
            this.trigger("Action");
    }
    LiteGraph.registerNodeType("Music Action/Play Web List", playplaylist );

    function setfilters(){
        this.addProperty("Filters","bassboost, surround");
        this.addProperty("",`<div class="card" >
        <div class="container">
        <h2 >Filters</h2>
        <h3 >3D${LiteGraph.insert_button("3D")}</h3>
        <h3 >Bassboost${LiteGraph.insert_button("Bassboost")}</h3>
        <h3 >Echo${LiteGraph.insert_button("Echo")}</h3>
        <h3 >Flanger${LiteGraph.insert_button("Flanger")}</h3>
        <h3 >Gate${LiteGraph.insert_button("Gate")}</h3>
        <h3 >Haas${LiteGraph.insert_button("Haas")}</h3>
        <h3 >Karaoke${LiteGraph.insert_button("Karaoke")}</h3>
        <h3 >Nightcore${LiteGraph.insert_button("Nightcore")}</h3>
        <h3 >Reverse${LiteGraph.insert_button("Reverse")}</h3>
        <h3 >Vaporwave${LiteGraph.insert_button("Vaporwave")}</h3>
        <h3 >Mcompand${LiteGraph.insert_button("Mcompand")}</h3>
        <h3 >Phaser${LiteGraph.insert_button("Phaser")}</h3>
        <h3 >Tremolo${LiteGraph.insert_button("Tremolo")}</h3>
        <h3 >Surround${LiteGraph.insert_button("Surround")}</h3>
        <h3 >Earwax${LiteGraph.insert_button("Earwax")}</h3>
        </div>
        </div>`,"list")
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    setfilters.title = "Set Filters";
    setfilters.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  
  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait new Promise(res => setTimeout(res, Number(\`1\`)*1000))\nawait client.player.setFilters(message, []);\nawait client.player.setFilters(message, \`${this.properties["Filters"]}\`?\`${this.properties["Filters"]}\`.split(",").map(item => item.trim()):[]);`);
            }
            this.trigger("Action");
    }
    LiteGraph.registerNodeType("Music Action/Set Filters", setfilters );


    function  addtqueue(){
        this.addProperty("Link","");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    addtqueue.title = "Add To Queue";
    addtqueue.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait client.player.addToQueue(message, \`${LiteGraph.parse(this.properties["Link"])}\`);`);
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Add To Queue", addtqueue );

    function nowplaying(){
        this.addProperty("Save to variable","");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    nowplaying.title = "Now Playing";
    nowplaying.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll('}',"")} = await client.player.nowPlaying(message);`);
            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Song")
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Now Playing", nowplaying );


    function getqueue(){
        this.addProperty("Save to variable","");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    getqueue.title = "Get Queue";
    getqueue.prototype.onAction = function()
    {
            this.setOutputData(0, this.getInputData(0));  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nif(client.player.getQueue(message)){
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll('}',"")} = await client.player.getQueue(message).songs;
            }else{
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll('}',"")} = [];
            }`);
            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Queue")
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Get Queue", getqueue );

    function pause(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    pause.title = "Pause Song";
    pause.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait client.player.pause(message);`);
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Pause Song", pause );

    function resume(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    resume.title = "Resume Song";
    resume.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait client.player.resume(message);`);
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Resume Song", resume );

    function skip(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    skip.title = "Skip Song";
    skip.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait client.player.skip(message);`);
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Skip Song", skip );

    function clearqueue(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    clearqueue.title = "Clear Queue";
    clearqueue.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait client.player.clearQueue(message);`);
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Clear Queue", clearqueue );

    function removequeue(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addProperty("ID","the id of the song")
        this.widgets_up = true; 
    }
    removequeue.title = "Remove Queue";
    removequeue.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait client.player.remove(message, parseInt(\`${LiteGraph.parse(this.properties['ID'])}\`)-1);`);
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Remove Queue", removequeue );

    function stopsong(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    stopsong.title = "Stop Song";
    stopsong.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait client.player.stop(message);`);
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Stop Song", stopsong );

    function loopsong(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addProperty("", "<div class='property'><span class='property_name'>Value</span><input id='Value' type='checkbox' checked='true'></div>","html");
        this.widgets_up = true; 
    }
    loopsong.title = "Loop Song";
    loopsong.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait client.player.toggleLoop(message, \`${this.actions["Value"]}\`);`);
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Loop Song", loopsong );

    function shufflequeue(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    shufflequeue.title = "Shuffle Queue";
    shufflequeue.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait client.player.shuffle(message);`);
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Shuffle Queue", shufflequeue );

    function loopqueue(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addProperty("", "<div class='property'><span class='property_name'>Value</span><input id='Value' type='checkbox' checked='true'></div>","html");
        this.widgets_up = true; 
    }
    loopqueue.title = "Loop Queue";
    loopqueue.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait client.player.toggleQueueLoop(message, \`${this.actions["Value"]}\`);`);
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Loop Queue", loopqueue );

    function setvolume(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addProperty("Volume",'');
        this.widgets_up = true; 
    }
    setvolume.title = "Set Volume";
    setvolume.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait client.player.setVolume(message, \`${LiteGraph.parse(this.properties["Volume"])}\`);`);
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Set Volume", setvolume );

    function musicbar(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addProperty("Size",'15');
        this.addProperty("Block behind arrow",'=');
        this.addProperty("Arrow",'>');
        this.addProperty("Block after arrow",'-');
        this.addProperty("Save to variable",'progressBar');
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true; 
    }
    musicbar.title = "Music Bar";
    musicbar.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\n
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = await client.player.createProgressBar(message, {
                size: \`${LiteGraph.parse(this.properties["Size"])}\`,
                block: \`${LiteGraph.parse(this.properties["Block after arrow"])}\`,
                prevblock: \`${LiteGraph.parse(this.properties["Block behind arrow"])}\`,
                arrow: \`${LiteGraph.parse(this.properties["Arrow"])}\`
            });`);
            }
            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Progress Bar");
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Music Action/Music Bar", musicbar );

    function isplaying(){
        this.addOutput("true", LiteGraph.EVENT);
        this.addOutput("false", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    isplaying.title = "Is Playing";
    isplaying.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  
            this.setOutputData(1, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nif(await client.player.isPlaying(message)){`);
            }
            this.trigger("true");
            appendFileSync(this.getInputData(0),`\n}else{`);
            this.trigger("false");
            appendFileSync(this.getInputData(0),`\n}`);

    }
    LiteGraph.registerNodeType("Music Action/Is Playing", isplaying );

   


    function drimg()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Image var", "image variable");
        this.addProperty("Image", "image to draw");
        this.addProperty("Width", "width of image");
        this.addProperty("Height", "height of image");
        this.addProperty("Position-x", "horizontal position of image");
        this.addProperty("Position-y", "vertical position of image");
        
        this.widgets_up = true;
    }
    
    //name to show
    drimg.title = "Draw Image";
    
    //function to call when the node is executed
    drimg.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  




             
            if(this.getOutputData(0)){
        



    appendFileSync(this.getInputData(0),`\n await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').drawImage(await client.canvas.loadImage(`+"`"+LiteGraph.parse(this.properties["Image"]).replaceAll("avatarURL()","avatarURL({format: 'png'})")+"`"+`),${LiteGraph.parse(this.properties["Position-x"]).replaceAll("${","").replaceAll("}","")},${LiteGraph.parse(this.properties["Position-y"]).replaceAll("${","").replaceAll("}","")},${LiteGraph.parse(this.properties["Width"]).replaceAll("${","").replaceAll("}","").replaceAll("px","")},${LiteGraph.parse(this.properties["Height"]).replaceAll("${","").replaceAll("}","").replaceAll("px","")});`);

            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Image Action/Draw Image", drimg );


    function drtxt()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Image var", "image variable");
        this.addProperty("Font Color", "color of text", "color");
        this.addProperty("Font", "font name");
        this.addProperty("Font Size", "size of font");
        this.addProperty("Text", "text to draw");
        this.addProperty("Align text", "<option selected='selected'>Left</option><option>Center</option><option>Right</option>","dropdown");
        this.addProperty("Position-x", "horizontal position");
        this.addProperty("Position-y", "vertical position");
        this.widgets_up = true;
    }
    
    //name to show
    drtxt.title = "Draw Text";
    
    //function to call when the node is executed
    drtxt.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  




             
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`\n${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').textAlign = \`${LiteGraph.parse_dropdown(this.properties["Align text"]).toLowerCase()}\`;
        ${this.properties["Image var"].replaceAll("${","").replaceAll("global.","").replaceAll("}","")}.getContext('2d').font = client.measure(${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}, \`${LiteGraph.parse(this.properties["Text"])}\`, \`${LiteGraph.parse(this.properties["Font Size"]).replaceAll("px","").replaceAll("%","")}px\`,\`${LiteGraph.parse(this.properties["Font"])}\`, \`${LiteGraph.parse(this.properties["Position-x"])}\`);
        ${this.properties["Image var"].replaceAll("${","").replaceAll("global.","").replaceAll("}","")}.getContext('2d').fillStyle = `+"`"+`${LiteGraph.parse(this.properties["Font Color"])}`+"`"+`;
        await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').fillText(`+"`"+`${LiteGraph.parse(this.properties["Text"])}`+"`"+","+"`"+`${LiteGraph.parse(this.properties["Position-x"])}`+"`"+","+"`"+`${LiteGraph.parse(this.properties["Position-y"])}`+"`"+`);`);

            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Image Action/Draw Text", drtxt );






    function mrtxt()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Image var", "image variable");
        this.addProperty("Font", "font name");
        this.addProperty("Font Size", "size of font");
        this.addProperty("Text", "text to measure");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    mrtxt.title = "Measure Text";
    
    //function to call when the node is executed
    mrtxt.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  




             
            if(this.getOutputData(0)){
        








        appendFileSync(this.getInputData(0),`\n${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').font = `+"`"+`${LiteGraph.parse(this.properties["Font Size"]).replaceAll("px","").replaceAll("%","")}px "${LiteGraph.parse(this.properties["Font"])}"`+"`"+`;
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').measureText(`+"`"+LiteGraph.parse(this.properties["Text"])+"`"+`).width;`);
        LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'width')
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Image Action/Measure Text", mrtxt );



    

    function drarc()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Image var", "image variable");
        this.addProperty("Position-x", "horizontal position");
        this.addProperty("Position-y", "vertical position");
        this.addProperty("Radius", "radius of arc");
        this.addProperty("Starting Angle", "0");
        this.addProperty("Ending Angle", "Math.PI * 2");
        this.addProperty("", "<div class='property'><span class='property_name'>Counter-clockwise</span><input id='Counter-clockwise' type='checkbox' ></div>","html");
        this.addProperty("", "<div class='property'><span class='property_name'>Clip</span><input id='Clip' type='checkbox' ></div>","html");
        this.addProperty("Stroke Style", "#000000", "color");
        this.addProperty("Line Width", "10");
        this.widgets_up = true;
    }
    
    //name to show
    drarc.title = "Draw Arc";
    
    //function to call when the node is executed
    drarc.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  




             
            if(this.getOutputData(0)){
        


        appendFileSync(this.getInputData(0),`\nif(\`${this.actions["Clip"]}\`.toLowerCase().trim() == "true")
        {await ${this.properties["Image var"].replaceAll("${","").replaceAll("global.","").replaceAll("}","")}.getContext('2d').beginPath();
       await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').`+`arc(${LiteGraph.parse(this.properties["Position-x"]).replaceAll("${","").replaceAll("}","")}, ${LiteGraph.parse(this.properties["Position-y"]).replaceAll("${","").replaceAll("}","")}, ${LiteGraph.parse(this.properties["Radius"]).replaceAll("${","").replaceAll("}","")}, ${LiteGraph.parse(this.properties["Starting Angle"]).replaceAll("${","").replaceAll("}","")}, ${LiteGraph.parse(this.properties["Ending Angle"]).replaceAll("${","").replaceAll("}","")}, ${this.actions["Counter-clockwise"]});
       await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').closePath();
       await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').clip();
        }else{
            await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').beginPath();
            await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').`+`arc(${LiteGraph.parse(this.properties["Position-x"]).replaceAll("${","").replaceAll("}","")}, ${LiteGraph.parse(this.properties["Position-y"]).replaceAll("${","").replaceAll("}","")}, ${LiteGraph.parse(this.properties["Radius"]).replaceAll("${","").replaceAll("}","")}, ${LiteGraph.parse(this.properties["Starting Angle"]).replaceAll("${","").replaceAll("}","")}, ${LiteGraph.parse(this.properties["Ending Angle"]).replaceAll("${","").replaceAll("}","")}, ${this.actions["Counter-clockwise"]});
            await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').closePath();
            ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').strokeStyle = `+"`"+LiteGraph.parse(this.properties["Stroke Style"])+"`"+`
            ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').lineWidth = `+"`"+LiteGraph.parse(this.properties["Line Width"])+"`"+`
            await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').stroke();
        }`);
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Image Action/Draw Arc", drarc );


    function drrect()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Image var", "image variable");
        this.addProperty("Width", "width of rectangle");
        this.addProperty("Height", "height of rectangle");
        this.addProperty("Position-x", "horizontal position");
        this.addProperty("Position-y", "vertical position");
        this.addProperty("", "<div class='property'><span class='property_name'>Clip</span><input id='Clip' type='checkbox' ></div>","html");
        this.addProperty("Stroke Style", "#000000", "color");
        this.addProperty("Line Width", "10");
        this.widgets_up = true;
    }
    
    //name to show
    drrect.title = "Draw Rect";
    
    //function to call when the node is executed
    drrect.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  




             
            if(this.getOutputData(0)){
        


       
        appendFileSync(this.getInputData(0),`\nif(\`${this.actions["Clip"]}\`.toLowerCase().trim() == "true"){
            await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').beginPath();
       await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').`+`rect(${LiteGraph.parse(this.properties["Position-x"]).replaceAll("${","").replaceAll("}","")}, ${LiteGraph.parse(this.properties["Position-y"]).replaceAll("${","").replaceAll("}","")}, ${LiteGraph.parse(this.properties["Width"]).replaceAll("${","").replaceAll("}","")}, ${LiteGraph.parse(this.properties["Height"]).replaceAll("${","").replaceAll("}","")});
       await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').closePath();
       await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').clip();
    }else{
        await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').beginPath();
            await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').`+`rect(${LiteGraph.parse(this.properties["Position-x"]).replaceAll("${","").replaceAll("}","")}, ${LiteGraph.parse(this.properties["Position-y"]).replaceAll("${","").replaceAll("}","")}, ${LiteGraph.parse(this.properties["Width"]).replaceAll("${","").replaceAll("}","")}, ${LiteGraph.parse(this.properties["Height"]).replaceAll("${","").replaceAll("}","")});
            await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').closePath();
            ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').strokeStyle = `+"`"+LiteGraph.parse(this.properties["Stroke Style"])+"`"+`
            ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').lineWidth = `+"`"+LiteGraph.parse(this.properties["Line Width"])+"`"+`
            await ${this.properties["Image var"].replaceAll("${","").replaceAll("}","")}.getContext('2d').stroke();
    }`);
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Image Action/Draw Rect", drrect );




    function msgcol()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addOutput("on end", LiteGraph.EVENT);
        this.addProperty("Channel", "channel to listen to");
        this.addProperty("Seconds", "type 0 or infinite or leave empty if you want to listen for message forever");
        this.addProperty("User ID", "all");
        this.addProperty("Max messages", "type 0 or infinite or leave empty if you want no limit");
        this.addProperty("Message Content", "");
        this.addProperty("Save message to variable", "collectedMsg");
        this.addProperty("Save end reason to variable", "reason");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
        this.size = [175, 46]
    }
    
    //name to show
    msgcol.title = "Message Listener";
    
    //function to call when the node is executed
    msgcol.prototype.onAction = function(action, param)
    {
        
 
            this.setOutputData(0, this.getInputData(0));  
            this.setOutputData(1, this.getInputData(0));  




            if(this.getOutputData(0)){
            
            
            if(this.properties["Message Content"].trim()){
            if(this.properties["Max messages"].toLowerCase() == ('0'||'infinite'||'')){
                if(this.properties["User ID"].toLowerCase() == "all"){
                    if(this.properties["Seconds"].toLowerCase() == ('0'||'infinite'||'')){
                        appendFileSync(this.getInputData(0),`
                            await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot&&m.content==\`${LiteGraph.parse(this.properties["Message Content"])}\`}).on('collect',async m=>{
                            if(m.author.bot) return;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }else{
                        appendFileSync(this.getInputData(0),`
                        await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot&&m.content==\`${LiteGraph.parse(this.properties["Message Content"])}\`, time: Number(\`${LiteGraph.parse(this.properties["Seconds"])}\`) * 1000}).on('collect',async m=>{
                            if(m.author.bot) return;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }
                
            }else{
                
                    if(this.properties["Seconds"].toLowerCase() == ('0'||'infinite'||'')){
                        appendFileSync(this.getInputData(0),`
                            await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot&&m.content==\`${LiteGraph.parse(this.properties["Message Content"])}\`&&m.author.id==\`${this.properties["User ID"]}\`}).on('collect',async m=>{
                            if(m.author.bot) return;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }else{
                        appendFileSync(this.getInputData(0),`
                            await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot&&m.content==\`${LiteGraph.parse(this.properties["Message Content"])}\`&&m.author.id==\`${this.properties["User ID"]}\`, time: Number(\`${LiteGraph.parse(this.properties["Seconds"])}\`) * 1000}).on('collect',async m=>{
                            if(m.author.bot) return;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }
                
            }
            }else{
                if(this.properties["User ID"].toLowerCase() == "all"){
                    if(this.properties["Seconds"].toLowerCase() == ('0'||'infinite'||'')){
                        appendFileSync(this.getInputData(0),`
                            await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot&&m.content==\`${LiteGraph.parse(this.properties["Message Content"])}\`,max:Number(\`${this.properties["Max messages"]}\`)}).on('collect',async m=>{
                            if(m.author.bot) return;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }else{
                        appendFileSync(this.getInputData(0),`
                        await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot&&m.content==\`${LiteGraph.parse(this.properties["Message Content"])}\`,max:Number(\`${this.properties["Max messages"]}\`), time: Number(\`${LiteGraph.parse(this.properties["Seconds"])}\`) * 1000}).on('collect',async m=>{
                            if(m.author.bot) return;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }
                
            }else{
                
                    if(this.properties["Seconds"].toLowerCase() == ('0'||'infinite'||'')){
                        appendFileSync(this.getInputData(0),`
                            await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot&&m.content==\`${LiteGraph.parse(this.properties["Message Content"])}\`&&m.author.id==\`${this.properties["User ID"]}\`,max:Number(\`${this.properties["Max messages"]}\`)}).on('collect',async m=>{
                            if(m.author.bot) return;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }else{
                        appendFileSync(this.getInputData(0),`
                            await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot&&m.content==\`${LiteGraph.parse(this.properties["Message Content"])}\`&&m.author.id==\`${this.properties["User ID"]}\`,max:Number(\`${this.properties["Max messages"]}\`),time: Number(\`${LiteGraph.parse(this.properties["Seconds"])}\`) * 1000}).on('collect',async m=>{
                            if(m.author.bot) return;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }
                
            }
            }
        }else{
            if(this.properties["Max messages"].toLowerCase() == ('0'||'infinite'||'')){
                if(this.properties["User ID"].toLowerCase() == "all"){
                    if(this.properties["Seconds"].toLowerCase() == ('0'||'infinite'||'')){
                        appendFileSync(this.getInputData(0),`
                            await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot}).on('collect',async m=>{
                            if(m.author.bot) return;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }else{
                        appendFileSync(this.getInputData(0),`
                        await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot, time: Number(\`${LiteGraph.parse(this.properties["Seconds"])}\`) * 1000}).on('collect',async m=>{
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }
                
            }else{
                
                    if(this.properties["Seconds"].toLowerCase() == ('0'||'infinite'||'')){
                        appendFileSync(this.getInputData(0),`
                            await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot&&m.author.id==\`${this.properties["User ID"]}\`}).on('collect',async m=>{
                            if(m.author.bot) return;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }else{
                        appendFileSync(this.getInputData(0),`
                            await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot&&m.author.id==\`${this.properties["User ID"]}\`, time: Number(\`${LiteGraph.parse(this.properties["Seconds"])}\`) * 1000}).on('collect',async m=>{
                            if(m.author.bot) return;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }
                
            }
            }else{
                if(this.properties["User ID"].toLowerCase() == "all"){
                    if(this.properties["Seconds"].toLowerCase() == ('0'||'infinite'||'')){
                        appendFileSync(this.getInputData(0),`
                            await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot,max:Number(\`${this.properties["Max messages"]}\`)}).on('collect',async m=>{
                            if(m.author.bot) return;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }else{
                        appendFileSync(this.getInputData(0),`
                        await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot,max:Number(\`${this.properties["Max messages"]}\`), time: Number(\`${LiteGraph.parse(this.properties["Seconds"])}\`) * 1000}).on('collect',async m=>{
                            if(m.author.bot) return;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }
                
            }else{
                
                    if(this.properties["Seconds"].toLowerCase() == ('0'||'infinite'||'')){
                        appendFileSync(this.getInputData(0),`
                            await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot&&m.author.id==\`${this.properties["User ID"]}\`,max:Number(\`${this.properties["Max messages"]}\`)}).on('collect',async m=>{
                            if(m.author.bot) return;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }else{
                        appendFileSync(this.getInputData(0),`
                            await client.channels.cache.find(ch => ch.name == \`${this.properties["Channel"]}\` || ch.id == \`${this.properties["Channel"]}\`).createMessageCollector({filter:m => !m.author.bot&&m.author.id==\`${this.properties["User ID"]}\`,max:Number(\`${this.properties["Max messages"]}\`),time: Number(\`${LiteGraph.parse(this.properties["Seconds"])}\`) * 1000}).on('collect',async m=>{
                            if(m.author.bot) return;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"].replaceAll('${','').replaceAll('}','')} = m;`);
                    }
                
            }
            }
        }
            
                    this.trigger("Action");
                    appendFileSync(this.getInputData(0),`\n})
                    .on("end", async (reason) =>{
                        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save end reason to variable"].replaceAll('${','').replaceAll('}','')} = reason;`)
                    this.trigger("on end");
                    appendFileSync(this.getInputData(0),`\n})`)
                }
                appendFileSync(this.getInputData(0),`\n`);
                LiteGraph.save_var(this.properties["Save message to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Message",[{name:"id",value:"id"},{name:"author id",value:"author.id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}])
                LiteGraph.save_var(this.properties["Save end reason to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Text") 
    }
    
    //register in the system
    LiteGraph.registerNodeType("Message Action/Message Listener", msgcol );

    function deletemessageid()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Channel", "message channel");
        this.addProperty("Message ID", "message to delete");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.size = [170, 27];
        this.widgets_up = true;
    }
    
    //name to show
    deletemessageid.title = "Delete Message";
    
    //function to call when the node is executed
    deletemessageid.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  




            if(this.getOutputData(0)){
            
            
           
            
                appendFileSync(this.getInputData(0),`\nawait client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).messages.cache.get(`+"`"+LiteGraph.parse(this.properties["Message ID"])+"`"+`).delete();`);
            
        }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Message Action/Delete Message", deletemessageid );

    function editmessage()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Channel", "message channel");
        this.addProperty("Message ID", "message to edit");
        this.addProperty("New message", "");
        this.addProperty("New Embeds", "")
        this.addProperty("Button Rows", "");
        this.addProperty("Attachments","")
        this.addProperty("Save to variable","")
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
    }
    
    //name to show
    editmessage.title = "Edit Message";
    
    //function to call when the node is executed
    editmessage.prototype.onAction = function(action, param)
    {
        
 
            this.setOutputData(0, this.getInputData(0));  




            if(this.getOutputData(0)){
            
            
                if(this.properties["Save to variable"].trim()){
                    appendFileSync(this.getInputData(0),"\n"+`await client.channels.cache.find(ch=>ch.name==\`${LiteGraph.parse(this.properties["Channel"])}\`||ch.id==\`${LiteGraph.parse(this.properties["Channel"])}\`).messages.cache.get(\`${LiteGraph.parse(this.properties["Message ID"])}\`).edit({content:\`${LiteGraph.parse(this.properties["New message"]||' ')}\`,embeds:[${this.properties["New Embeds"].replaceAll("${","").replaceAll("}","")}],components:[${this.properties["Button Rows"].replaceAll("${","").replaceAll("}","")}],files:[${this.properties["Attachments"].replaceAll("${","").replaceAll("}","")}],embed:null}).then(m=>{
                        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = m;
                    });`);
                    LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Message",[{name:"id",value:"id"},{name:"author id",value:"author.id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}])
                }else{
                    appendFileSync(this.getInputData(0),"\n"+`await client.channels.cache.find(ch=>ch.name==\`${LiteGraph.parse(this.properties["Channel"])}\`||ch.id==\`${LiteGraph.parse(this.properties["Channel"])}\`).messages.cache.get(\`${LiteGraph.parse(this.properties["Message ID"])}\`).edit({content:\`${LiteGraph.parse(this.properties["New message"]||' ')}\`,embeds:[${this.properties["New Embeds"].replaceAll("${","").replaceAll("}","")}],components:[${this.properties["Button Rows"].replaceAll("${","").replaceAll("}","")}],files:[${this.properties["Attachments"].replaceAll("${","").replaceAll("}","")}],embed:null});`);
                }
                }
                this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Message Action/Edit Message", editmessage );

    function sendmessageembed()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Color", "",'color');
        this.addProperty("Title", "", 'title');
        this.addProperty("URL", "");
        this.addProperty("Author Name", "",'authorname');
        this.addProperty("Author Avatar", "", 'authorurl');
        this.addProperty("Author URL", "");
        this.addProperty("Description", "\n\n\n\n\n", "description");
        this.addProperty("Thumbnail", "", 'thumb');
        this.addProperty("Image", "", "img");
        this.addProperty("Footer Text", "", 'fttxt');
        this.addProperty("Footer Image", "", 'ftimg');
        this.addProperty("Field Title", "", "ft");
        this.addProperty("Field Value", "", "fv");
        this.addProperty("", "<div class='property'><span class='property_name'>Inline</span><input id='Inline' type='checkbox' ></div>","html");
        this.addProperty("Timestamp", "false", "tsp");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addProperty('Preview:','','emb');
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
    }
    
    //name to show
    sendmessageembed.title = "Create Embed";
    

    //function to call when the node is executed
    sendmessageembed.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  




           
            if(this.getOutputData(0)){
            
            
            appendFileSync(this.getInputData(0),`\nif(\`${LiteGraph.parse(this.properties["Timestamp"])}\`.toLowerCase().trim() == "true"){`)
               
                
                    if(LiteGraph.parse(this.properties["Field Title"])&& LiteGraph.parse(this.properties["Field Value"])){
                        
                           
                        appendFileSync(this.getInputData(0),`
                        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = `+`new Discord.MessageEmbed().setColor(`+"`"+LiteGraph.parse(this.properties["Color"])+"`"+`)
                        .setTitle(`+"`"+LiteGraph.parse(this.properties["Title"])+"`"+`)
                        .setURL(`+"`"+LiteGraph.parse(this.properties["URL"])+"`"+`)
                        .setAuthor(`+"`"+LiteGraph.parse(this.properties["Author Name"])+"`"+`, `+"`"+LiteGraph.parse(this.properties["Author Avatar"])+"`"+`,\`${LiteGraph.parse(this.properties["Author URL"])}\`)
                        .setDescription(`+"`"+LiteGraph.parse(this.properties["Description"])+"`"+`)
                        .setThumbnail(`+"`"+LiteGraph.parse(this.properties["Thumbnail"])+"`"+`)
                        .setTimestamp(`+"new Date()"+`)
                .addField(`+"`"+LiteGraph.parse(this.properties["Field Title"])+"`"+`, `+"`"+LiteGraph.parse(this.properties["Field Value"])+"`"+`, ${this.actions["Inline"]})
           
                .setImage(`+"`"+LiteGraph.parse(this.properties["Image"])+"`"+`)
                .setFooter(`+"`"+LiteGraph.parse(this.properties["Footer Text"])+"`"+`, `+"`"+LiteGraph.parse(this.properties["Footer Image"])+"`"+`)
                .setTimestamp(`+"new Date()"+`)`);
                    
        }else{
           
            
                    appendFileSync(this.getInputData(0),`
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = `+`new Discord.MessageEmbed().setColor(`+"`"+LiteGraph.parse(this.properties["Color"])+"`"+`)
                    .setTitle(`+"`"+LiteGraph.parse(this.properties["Title"])+"`"+`)
                    .setURL(`+"`"+LiteGraph.parse(this.properties["URL"])+"`"+`)
                    .setAuthor(`+"`"+LiteGraph.parse(this.properties["Author Name"])+"`"+`, `+"`"+LiteGraph.parse(this.properties["Author Avatar"])+"`"+`,\`${LiteGraph.parse(this.properties["Author URL"])}\`)
                    .setDescription(`+"`"+LiteGraph.parse(this.properties["Description"])+"`"+`)
                    .setThumbnail(`+"`"+LiteGraph.parse(this.properties["Thumbnail"])+"`"+`)
            .setImage(`+"`"+LiteGraph.parse(this.properties["Image"])+"`"+`)
            .setFooter(`+"`"+LiteGraph.parse(this.properties["Footer Text"])+"`"+`, `+"`"+LiteGraph.parse(this.properties["Footer Image"])+"`"+`)
            .setTimestamp(`+"new Date()"+`)`);
                
            
        }
                
            }
            appendFileSync(this.getInputData(0),`\n}else{`)
           
           
            
                if(LiteGraph.parse(this.properties["Field Title"])&& LiteGraph.parse(this.properties["Field Value"])){
                    
                       
                    appendFileSync(this.getInputData(0),`
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = `+`new Discord.MessageEmbed().setColor(`+"`"+LiteGraph.parse(this.properties["Color"])+"`"+`)
                    .setTitle(`+"`"+LiteGraph.parse(this.properties["Title"])+"`"+`)
                    .setURL(`+"`"+LiteGraph.parse(this.properties["URL"])+"`"+`)
                    .setAuthor(`+"`"+LiteGraph.parse(this.properties["Author Name"])+"`"+`, `+"`"+LiteGraph.parse(this.properties["Author Avatar"])+"`"+`,\`${LiteGraph.parse(this.properties["Author URL"])}\`)
                    .setDescription(`+"`"+LiteGraph.parse(this.properties["Description"])+"`"+`)
                    .setThumbnail(`+"`"+LiteGraph.parse(this.properties["Thumbnail"])+"`"+`)
            .addField(`+"`"+LiteGraph.parse(this.properties["Field Title"])+"`"+`, `+"`"+LiteGraph.parse(this.properties["Field Value"])+"`"+`, ${this.actions["Inline"]})
       
            .setImage(`+"`"+LiteGraph.parse(this.properties["Image"])+"`"+`)
            .setFooter(`+"`"+LiteGraph.parse(this.properties["Footer Text"])+"`"+`, `+"`"+LiteGraph.parse(this.properties["Footer Image"])+"`"+`)
            .setTimestamp(`+"`"+LiteGraph.parse(this.properties["Timestamp"])+"`"+`)`);
                
    }else{
       
        
                appendFileSync(this.getInputData(0),`
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = `+`new Discord.MessageEmbed().setColor(`+"`"+LiteGraph.parse(this.properties["Color"])+"`"+`)
                .setTitle(`+"`"+LiteGraph.parse(this.properties["Title"])+"`"+`)
                .setURL(`+"`"+LiteGraph.parse(this.properties["URL"])+"`"+`)
                .setAuthor(`+"`"+LiteGraph.parse(this.properties["Author Name"])+"`"+`, `+"`"+LiteGraph.parse(this.properties["Author Avatar"])+"`"+`,\`${LiteGraph.parse(this.properties["Author URL"])}\`)
                .setDescription(`+"`"+LiteGraph.parse(this.properties["Description"])+"`"+`)
                .setThumbnail(`+"`"+LiteGraph.parse(this.properties["Thumbnail"])+"`"+`)
        .setImage(`+"`"+LiteGraph.parse(this.properties["Image"])+"`"+`)
        .setFooter(`+"`"+LiteGraph.parse(this.properties["Footer Text"])+"`"+`, `+"`"+LiteGraph.parse(this.properties["Footer Image"])+"`"+`)
        .setTimestamp(`+"`"+LiteGraph.parse(this.properties["Timestamp"])+"`"+`)`);
            
        
    }
            
        
        appendFileSync(this.getInputData(0),`\n}`)
            
            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Embed Object')
                this.trigger("Action");
           
    }
    
    //register in the system
    LiteGraph.registerNodeType("Message Action/Create Embed", sendmessageembed);


    function addfield()
    {
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Embed Object", "");
        this.addProperty("Field Title", "");
        this.addProperty("Field Value", "");
        this.addProperty("", "<div class='property'><span class='property_name'>Inline</span><input id='Inline' type='checkbox' ></div>","html");
        this.widgets_up = true;
    }
    
    //name to show
    addfield.title = "Add Field";
    
    //function to call when the node is executed
    addfield.prototype.onAction = function()
    {
       
            this.setOutputData(0, this.getInputData(0));
            if(this.getOutputData(0)){
        

    appendFileSync(this.getInputData(0),` \n${this.properties["Embed Object"].replaceAll('${',"").replaceAll("}","")}.addField(`+"`"+LiteGraph.parse(this.properties["Field Title"]||`\\u200b`)+"`"+`, `+"`"+LiteGraph.parse(this.properties["Field Value"]||`\\u200b`)+"`"+`, ${this.actions["Inline"]})`);

        
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Message Action/Add Field", addfield );


    function addrow(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addHTML("<style>#h::after {content: \" \";position: absolute;top: 50%;right: 100%;margin-top: -7px;border-width: 7px;border-style: solid;border-color: transparent rgba(0,0,0,0.9) transparent transparent;}</style><div class='property'><span class='property_name'>Components</span><span class='property_value' contenteditable='plaintext-only' onblur=\"document.getElementById('h').style.display='none'\" onfocus=\"document.getElementById('h').style.display='block'\" id='Components'></span><h id='h' style='display:none;border:1px solid black;font-size:15px;font-family:Arial;background-color:rgba(0,0,0,0.5);padding:7px;border-radius:4px;margin-top:-8%;position:absolute;left:101%;width:fit-content;color:#cccccc;backdrop-filter: blur(5px);'><i class=\"fa fa-info-circle\" style=\"font-size:18px;color:#cccccc;background:rgba(0,0,0,0.5);\"></i> Button/select menu variables separated by commas</h></div>")
        this.addProperty("Save to variable", ``);
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
        this.size = [150,27]
    }
    addrow.title = "Add Button Row";
    addrow.prototype.onAction = function(){
        this.setOutputData(0, this.getInputData(0));    
  
        if(this.getOutputData(0)){
            const path = require('path')
            
            appendFileSync(this.getInputData(0),`
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"]} = new Discord.MessageActionRow()
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"]}.addComponents([${this.actions["Components"].replaceAll("${","").replaceAll("}","")}])`)
          
        LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Button Row')
        this.trigger("Action");

        }
    }
    LiteGraph.registerNodeType("Message Action/Add Button Row", addrow );


    function addbtn(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addOutput("on click", LiteGraph.EVENT);
        this.addProperty("Name", "test");
        this.addProperty("Custom ID", "test");
        this.addProperty("Style", `<option selected='selected'>Blurple</option><option>Gray</option><option>Green</option><option>Red</option>`,'dropdown');
        this.addProperty("Emoji", ``);
        this.addProperty("URL", ``);
        this.addProperty("Disabled?", `false`);
        this.addProperty("Save to variable", ``);
        this.addProperty("Save user that clicked as", `clickedUser`);
        this.addProperty("Save button's message as", `btnMsg`);
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    addbtn.title = "Add Button";
    addbtn.prototype.onAction = function(){
        this.setOutputData(0, this.getInputData(0));  

        if(!global.commandsdata.find(c=>c.name== `${LiteGraph.botpath}\\components\\${this.properties["Custom ID"].replaceAll("/","").replaceAll("\\","") || this.properties["Name"].replaceAll("/","").replaceAll("\\","")}.js`)){
        if(this.getOutputData(0)){
            const path = require('path')
            
           
              if(this.properties["Emoji"].trim()){
                if(this.properties["URL"].trim()){
                    appendFileSync(this.getInputData(0),`
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"]} = new Discord.MessageButton()
                            .setLabel(\`${LiteGraph.parse(this.properties["Name"])}\`)
                            .setStyle(\`LINK\`)
                            .setEmoji(\`${LiteGraph.parse(this.properties["Emoji"])}\`)
                            .setDisabled(\`${this.properties["Disabled?"]}\`)
                            .setURL(\`${this.properties["URL"]}\`)`)
                }else{
                    appendFileSync(this.getInputData(0),`
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"]} = new Discord.MessageButton()
                            .setCustomId(\`${LiteGraph.parse(this.properties["Custom ID"]).replaceAll("/","").replaceAll("\\","")}\`)
                            .setLabel(\`${LiteGraph.parse(this.properties["Name"])}\`)
                            .setStyle(Number(\`${LiteGraph.parse_dropdown(this.properties["Style"]).replace("Blurple","1").replace("Gray","2").replace("Green","3").replace("Red","4")}\`))
                            .setEmoji(\`${LiteGraph.parse(this.properties["Emoji"])}\`)
                            .setDisabled(\`${this.properties["Disabled?"]}\`)
                    `)
                }
              }else{
                if(this.properties["URL"].trim()){
                    appendFileSync(this.getInputData(0),`
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"]} = new Discord.MessageButton()
                            .setLabel(\`${LiteGraph.parse(this.properties["Name"])}\`)
                            .setStyle(\`LINK\`)
                            .setDisabled(\`${this.properties["Disabled?"]}\`)
                            .setURL(\`${this.properties["URL"]}\`)
                    `)
                }else{
                    appendFileSync(this.getInputData(0),`
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"]} = new Discord.MessageButton()
                            .setCustomId(\`${LiteGraph.parse(this.properties["Custom ID"]).replaceAll("/","").replaceAll("\\","")}\`)
                            .setLabel(\`${LiteGraph.parse(this.properties["Name"])}\`)
                            .setStyle(Number(\`${LiteGraph.parse_dropdown(this.properties["Style"]).replace("Blurple","1").replace("Gray","2").replace("Green","3").replace("Red","4")}\`))
                            .setDisabled(\`${this.properties["Disabled?"]}\`)
                    `)
                }
              } 
              writeFileSync(`${LiteGraph.botpath}\\components\\${this.properties["Custom ID"].replaceAll("/","").replaceAll("\\","") || this.properties["Name"].replaceAll("/","").replaceAll("\\","")}.js`,`module.exports = {
                  name:\`${this.properties["Custom ID"].replaceAll("/","").replaceAll("\\","") || this.properties["Name"].replaceAll("/","").replaceAll("\\","")}\`,
                  async execute(Discord, client, message){
                    let server
                    let temp = {}
                    if(message.guild){
                        if(!client.servers.get(message.guild.id)){
                            client.servers.set(message.guild.id,{id:message.guild.id})
                        }
                        server = client.servers.get(message.guild.id)
                    }else{
                        if(!client.servers.get(message.channel.id)){
                            client.servers.set(message.channel.id,{id:message.channel.id})
                        }
                        server = client.servers.get(message.channel.id)
                    }
                    let dbwVars = {}
                    dbwVars["CommandAuthor"] = message.author;
                    dbwVars["CommandChannel"] = message.channel;
                    dbwVars["CommandGuild"] = message.guild;
                    dbwVars["Commands"] = client.commands.toJSON();
                    dbwVars["SlashCommands"] = client.slashcommands.toJSON();\ndbwVars["ContextMenus"] = client.contextmenus.toJSON();
                    dbwVars["Bot"] = client.user;
                    try{
                        dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.guild.id}prefix\`)||client.prefix
                    }catch{
                        dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.channel.id}prefix\`)||client.prefix
                    }
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save user that clicked as"]} = message.user
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save button's message as"]} = message.message`)
                  this.setOutputData(1, `${LiteGraph.botpath}\\components\\${this.properties["Custom ID"].replaceAll("/","").replaceAll("\\","") || this.properties["Name"].replaceAll("/","").replaceAll("\\","")}.js`);   
                  this.trigger('on click')
              LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Button")
              LiteGraph.save_var(this.properties["Save user that clicked as"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"User",[{name:"id",value:"id"},{name:"name",value:"username"},{name:"tag",value:"tag"},{name:"discriminator",value:"discriminator"},{name:"last message id",value:"lastMessageId"}])  
              LiteGraph.save_var(this.properties["Save button's message as"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Message",[{name:"id",value:"id"},{name:"author id",value:"author.id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}])
        this.trigger("Action");
        }
    }else{
        alert(`You already have a component named <b>${this.properties["Custom ID"] || this.properties["Name"]}</b>!`)
    }
    }
    let parseBooleanStyle = function(value, nullOnFailure = false){
        switch(value){
            case true:
            case 'true':
            case 1:
            case '1':
            case 'on':
            case 'yes':
                value = true;
                break;
            case false:
            case 'false':
            case 0:
            case '0':
            case 'off':
            case 'no':
                value = false;
                break;
            default:
                if(nullOnFailure){
                    value = null;
                }else{
                    value = false;
                }
                break;
        }
        return value;
    };
    LiteGraph.registerNodeType("Message Action/Add Button", addbtn );



    function addselectmenu(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addOutput("on select", LiteGraph.EVENT);
        this.addProperty("Name", "test");
        this.addProperty("Custom ID", "test");
        this.addProperty("Disabled?", `false`);
        this.addProperty("Min Selected", `1`);
        this.addProperty("Max Selected", `1`);
        this.addProperty("Save to variable", ``);
        this.addProperty("Save user that selected as", `selectedUser`);
        this.addProperty("Save select's message as", `selectMsg`);
        this.addProperty("Save selected values as", `selectedValues`);
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    addselectmenu.title = "Add Select";
    addselectmenu.prototype.onAction = function(){
        this.setOutputData(0, this.getInputData(0));  

        if(!global.commandsdata.find(c=>c.name== `${LiteGraph.botpath}\\components\\${this.properties["Custom ID"].replaceAll("/","").replaceAll("\\","")}.js`)){
        if(this.getOutputData(0)){
            const path = require('path')
            
           
                    appendFileSync(this.getInputData(0),`
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"]} = new Discord.MessageSelectMenu()
                    .setCustomId(\`${LiteGraph.parse(this.properties["Custom ID"]).replaceAll("/","").replaceAll("\\","")}\`)
                    .setPlaceholder(\`${LiteGraph.parse(this.properties["Name"])}\`)
                    .setDisabled(\`${this.properties["Disabled?"]}\`)
                    .setMinValues(Number(\`${this.properties["Min Selected"]}\`))
                    .setMaxValues(Number(\`${this.properties["Max Selected"]}\`))`)
                    writeFileSync(`${LiteGraph.botpath}\\components\\${this.properties["Custom ID"].replaceAll("/","").replaceAll("\\","")}.js`,`module.exports = {
                        name:\`${this.properties["Custom ID"].replaceAll("/","").replaceAll("\\","")}\`,
                        async execute(Discord, client, message){
                          let server
                          let temp = {}
                          if(message.guild){
                              if(!client.servers.get(message.guild.id)){
                                  client.servers.set(message.guild.id,{id:message.guild.id})
                              }
                              server = client.servers.get(message.guild.id)
                          }else{
                              if(!client.servers.get(message.channel.id)){
                                  client.servers.set(message.channel.id,{id:message.channel.id})
                              }
                              server = client.servers.get(message.channel.id)
                          }
                          let dbwVars = {}
                          dbwVars["CommandAuthor"] = message.author;
                          dbwVars["CommandChannel"] = message.channel;
                          dbwVars["CommandGuild"] = message.guild;
                          dbwVars["Commands"] = client.commands.toJSON();
                          dbwVars["SlashCommands"] = client.slashcommands.toJSON();\ndbwVars["ContextMenus"] = client.contextmenus.toJSON();
                          dbwVars["Bot"] = client.user;
                          try{
                              dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.guild.id}prefix\`)||client.prefix
                          }catch{
                              dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.channel.id}prefix\`)||client.prefix
                          }
                          ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save user that selected as"]} = message.user
                          ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save select's message as"]} = message.message
                          ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save selected values as"]} = message.data.values`)
                          this.setOutputData(1, `${LiteGraph.botpath}\\components\\${this.properties["Custom ID"].replaceAll("/","").replaceAll("\\","")}.js`);    
                          this.trigger('on select')
                    LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Select Menu")
                    LiteGraph.save_var(this.properties["Save user that selected as"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"User",[{name:"id",value:"id"},{name:"name",value:"username"},{name:"tag",value:"tag"},{name:"discriminator",value:"discriminator"},{name:"last message id",value:"lastMessageId"}]) 
                    LiteGraph.save_var(this.properties["Save select's message as"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Message",[{name:"id",value:"id"},{name:"author id",value:"author.id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}])
                    LiteGraph.save_var(this.properties["Save selected values as"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Array',[{name:"size",value:"length"}])
        this.trigger("Action");
        }
        }else{
            alert(`You already have a component named <b>${this.properties["Custom ID"]}</b>!`)
        }
    }
    LiteGraph.registerNodeType("Message Action/Add Select", addselectmenu );

    function addselectopt(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Select Menu Variable", ``);
        this.addProperty("Name", "test");
        this.addProperty("Value", "test");
        this.addProperty("Description", `test`);
        this.addProperty("Emoji ID", ``);
        this.addProperty("Default?", `false`);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
        this.size = [160,27]
    }
    addselectopt.title = "Add Select Option";
    addselectopt.prototype.onAction = function(){
        this.setOutputData(0, this.getInputData(0));  


        if(this.getOutputData(0)){
            const path = require('path')
            
           
              if(this.properties["Emoji ID"].trim()){
                    appendFileSync(this.getInputData(0),`\n${this.properties["Select Menu Variable"].replaceAll('${','').replaceAll('}','')}.addOptions({label:\`${LiteGraph.parse(this.properties["Name"])}\`,value:\`${LiteGraph.parse(this.properties["Value"])}\`,description:\`${LiteGraph.parse(this.properties["Description"])}\`,emoji:{id:\`${LiteGraph.parse(this.properties["Emoji ID"])}\`},default:client.parseBoolean(\`${LiteGraph.parse(this.properties["Default?"])}\`)})`)
              }else{
                    appendFileSync(this.getInputData(0),`\n${this.properties["Select Menu Variable"].replaceAll('${','').replaceAll('}','')}.addOptions({label:\`${LiteGraph.parse(this.properties["Name"])}\`,value:\`${LiteGraph.parse(this.properties["Value"])}\`,description:\`${LiteGraph.parse(this.properties["Description"])}\`,default:client.parseBoolean(\`${LiteGraph.parse(this.properties["Default?"])}\`)})`)
              }
        this.trigger("Action");
        }
    }
    LiteGraph.registerNodeType("Message Action/Add Select Option", addselectopt );


    


    function addreact(){
        
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Message ID", "message to add reaction to");
        this.addProperty("Channel", "the message channel");
        this.addProperty("Emoji", "emoji to add");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
    }
    addreact.title = "Add Reaction";
    
    //function to call when the node is executed
    addreact.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  




            if(this.getOutputData(0)){
            
                appendFileSync(this.getInputData(0),`\n await client.channels.cache.find(ch=>ch.name==\``+LiteGraph.parse(this.properties["Channel"])+"`|| ch.id == `"+LiteGraph.parse(this.properties["Channel"])+"`)"+`.messages.cache.get(`+"`"+LiteGraph.parse(this.properties["Message ID"])+"`"+`)`+".react(`"+this.properties['Emoji']+"`);");
            
        }
        this.trigger("Action");
   
    }
    
    //register in the system
    LiteGraph.registerNodeType("Reactions/Add Reaction", addreact );


    function remreact(){
        
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Message ID", "message to remove reaction");
        this.addProperty("Channel", "message channel");
        this.addProperty("User", "user to remove reaction from");
        this.addProperty("Emoji", "emoji to remove");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
        this.size =[170,27]
    }
    remreact.title = "Remove Reaction";
    
    //function to call when the node is executed
    remreact.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  




            if(this.getOutputData(0)){
            
                appendFileSync(this.getInputData(0),`\n await client.channels.cache.find(ch=>ch.name==\``+LiteGraph.parse(this.properties["Channel"])+"`|| ch.id == `"+LiteGraph.parse(this.properties["Channel"])+"`)"+`.messages.cache.get(`+"`"+LiteGraph.parse(this.properties["Message ID"])+"`"+`)`+`.reactions.resolve(`+"`"+`${LiteGraph.parse(this.properties["Emoji"])}`+"`"+`).users.remove(`+"`"+LiteGraph.parse(this.properties["User"])+"`"+`)`);
            
        }
        this.trigger("Action");
   
    }
    
    //register in the system
    LiteGraph.registerNodeType("Reactions/Remove Reaction", remreact );

    function remareact(){
        
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Message ID", "message to remove reaction");
        this.addProperty("Channel", "message channel");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
        this.size =[175,27]
    }
    remareact.title = "Remove All Reactions";
    
    //function to call when the node is executed
    remareact.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  




            if(this.getOutputData(0)){
            
                appendFileSync(this.getInputData(0),`\nawait client.channels.cache.find(ch=>ch.name==\``+LiteGraph.parse(this.properties["Channel"])+"`|| ch.id == `"+LiteGraph.parse(this.properties["Channel"])+"`)"+`.messages.cache.get(`+"`"+LiteGraph.parse(this.properties["Message ID"])+"`"+`)`+`.reactions.removeAll()`);
            
        }
        this.trigger("Action");
   
    }
    
    //register in the system
    LiteGraph.registerNodeType("Reactions/Remove All", remareact );




    function addroleperms()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Role", "role to set permissions to");
        this.addProperty("Permissions", "Send messages, View channels");
        this.addProperty("",`<div class="card" >
        <div class="container">
        <h2 >Permissions</h2>
        <h3 >Administrator${LiteGraph.insert_button("Administrator")}</h3>
        <h3 >Create instant invite${LiteGraph.insert_button("Create instant invite")}</h3>
        <h3 >Kick members${LiteGraph.insert_button("Kick members")}</h3>
        <h3 >Ban members${LiteGraph.insert_button("Ban members")}</h3>
        <h3 >Manage channels${LiteGraph.insert_button("Manage channels")}</h3>
        <h3 >Manage guild${LiteGraph.insert_button("Manage guild")}</h3>
        <h3 >Add reactions${LiteGraph.insert_button("Add reactions")}</h3>
        <h3 >View audit log${LiteGraph.insert_button("View audit log")}</h3>
        <h3 >Priority speaker${LiteGraph.insert_button("Priority speaker")}</h3>
        <h3 >Stream${LiteGraph.insert_button("Stream")}</h3>
        <h3 >View channels${LiteGraph.insert_button("View channels")}</h3>
        <h3 >Send messages${LiteGraph.insert_button("Send messages")}</h3>
        <h3 >Send tts messages${LiteGraph.insert_button("Send tts messages")}</h3>
        <h3 >Manage Messages${LiteGraph.insert_button(">Manage Messages")}</h3>
        <h3 >Embed links${LiteGraph.insert_button("Embed links")}</h3>
        <h3 >Attach files${LiteGraph.insert_button("Attach files")}</h3>
        <h3 >Read message history${LiteGraph.insert_button("Read message history")}</h3>
        <h3 >Mention everyone${LiteGraph.insert_button("Mention everyone")}</h3>
        <h3 >Use external emojis${LiteGraph.insert_button("Use external emojis")}</h3>
        <h3 >View guild insights${LiteGraph.insert_button("View guild insights")}</h3>
        <h3 >Connect${LiteGraph.insert_button("Connect")}</h3>
        <h3 >Speak${LiteGraph.insert_button("Speak")}</h3>
        <h3 >Mute members${LiteGraph.insert_button("Mute members")}</h3>
        <h3 >Deafen members${LiteGraph.insert_button("Deafen members")}</h3>
        <h3 >Move members${LiteGraph.insert_button("Move members")}</h3>
        <h3 >Use VAD${LiteGraph.insert_button("Use VAD")}</h3>
        <h3 >Change nickname${LiteGraph.insert_button("Change nickname")}</h3>
        <h3 >Manage nicknames${LiteGraph.insert_button("Manage nicknames")}</h3>
        <h3 >Manage roles${LiteGraph.insert_button("Manage roles")}</h3>
        <h3 >Manage webhooks${LiteGraph.insert_button("Manage webhooks")}</h3>
        <h3 >Manage emojis${LiteGraph.insert_button("Manage emojis")}</h3>
        </div>
        </div>`,"list")
        this.widgets_up = true;
    }
    
    //name to show
    addroleperms.title = "Role Perms";
    
    //function to call when the node is executed
    addroleperms.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).roles.cache.find(r => r.id === `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+` || r.name ===  `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+`).setPermissions(String(\`${LiteGraph.parse(this.properties["Permissions"])}\`.split(',').map(e=>e.trim())).replace(/\s/g,"_").toUpperCase().split(',').map(e=>e.trim()));`);
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Roles/Role Perms", addroleperms );

    function setserverdata()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Field", "field name ex: xp");
        this.addHTML("<style>#h::after {content: \" \";position: absolute;top: 50%;right: 100%;margin-top: -7px;border-width: 7px;border-style: solid;border-color: transparent rgba(0,0,0,0.9) transparent transparent;}</style><div class='property'><span class='property_name'>Value</span><span class='property_value' contenteditable='plaintext-only' onblur=\"document.getElementById('h').style.display='none'\" onfocus=\"document.getElementById('h').style.display='block'\" id='Value'></span><h id='h' style='display:none;border:1px solid black;font-size:15px;font-family:Arial;background-color:rgba(0,0,0,0.5);padding:7px;border-radius:4px;margin-top:-10%;position:absolute;left:101%;width:fit-content;color:#cccccc;backdrop-filter: blur(5px);'><i class=\"fa fa-info-circle\" style=\"font-size:18px;color:#cccccc;background:rgba(0,0,0,0.5);\"></i> If the value should be an array then put them between [ ]</h></div>")
        this.widgets_up = true;
        this.size = [145,27];
    }
    
    //name to show
    setserverdata.title = "Set Server Data";
    
    //function to call when the node is executed
    setserverdata.prototype.onAction = function()
    {
         
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
               
                   
                    appendFileSync(this.getInputData(0),`\nawait client.userdata.set(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"`"+`,\`${(this.actions["Value"])}\`);`);
       
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Server Action/Set Server Data", setserverdata );



    function addserverdata()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Field", "field name ex: xp");
        this.addProperty("Method", "<option selected='selected'>Add</option><option>Subtract</option><option>Multiply</option><option>Divide</option><option>Equal</option><option>Modulus</option>","dropdown");
        this.addProperty("Value", "");
        this.widgets_up = true;
        this.size = [150,27];
    }
    
    //name to show
    addserverdata.title = "Edit Server Data";
    
    //function to call when the node is executed
    addserverdata.prototype.onAction = function()
    {
       
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
                if(LiteGraph.parse_dropdown(this.properties["Method"]) == "Add"){
                    appendFileSync(this.getInputData(0),`\nawait client.userdata.add(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"`"+`,${this.properties["Value"].replaceAll('${',"").replaceAll("}","")});`);
                }
                if(LiteGraph.parse_dropdown(this.properties["Method"]) == "Subtract"){
                    appendFileSync(this.getInputData(0),`\nawait client.userdata.subtract(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"`"+`,${this.properties["Value"].replaceAll('${',"").replaceAll("}","")});`);
                }
                if(LiteGraph.parse_dropdown(this.properties["Method"]) == "Multiply"){
                    appendFileSync(this.getInputData(0),`\nawait client.userdata.multiply(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"`"+`,${this.properties["Value"].replaceAll('${',"").replaceAll("}","")});`);
                }
                if(LiteGraph.parse_dropdown(this.properties["Method"]) == "Divide"){
                    appendFileSync(this.getInputData(0),`\nawait client.userdata.divide(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"`"+`,${this.properties["Value"].replaceAll('${',"").replaceAll("}","")});`);
                }
                if(LiteGraph.parse_dropdown(this.properties["Method"]) == "Equal"){
                    appendFileSync(this.getInputData(0),`\nawait client.userdata.set(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"`"+`,${this.properties["Value"].replaceAll('${',"").replaceAll("}","")});`);
                }
                if(LiteGraph.parse_dropdown(this.properties["Method"]) == "Modulus"){
                    appendFileSync(this.getInputData(0),`\nawait client.userdata.modulus(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"`"+`,${this.properties["Value"].replaceAll('${',"").replaceAll("}","")});`);
                }
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Server Action/Edit Server Data", addserverdata );



    function getserverdata()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Field", "field name ex: xp");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.size = [155,27]
    }
    
    //name to show
    getserverdata.title = "Get Server Data";
    
    //function to call when the node is executed
    getserverdata.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = await client.parseString(client.userdata.fetch(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"`"+`));`);
        LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'User Data')
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Server Action/Get Server Data", getserverdata );


    function ifserverdata()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("true", LiteGraph.EVENT);
        this.addOutput("false", LiteGraph.EVENT);
        this.addProperty("Guild", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Field", "field name ex: xp");
        this.addHTML("<div class='property'><span class='property_name'>Method</span><select class='property_value' id='Method'><option value='>'>Greater</option><option value='>='>Greater Or Equal</option><option selected='selected' value='=='>Equal</option><option value='!='>Not Equal</option><option value='<='>Less Or Equal</option><option value='<'>Less</option></select></div>");
        this.addProperty("Value", "");
        this.widgets_up = true;
    }
    
    //name to show
    ifserverdata.title = "If Server Data";
    
    //function to call when the node is executed
    ifserverdata.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  
            this.setOutputData(1, this.getInputData(0)); 


  
            if(this.getOutputData(0)){
            appendFileSync(this.getInputData(0),`\nif(await client.userdata.fetch(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"`"+`) ${this.actions["Method"]} (client.parseString(\`${LiteGraph.parse(this.properties["Value"])}\`))){`);
            this.trigger(`true`);
            appendFileSync(this.getInputData(0),`\n}`);
            appendFileSync(this.getInputData(0),`else{`); 
            this.trigger(`false`);
            appendFileSync(this.getInputData(0),`\n}`);
        }
    }
    
    //register in the system
    LiteGraph.registerNodeType("Server Action/If Server Data", ifserverdata );



    function readcsv()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("found", LiteGraph.EVENT);
        this.addOutput("not found", LiteGraph.EVENT);
        this.addProperty("File", "spreadsheet's path");
        this.addProperty("Header", "");
        this.addProperty("Row", "");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    readcsv.title = "Get Row";
    
    //function to call when the node is executed
    readcsv.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  
            this.setOutputData(1, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`\n
        
            await require('papaparse').parse(`+"require(`fs`).readFileSync(`"+LiteGraph.parse(this.properties["File"]).replaceAll('.csv','')+".csv`).toString()"+`,{
            header:true,
            worker:true,
            complete: async function(results, file){
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = await results.data.filter(obj => obj[\`${LiteGraph.parse(this.properties["Header"])}\`] === \`${LiteGraph.parse(this.properties["Row"])}\`)[0];
                if(await results.data.filter(obj => obj[\`${LiteGraph.parse(this.properties["Header"])}\`] === \`${LiteGraph.parse(this.properties["Row"])}\`).length > 0){

`)
            }
            LiteGraph.save_var(`${this.properties["Save to variable"]}['Header']`,`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Spreadsheet data')    
            this.trigger("found");
            appendFileSync(this.getInputData(0),`}else{`)
            this.trigger("not found")
            appendFileSync(this.getInputData(0),` \n}
        }})`)
    }
    
    //register in the system
   // LiteGraph.registerNodeType("Spreadsheets/Get Row", readcsv );


    function readcsvweb()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("found", LiteGraph.EVENT);
        this.addOutput("not found", LiteGraph.EVENT);
        this.addProperty("Link", "spreadsheet's path");
        this.addProperty("Header", "");
        this.addProperty("Row", "");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    readcsvweb.title = "Get Row Web";
    
    //function to call when the node is executed
    readcsvweb.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  
            this.setOutputData(1, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`\n
        
            await require('papaparse').parse(`+"await require('node-fetch')(`"+LiteGraph.parse(this.properties["Link"])+"`).then(response => response.text())"+`,{
            header:true,
            worker:true,
            complete: async function(results, file){
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}s.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = await results.data.filter(obj => obj[\`${LiteGraph.parse(this.properties["Header"])}\`] === \`${LiteGraph.parse(this.properties["Row"])}\`)[0];
                if(await results.data.filter(obj => obj[\`${LiteGraph.parse(this.properties["Header"])}\`] === \`${LiteGraph.parse(this.properties["Row"])}\`).length > 0){

`)
            }
            LiteGraph.save_var(`${this.properties["Save to variable"]}['Header']`,`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Spreadsheet data')         
            this.trigger("found");
            appendFileSync(this.getInputData(0),`}else{`)
            this.trigger("not found")
            appendFileSync(this.getInputData(0),` \n}
        }})`)
    }
    
    //register in the system
   // LiteGraph.registerNodeType("Spreadsheets/Get Row Web", readcsvweb );


    function writecsv()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("File", "spreadsheet's path");
        this.addProperty("Data", "Header,second header\nrow, second row");
        this.size = [170,27];
        this.widgets_up = true;
    }
    
    //name to show
    writecsv.title = "Write Spreadsheet";
    
    //function to call when the node is executed
    writecsv.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`\nawait require('fs').writeFileSync(`+"`"+LiteGraph.parse(this.properties["File"]).replaceAll('.csv','')+".csv`"+`,`+"`"+`${LiteGraph.parse(this.properties["Data"])}`+"`"+`)`);
            }   
            this.trigger("Action");
    }
    
    //register in the system
   // LiteGraph.registerNodeType("Spreadsheets/Write Spreadsheet", writecsv );



    function rolecolor()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Role", "role to change color to");
        this.addProperty("Color", "", "color");
        this.widgets_up = true;
    }
    
    //name to show
    rolecolor.title = "Role Color";
    
    //function to call when the node is executed
    rolecolor.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`\n await client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).roles.cache.find(r => r.id === `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+` || r.name ===  `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+`).setColor(`+"`"+LiteGraph.parse(this.properties["Color"])+"`"+`);`);
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Roles/Role Color", rolecolor );

    function rolepos()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Role", "role to change position to");
        this.addProperty("Position", "");
        this.widgets_up = true;
    }
    
    //name to show
    rolepos.title = "Role Pos";
    
    //function to call when the node is executed
    rolepos.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).roles.cache.find(r => r.id === `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+` || r.name ===  `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+`).setPosition(`+LiteGraph.parse(this.properties["Position"]).replaceAll("${","").replaceAll("}","")+`);`);
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Roles/Role Pos", rolepos );



    function rolename()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Role", "role to change name to");
        this.addProperty("Name", "");
        this.widgets_up = true;
    }
    
    //name to show
    rolename.title = "Role Name";
    
    //function to call when the node is executed
    rolename.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).roles.cache.find(r => r.id === `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+` || r.name ===  `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+`).setName(`+"`"+LiteGraph.parse(this.properties["Name"])+"`"+`);`);
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Roles/Role Name", rolename );



    function crole()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Name", "name of role");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    crole.title = "Create Role";
    
    //function to call when the node is executed
    crole.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
                if(this.properties["Save to variable"].trim()){
                    appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).roles.create({name: `+"`"+LiteGraph.parse(this.properties["Name"])+"`"+`}).then(r=>{
                        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = r;
                    });`);
                    LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Role',[{name:"id",value:"id"},{name:"name",value:"name"},{name:"guild id",value:"guild.id"}])
                }else{
                    appendFileSync(this.getInputData(0),`\n await client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).roles.create({name: `+"`"+LiteGraph.parse(this.properties["Name"])+"`"+`});`);
                }      
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Roles/Create Role", crole );


    function delrole()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("Role", "role to delete");
     
        this.widgets_up = true;
    }
    
    //name to show
    delrole.title = "Delete Role";
    
    //function to call when the node is executed
    delrole.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).roles.cache.find(r => r.id === `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+` || r.name ===  `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+`).delete();`);
    }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Roles/Delete Role", delrole );




    function hasrole()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("true", LiteGraph.EVENT);
        this.addOutput("false", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("User ID", "user to check role");
        this.addProperty("Role", "role to check");
        this.widgets_up = true;
    }
    
    //name to show
    hasrole.title = "Has Role";
    
    //function to call when the node is executed
    hasrole.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  
            this.setOutputData(1, this.getInputData(0));  


 
           
            if(this.getOutputData(0)){
        

        appendFileSync(this.getInputData(0),`\nif(`+`client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).members.cache.get(`+"`"+`${LiteGraph.parse(this.properties["User ID"])}`+"`"+`)`+`.roles.cache.some(r => r.id === `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+` || r.name === `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+`)`+`){`); 
        }
        this.trigger(`true`);
        appendFileSync(this.getInputData(0),`\n}`);
        appendFileSync(this.getInputData(0),`else{`); 
        this.trigger(`false`);
        appendFileSync(this.getInputData(0),`\n}`);
    }
    
    //register in the system
    LiteGraph.registerNodeType("User Action/Has Role", hasrole );




    function checkperms()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("true", LiteGraph.EVENT);
        this.addOutput("false", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("User ID", "user to check perms");
        this.addProperty("Value", "<option selected='selected'>Administrator</option><option>Create instant invite</option><option>Kick members</option><option>Ban members</option><option>Manage channels</option><option>Manage guild</option><option>Add reactions</option><option>View audit log</option><option>Priority speaker</option><option>Stream</option><option>View channel</option><option>Send messages</option><option>Send tts messages</option><option>Manage Messages</option><option>Embed links</option><option>Attach files</option><option>Read message history</option><option>Mention everyone</option><option>Use external emojis</option><option>View guild insights</option><option>Connect</option><option>Speak</option><option>Mute members</option><option>Deafen members</option><option>Move members</option><option>Use VAD</option><option>Change nickname</option><option>Manage nicknames</option><option>Manage roles</option><option>Manage webhooks</option><option>Manage emojis</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    checkperms.title = "Has Perms";
    
    //function to call when the node is executed
    checkperms.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  
            this.setOutputData(1, this.getInputData(0));  


 
           
            if(this.getOutputData(0)){
        

        appendFileSync(this.getInputData(0),`\nif(`+`client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).members.cache.get(`+"`"+`${LiteGraph.parse(this.properties["User ID"])}`+"`"+`)`+`.permissions.has(Discord.Permissions.FLAGS[\`${LiteGraph.parse_dropdown(this.properties["Value"])}\`.toUpperCase().trim().replace(/\\s/g,"_")])`+`){`); 
        }
        this.trigger(`true`);
        appendFileSync(this.getInputData(0),`\n}`);
        appendFileSync(this.getInputData(0),`else{`); 
        this.trigger(`false`);
        appendFileSync(this.getInputData(0),`\n}`);
    }
    
    //register in the system
    LiteGraph.registerNodeType("User Action/Has Perms", checkperms );



    function addrole()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("User ID", "user to add role to");
        this.addProperty("Role", "role to add");
        this.widgets_up = true;
    }
    
    //name to show
    addrole.title = "Add Role";
    
    //function to call when the node is executed
    addrole.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`\n await client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).members.cache.get(`+"`"+LiteGraph.parse(this.properties["User ID"])+"`"+`).roles.add(`+`client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).roles.cache.find(r => r.id === `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+` || r.name ===  `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+`)`+`);`);
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("User Action/Add Role", addrole );

    function rrole()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("User ID", "user to remove role from");
        this.addProperty("Role", "role to remove");
        this.widgets_up = true;
    }
    
    //name to show
    rrole.title = "Remove Role";
    
    //function to call when the node is executed
    rrole.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).members.cache.get(`+"`"+LiteGraph.parse(this.properties["User ID"])+"`"+`).roles.remove(`+`client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).roles.cache.find(r => r.id === `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+` || r.name ===  `+"`"+`${LiteGraph.parse(this.properties["Role"])}`+"`"+`)`+`);`);
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("User Action/Remove Role", rrole );

    function setnickname()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("User ID", "user to set nickname to");
        this.addProperty("Nickname", "nickname to set");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true;
    }
    
    //name to show
    setnickname.title = "Set Nickname";
    
    //function to call when the node is executed
    setnickname.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



           
            if(this.getOutputData(0)){
            
            
           
                appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(`+"`"+`${LiteGraph.parse(this.properties["Guild ID"])}`+"`"+`).members.cache.get(`+"`"+`${LiteGraph.parse(this.properties["User ID"])}`+"`"+`).setNickname(`+"`"+`${LiteGraph.parse(this.properties["Nickname"])}`+"`"+`)`);
        }
            this.trigger("Action");
 
    }
    
    //register in the system
    LiteGraph.registerNodeType("User Action/Set Nickname", setnickname );

    function setuserdata()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild", "${dbwVars.CommandGuild?.id}");
        this.addProperty("User ID", "user to set data to");
        this.addProperty("Field", "field name ex: xp");
        this.addHTML("<style>#h::after {content: \" \";position: absolute;top: 50%;right: 100%;margin-top: -7px;border-width: 7px;border-style: solid;border-color: transparent rgba(0,0,0,0.9) transparent transparent;}</style><div class='property'><span class='property_name'>Value</span><span class='property_value' contenteditable='plaintext-only' onblur=\"document.getElementById('h').style.display='none'\" onfocus=\"document.getElementById('h').style.display='block'\" id='Value'></span><h id='h' style='display:none;border:1px solid black;font-size:15px;font-family:Arial;background-color:rgba(0,0,0,0.5);padding:7px;border-radius:4px;margin-top:-10%;position:absolute;left:101%;width:fit-content;color:#cccccc;backdrop-filter: blur(5px);'><i class=\"fa fa-info-circle\" style=\"font-size:18px;color:#cccccc;background:rgba(0,0,0,0.5);\"></i> If the value should be an array then put them between [ ]</h></div>")
        this.widgets_up = true;
    }
    
    //name to show
    setuserdata.title = "Set User Data";
    
    //function to call when the node is executed
    setuserdata.prototype.onAction = function()
    {
         
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
                appendFileSync(this.getInputData(0),`\nawait client.userdata.set(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"_"+LiteGraph.parse(this.properties["User ID"])+"`"+`,\`${(this.actions["Value"])}\`);`);   
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("User Action/Set User Data", setuserdata );



    function adduserdata()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild", "${dbwVars.CommandGuild?.id}");
        this.addProperty("User ID", "user to add data to");
        this.addProperty("Field", "field name ex: xp");
        this.addProperty("Method", "<option selected='selected'>Add</option><option>Subtract</option><option>Multiply</option><option>Divide</option><option>Equal</option><option>Modulus</option>","dropdown");
        this.addProperty("Value", "");
        this.widgets_up = true;
    }
    
    //name to show
    adduserdata.title = "Edit User Data";
    
    //function to call when the node is executed
    adduserdata.prototype.onAction = function()
    {
       
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
                if(LiteGraph.parse_dropdown(this.properties["Method"])=="Add"){
                    appendFileSync(this.getInputData(0),`\nawait client.userdata.add(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"_"+LiteGraph.parse(this.properties["User ID"])+"`"+`,${this.properties["Value"].replaceAll('${',"").replaceAll("}","")});`);
                }
                if(LiteGraph.parse_dropdown(this.properties["Method"])=="Subtract"){
                    appendFileSync(this.getInputData(0),`\nawait client.userdata.subtract(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"_"+LiteGraph.parse(this.properties["User ID"])+"`"+`,${this.properties["Value"].replaceAll('${',"").replaceAll("}","")});`);
                }
                if(LiteGraph.parse_dropdown(this.properties["Method"])=="Multiply"){
                    appendFileSync(this.getInputData(0),`\nawait client.userdata.multiply(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"_"+LiteGraph.parse(this.properties["User ID"])+"`"+`,${this.properties["Value"].replaceAll('${',"").replaceAll("}","")});`);
                }
                if(LiteGraph.parse_dropdown(this.properties["Method"])=="Divide"){
                    appendFileSync(this.getInputData(0),`\nawait client.userdata.divide(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"_"+LiteGraph.parse(this.properties["User ID"])+"`"+`,${this.properties["Value"].replaceAll('${',"").replaceAll("}","")});`);
                }
                if(LiteGraph.parse_dropdown(this.properties["Method"])=="Equal"){
                    appendFileSync(this.getInputData(0),`\nawait client.userdata.set(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"_"+LiteGraph.parse(this.properties["User ID"])+"`"+`,${this.properties["Value"].replaceAll('${',"").replaceAll("}","")});`);
                }
                if(LiteGraph.parse_dropdown(this.properties["Method"])=="Modulus"){
                    appendFileSync(this.getInputData(0),`\nawait client.userdata.modulus(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"_"+LiteGraph.parse(this.properties["User ID"])+"`"+`,${this.properties["Value"].replaceAll('${',"").replaceAll("}","")});`);
                }
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("User Action/Edit User Data", adduserdata );

    function getuserdata()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild", "${dbwVars.CommandGuild?.id}");
        this.addProperty("User ID", "user to get data from");
        this.addProperty("Field", "field name ex: xp");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    getuserdata.title = "Get User Data";
    
    //function to call when the node is executed
    getuserdata.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = await client.parseString(client.userdata.fetch(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"_"+LiteGraph.parse(this.properties["User ID"])+"`"+`));`);
        LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'User Data')
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("User Action/Get User Data", getuserdata );


    function ifuserdata()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("true", LiteGraph.EVENT);
        this.addOutput("false", LiteGraph.EVENT);
        this.addProperty("Guild", "${dbwVars.CommandGuild?.id}");
        this.addProperty("User ID", "user to check data from");
        this.addProperty("Field", "field name ex: xp");
        this.addProperty("", "<div class='property'><span class='property_name'>Method</span><select id='Method' class='property_value'><option value='>'>Greater</option><option value='>='>Greater Or Equal</option><option selected='selected' value='=='>Equal</option><option value='!='>Not Equal</option><option value='<='>Less Or Equal</option><option value='<'>Less</option></select></div>", "html");
        this.addProperty("Value", "");
        this.widgets_up = true;
    }
    
    //name to show
    ifuserdata.title = "If User Data";
    
    //function to call when the node is executed
    ifuserdata.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  
            this.setOutputData(1, this.getInputData(0)); 


  
            if(this.getOutputData(0)){
            appendFileSync(this.getInputData(0),`\nif(await client.userdata.fetch(`+"`"+`${LiteGraph.parse(this.properties["Guild"])}_`+`${LiteGraph.parse(this.properties["Field"])}`+"_"+LiteGraph.parse(this.properties["User ID"])+"`"+`) ${(this.actions["Method"])} (client.parseString(\`${LiteGraph.parse(this.properties["Value"])}\`))){`);
            this.trigger(`true`);
            appendFileSync(this.getInputData(0),`\n}`);
            appendFileSync(this.getInputData(0),`else{`); 
            this.trigger(`false`);
            appendFileSync(this.getInputData(0),`\n}`);
        }
    }
    
    //register in the system
    LiteGraph.registerNodeType("User Action/If User Data", ifuserdata );





    function kickuser()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("User ID", "user to kick");
        this.addProperty("Reason", "");
        this.widgets_up = true;
    }
    
    //name to show
    kickuser.title = "Kick User";
    
    //function to call when the node is executed
    kickuser.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  
 


  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).members.cache.get(`+"`"+`${LiteGraph.parse(this.properties["User ID"])}`+"`"+`).kick(`+"`"+LiteGraph.parse(this.properties["Reason"])+"`"+`);`);
            }
            this.trigger("Action");
 
    }
    
    //register in the system
    LiteGraph.registerNodeType("User Action/Kick User", kickuser );


    function banuser()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("User ID", "user to ban");
        this.addProperty("Reason", "");
        this.addProperty("Days", "");
        this.widgets_up = true;
    }
    
    //name to show
    banuser.title = "Ban User";
    
    //function to call when the node is executed
    banuser.prototype.onAction = function()
    {
       
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
                if(LiteGraph.parse(this.properties["Days"])){
                    appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).members.cache.get(`+"`"+`${LiteGraph.parse(this.properties["User ID"])}`+"`"+`).ban({ days: `+"`"+LiteGraph.parse(this.properties["Days"])+"`"+`, reason: `+"`"+LiteGraph.parse(this.properties["Reason"])+"`"+`});`);
                }else{
                    appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).members.cache.get(`+"`"+`${LiteGraph.parse(this.properties["User ID"])}`+"`"+`).ban({  reason: `+"`"+LiteGraph.parse(this.properties["Reason"])+"`"+`});`);
                }
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("User Action/Ban User", banuser );

    function unbanuser()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "${dbwVars.CommandGuild?.id}");
        this.addProperty("User ID", "user to unban");
        this.widgets_up = true;
    }
    
    //name to show
    unbanuser.title = "Unban User";
    
    //function to call when the node is executed
    unbanuser.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(`+"`"+LiteGraph.parse(this.properties["Guild ID"])+"`"+`).members.unban(`+"`"+LiteGraph.parse(this.properties["User ID"])+"`"+`);`);
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("User Action/Unban User", unbanuser );




    function readjsonweb(){
        this.addProperty("Link","link to read data from");
        this.addProperty("Headers","");
        this.addProperty("Save to variable","");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    readjsonweb.title = "Read JSON API";
    readjsonweb.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = await require('node-fetch')(`+"`"+LiteGraph.parse(this.properties["Link"]) +"`"+`,{
                headers: { 'Content-Type': 'application/json',${this.properties["Headers"]}}
            }).then(response => response.json());
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")}.constructor.prototype.toString = () => {
                return require('lossless-json').stringify(${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")},client.circularReplacer())
            }`);
            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'JSON')   
        
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Utilities/Read JSON API", readjsonweb );
    function readxmlweb(){
        this.addProperty("Link","link to read data from");
        this.addProperty("Headers","");
        this.addProperty("Save to variable","");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    readxmlweb.title = "Read XML API";
    readxmlweb.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = await require('node-fetch')(`+"`"+LiteGraph.parse(this.properties["Link"]) +"`"+`,{
                headers: { 'Content-Type': 'text/xml',${this.properties["Headers"]}}
            }).then(response => response.text());
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = require("fast-xml-parser").parse( ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")},{attrNodeName:"attribute",textNodeName:"","attributeNamePrefix":"", arrayMode: "false",
            ignoreAttributes: true,
            parseAttributeValue: true},true)
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")}.constructor.prototype.toString = () => {
                return require('lossless-json').stringify(${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")},client.circularReplacer())
            }`);
            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'JSON')   
        
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Utilities/Read XML API", readxmlweb );

    function readjsonfile(){
        this.addProperty("File","file name");
        this.addProperty("Save to variable","");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    readjsonfile.title = "Read File";
    readjsonfile.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
            appendFileSync(this.getInputData(0),`
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = client.parseString(require('fs').readFileSync(`+"`"+LiteGraph.parse(this.properties["File"])+"`"+`).toString());
            if(${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")}.constructor == ({}).constructor){
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")}.constructor.prototype.toString = () => {
                    return require('lossless-json').stringify(${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")},client.circularReplacer())
                }
            }
            if(${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")}.constructor == ([]).constructor){
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")}.forEach(e=>{
                    e.constructor.prototype.toString = () => {
                        return require('lossless-json').stringify(e,client.circularReplacer())
                    }
                })
            }`);
            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Text')   
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Utilities/Read File", readjsonfile );

 function writejsonfile(){
        this.addProperty("File"," file name");
        this.addProperty("Content","\n\n\n\n\n");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    writejsonfile.title = "Write File";
    writejsonfile.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait require('fs').writeFileSync(`+"`"+LiteGraph.parse(this.properties["File"])+"`"+`, `+`\`${LiteGraph.parse(this.properties["Content"])}\``+`);`);
        
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Utilities/Write File", writejsonfile );

    function appjsonfile(){
        this.addProperty("File"," file name");
        this.addProperty("Content","\n\n\n\n\n");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    appjsonfile.title = "Append File";
    appjsonfile.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nawait require('fs').appendFileSync(`+"`"+LiteGraph.parse(this.properties["File"])+"`"+`, `+`\`${LiteGraph.parse(this.properties["Content"])}\``+")"+`);`);
        
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Utilities/Append File", appjsonfile );


    

        function wait(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addProperty("Seconds", "0");
        this.widgets_up = true; 
    }
    wait.title = "Wait";
    wait.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`\nawait new Promise(res => setTimeout(res, Number(\`${LiteGraph.parse(this.properties["Seconds"])}\`)*1000))`);
            }
            this.trigger("Action");
    }
    LiteGraph.registerNodeType("Utilities/Wait", wait );


    function customnode(){
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addProperty("Code", "\n\n\n\n\n");
        this.widgets_up = true; 
    }
    customnode.title = "Custom Node";
    customnode.prototype.onAction = function()
    {
     
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`
        
         \n${this.properties["Code"]}`);
            }
            this.trigger("Action");
    }
    LiteGraph.registerNodeType("Utilities/Custom Node", customnode );

    function checkvariable()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("true", LiteGraph.EVENT);
        this.addOutput("false", LiteGraph.EVENT);
        this.addProperty("Value", "");
        this.addProperty("", "<div class='property'><span class='property_name'>Method</span><select id='Method' class='property_value'><option value='>'>Greater</option><option value='>='>Greater Or Equal</option><option selected='selected' value='=='>Equal</option><option value='!='>Not Equal</option><option value='<='>Less Or Equal</option><option value='<'>Less</option></select></div>", "html");
        this.addProperty("Second Value", "");
        this.widgets_up = true;
    }
    
    //name to show
    checkvariable.title = "If";
    
    //function to call when the node is executed
    checkvariable.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  
            this.setOutputData(1, this.getInputData(0));  



 
           
            if(this.getOutputData(0)){
        

        appendFileSync(this.getInputData(0),`\nif((client.parseString(\`${LiteGraph.parse(this.properties["Value"])}\`)) ${this.actions["Method"]} (client.parseString(\`${LiteGraph.parse(this.properties["Second Value"])}\`))){`); 
        this.trigger(`true`);
        appendFileSync(this.getInputData(0),`\n}`);
        appendFileSync(this.getInputData(0),`else{`); 
        this.trigger(`false`);
        appendFileSync(this.getInputData(0),`\n}`);
    }
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action/If", checkvariable );


    function checkvariablee()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("true", LiteGraph.EVENT);
        this.addOutput("false", LiteGraph.EVENT);
        this.addProperty('Variable',"");
        this.widgets_up = true;
    }
    
    //name to show
    checkvariablee.title = "If Exists";
    
    //function to call when the node is executed
    checkvariablee.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  
            this.setOutputData(1, this.getInputData(0));  



 
           
            if(this.getOutputData(0)){
        

        appendFileSync(this.getInputData(0),`\nif(${this.properties["Variable"].replaceAll('${','').replaceAll('}','')}){`); 
        this.trigger(`true`);
        appendFileSync(this.getInputData(0),`\n}`);
        appendFileSync(this.getInputData(0),`else{`); 
        this.trigger(`false`);
        appendFileSync(this.getInputData(0),`\n}`);
    }
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action/If Exists", checkvariablee );

    function switchcase()
    {
        let that = this;
        this.emojis = []
        this.addProperty("Value", "Value to check");
        this.addProperty("", "<div class='property'><span class='property_name'>Method</span><select id='Method' class='property_value'><option value='>'>Greater</option><option value='>='>Greater Or Equal</option><option selected='selected' value='=='>Equal</option><option value='!='>Not Equal</option><option value='<='>Less Or Equal</option><option value='<'>Less</option></select></div>", "html");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.emoji = this.addWidget("text","", "case");
        this.button = this.addWidget("button","Add Case", "", function(v){  if(!that.emojis.includes(that.emoji.value)&& that.emoji.value.trim() !=""){
            that.addOutput(that.emoji.value,LiteGraph.ACTION)
            that.emojis.push(that.emoji.value)
        }});
        this.button1 = this.addWidget("button","Delete Case", "", function(v){that.removeOutput(that.outputs.length-1);
             that.emojis.pop();});
        this.size = [210, 102];
        this.flags = { horizontal: true, render_box: false };
        this.constructor.collapsable = false;
    }
    
    //name to show
    switchcase.title = "If Case";
    
    //function to call when the node is executed
    switchcase.prototype.onAction = function()
    {
        for(let i =0; i<this.emojis.length; i++){
        this.setOutputData(i, this.getInputData(0));  




        if(this.getOutputData(0)){
        
        

                        appendFileSync(this.getInputData(0),`\nif((client.parseString(\`${LiteGraph.parse(this.properties["Value"])}\`))${this.actions["Method"]} (client.parseString(\`${LiteGraph.parse(this.emojis[i])}\`))){`);
                        this.trigger(this.emojis[i]);
                        appendFileSync(this.getInputData(0),`\n}`);
        }
    }
   
};
LiteGraph.registerNodeType("Variable Action/If Case",switchcase );
    
    function checkdm()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("true", LiteGraph.EVENT);
        this.addOutput("false", LiteGraph.EVENT);
        this.widgets_up = true;
    }
    
    //name to show
    checkdm.title = "Is dm";
    
    //function to call when the node is executed
    checkdm.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  
            this.setOutputData(1, this.getInputData(0));  



 
           
            if(this.getOutputData(0)){
        

        appendFileSync(this.getInputData(0),`\nif(message.channel.type === 'DM'){`); 
        }
        this.trigger(`true`);
        appendFileSync(this.getInputData(0),`\n}`);
        appendFileSync(this.getInputData(0),`else{`); 
        this.trigger(`false`);
        appendFileSync(this.getInputData(0),`\n}`);
        
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action/Is dm", checkdm );

    function createvar()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Name", "name of variable");
        this.addProperty("Value", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        
        this.widgets_up = true;
    }
    
    //name to show
    createvar.title = "Create";
    
    //function to call when the node is executed
    createvar.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
                    appendFileSync(this.getInputData(0),`
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = client.parseString(\`${LiteGraph.parse(this.properties["Value"])}\`);`);
                   LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Variable')
                
            }    
        this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action/Create", createvar );



    function getpr()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    getpr.title = "Get Prefix";
    
    //function to call when the node is executed
    getpr.prototype.onAction = function()
    {
     
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`
        try{
            if(message.guild){
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.userdata.fetch(\`${LiteGraph.parse(this.properties["Guild ID"])}prefix\`) || client.prefix;
                }else{
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.userdata.fetch(\`\${message.author.id}prefix\`) || client.prefix;
                }
        }catch{
            if(message.guild){
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.userdata.fetch(\`${LiteGraph.parse(this.properties["Guild ID"])}prefix\`) || client.prefix;
                }else{
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.userdata.fetch(\`\${message.author.id}prefix\`) || client.prefix;
                }
        }`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Text') 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action/Get Prefix", getpr );


    function getrl()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Name", "save as");
        this.addProperty("Mention Number", "0");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addProperty("",`<div class="card">
        <div class="container">
        <h2 >Parameters</h2>
        <h3 >if someone types [prefix][command name] @Developers @Beta testers</h3>
        <h3 >then "@Developers" is number 0 and "@Beta testers" is parameter 1.</h3>
        <h3 style="font-weight: 600; margin-top:10px;">Note: if someone types [prefix][command name] some text @Developers @Beta testers</h3>
        <h3 >then the mention number still doesn't change(it will be 0 for @Developers and 1 for @Beta testers)</h3>
        </div>
        </div>`,"list")
        this.widgets_up = true;
    }
    
    //name to show
    getrl.title = "Get Role";
    
    //function to call when the node is executed
    getrl.prototype.onAction = function()
    {
     
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await message.mentions.roles.toJSON()[${this.properties["Mention Number"].replaceAll("${","").replaceAll("}","")}];`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Role',[{name:"id",value:"id"},{name:"name",value:"name"},{name:"guild id",value:"guild.id"}])  
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action/Get Role", getrl );




    function getuser()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Name", "save as");
        this.addProperty("Mention Number", "0");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addProperty("",`<div class="card">
        <div class="container">
        <h2 >Parameters</h2>
        <h3 >if someone types [prefix][command name] @Discord Bot Workshop[-] @STR1KE</h3>
        <h3 >then "@Discord Bot Workshop[-]" is number 0 and "@STR1KE" is parameter 1.</h3>
        <h3 style="font-weight: 600; margin-top:10px;">Note: if someone types [prefix][command name] some text @Discord Bot Workshop[-] @STR1KE</h3>
        <h3 >then the mention number still doesn't change(it will be 0 for @Discord Bot Workshop[-] and 1 for @STR1KE)</h3>
        </div>
        </div>`,"list")
        this.widgets_up = true;
    }
    
    //name to show
    getuser.title = "Get User";
    
    //function to call when the node is executed
    getuser.prototype.onAction = function()
    {
     
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await message.mentions.users.toJSON()[${this.properties["Mention Number"].replaceAll("${","").replaceAll("}","")}];`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'User',[{name:"id",value:"id"},{name:"name",value:"username"},{name:"tag",value:"tag"},{name:"discriminator",value:"discriminator"},{name:"last message id",value:"lastMessageId"}]) 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action/Get User", getuser );


    function getch()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Name", "save as");
        this.addProperty("Mention Number", "0");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addProperty("",`<div class="card">
        <div class="container">
        <h2 >Parameters</h2>
        <h3 >if someone types [prefix][command name] #general #support</h3>
        <h3 >then "#general" is number 0 and "#support" is parameter 1.</h3>
        <h3 style="font-weight: 600; margin-top:10px">Note: if someone types [prefix][command name] some text #general #support</h3>
        <h3 >then the mention number still doesn't change(it will be 0 for #general and 1 for #support)</h3>
        </div>
        </div>`,"list")
        this.widgets_up = true;
    }
    
    //name to show
    getch.title = "Get Channel";
    
    //function to call when the node is executed
    getch.prototype.onAction = function()
    {
     
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await message.mentions.channels.toJSON()[${this.properties["Mention Number"].replaceAll("${","").replaceAll("}","")}];`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Channel",[{name:"id",value:"id"},{name:"name",value:"name"},{name:"guild id",value:"guild.id"},{name:"category id",value:"parent.id"}])  
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action/Get Channel", getch );


    function getdmch()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("User ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size = [170,27]
        this.widgets_up = true;
    }
    
    //name to show
    getdmch.title = "Get DM Channel";
    
    //function to call when the node is executed
    getdmch.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        await client.users.cache.get(\`${this.properties["User ID"]}\`).createDM(true).then(dm=>{
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("global.","").replaceAll("}","")} = dm
        })`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"DM Channel",[{name:"id",value:"id"}]) 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (2)/Get DM Channel",getdmch );

    function lastmsg()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("User ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size = [170,27]
        this.widgets_up = true;
    }
    
    //name to show
    lastmsg.title = "Get Last Message";
    
    //function to call when the node is executed
    lastmsg.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.users.cache.get(\`${this.properties["User ID"]}\`).lastMessage;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Message",[{name:"id",value:"id"},{name:"author id",value:"author.id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}])
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (2)/Get Last Message",lastmsg );


    function av()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("User ID", "");
        this.addProperty("Format", "<option selected='selected'>Default</option><option>png</option><option>jpg</option><option>webp</option><option>gif</option>","dropdown");
        this.addProperty("Size", "<option>4096</option><option>2048</option><option>1024</option><option>512</option><option>256</option><option selected='selected'>128</option><option>64</option><option>32</option><option>16</option>","dropdown");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    av.title = "Get Avatar";
    
    //function to call when the node is executed
    av.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        


        if(LiteGraph.parse_dropdown(this.properties["Format"]).toLowerCase()!='default'){
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.users.cache.get(\`${this.properties["User ID"]}\`).displayAvatarURL({format:\`${LiteGraph.parse_dropdown(this.properties["Format"]).toLowerCase()}\`,size:Number(\`${LiteGraph.parse_dropdown(this.properties["Size"])}\`)});`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Avatar') 
        }else{
            appendFileSync(this.getInputData(0),`
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.users.cache.get(\`${this.properties["User ID"]}\`).displayAvatarURL({dynamic:\`true\`,size:Number(\`${LiteGraph.parse_dropdown(this.properties["Size"])}\`)});`);
            LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Avatar') 
        }
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (2)/Get Avatar",av );

    function disname()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("User ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    disname.title = "Get Name";
    
    //function to call when the node is executed
    disname.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.users.cache.get(\`${this.properties["User ID"]}\`).username;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Name') 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (2)/Get Name",disname );

    function distag()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("User ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    distag.title = "Get Tag";
    
    //function to call when the node is executed
    distag.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.users.cache.get(\`${this.properties["User ID"]}\`).tag;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Tag') 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (2)/Get Tag",distag );



    function jdate()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("User ID", "");
        this.addProperty("Guild ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    jdate.title = "Get Join Date";
    
    //function to call when the node is executed
    jdate.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).members.cache.get(\`${this.properties["User ID"]}\`).joinedAt;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Date') 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (2)/Get Join Date",jdate );

    function cdate()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("User ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.size = [147,27]
    }
    
    //name to show
    cdate.title = "Get Create Date";
    
    //function to call when the node is executed
    cdate.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.users.cache.get(\`${this.properties["User ID"]}\`).createdAt;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Date') 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (2)/Get Create Date",cdate );

    function gp()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Member ID", "");
        this.addProperty("Guild ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    gp.title = "Get Perms";
    
    //function to call when the node is executed
    gp.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).members.cache.get(\`${this.properties["Member ID"]}\`).permissions.toArray();`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Array',[{name:"size",value:"length"}]) 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (2)/Get Perms",gp );

    function gb()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Member ID", "");
        this.addProperty("Guild ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    gb.title = "Get Ban";
    
    //function to call when the node is executed
    gb.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).bans.cache.get(\`${this.properties["Member ID"]}\`);`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Ban Info',[{name:"user id",value:"user.id"},{name:"reason",value:"reason"}])  
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (2)/Get Ban",gb );

    function gbs()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    gbs.title = "Get Bans";
    
    //function to call when the node is executed
    gbs.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).bans.cache.toJSON();`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Bans') 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (2)/Get Bans",gbs );

    function groles()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    groles.title = "Get Roles";
    
    //function to call when the node is executed
    groles.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).roles.cache.toJSON();`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Array',[{name:"size",value:"length"}]) 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (2)/Get Roles",groles );

    function gg()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    gg.title = "Get Guilds";
    
    //function to call when the node is executed
    gg.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = (await client.shard.broadcastEval(async client => await client.guilds.cache.toJSON())).flat(1);`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Array',[{name:"size",value:"length"}]) 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (2)/Get Guilds",gg );

    function guems()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    guems.title = "Get Users";
    
    //function to call when the node is executed
    guems.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.users.cache.toJSON();`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Array',[{name:"size",value:"length"}]) 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (2)/Get Users",guems );

    function gmems()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    gmems.title = "Get Members";
    
    //function to call when the node is executed
    gmems.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).members.cache.toJSON();`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Array',[{name:"size",value:"length"}]) 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (2)/Get Members",gmems );



    function gch()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    gch.title = "Get Channels";
    
    //function to call when the node is executed
    gch.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.guilds.cache.get(\`${this.properties["Guild ID"]}\`).channels.cache.toJSON();`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Array',[{name:"size",value:"length"}]) 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (2)/Get Channels",gch );

  


    function gs()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size = [170,27]
        this.widgets_up = true;
    }
    
    //name to show
    gs.title = "Get System Channel";
    
    //function to call when the node is executed
    gs.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).systemChannel;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Channel",[{name:"id",value:"id"},{name:"name",value:"name"},{name:"guild id",value:"guild.id"},{name:"category id",value:"parent.id"}])  
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (3)/Get System Channel",gs );
    function guildname(){
        this.addProperty("Guild ID","");
        this.addProperty("Save to variable","");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
        this.size = [157,27]
    }
    guildname.title = "Get Guild Name";
    guildname.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll('${','').replaceAll('}','')} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).name;`);
          LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`, "Guild Name")
            }
            this.trigger("Action");

    }
    LiteGraph.registerNodeType("Variable Action (4)/Get Guild Name", guildname );
    function guildcreate(){
      this.addProperty("Guild ID","");
      this.addProperty("Save to variable","");
       this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
      this.addOutput("Action", LiteGraph.EVENT);
      this.addInput("Trigger", LiteGraph.ACTION);
      this.widgets_up = true; 
      this.size = [160,27]
  }
  guildcreate.title = "Get Guild Create";
  guildcreate.prototype.onAction = function()
  {
      
          this.setOutputData(0, this.getInputData(0));  




          if(this.getOutputData(0)){
     
     
          appendFileSync(this.getInputData(0),`
          ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll('${','').replaceAll('}','')} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).createdAt;`);
        LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`, "Date")
          }
          this.trigger("Action");

  }
  LiteGraph.registerNodeType("Variable Action (4)/Get Guild Create", guildcreate );

  function guildemojis(){
    this.addProperty("Guild ID","");
    this.addProperty("Save to variable","");
     this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
    this.addOutput("Action", LiteGraph.EVENT);
    this.addInput("Trigger", LiteGraph.ACTION);
    this.widgets_up = true; 
    this.size = [160,27]
}
guildemojis.title = "Get Guild Emojis";
guildemojis.prototype.onAction = function()
{
    
        this.setOutputData(0, this.getInputData(0));  




        if(this.getOutputData(0)){
   
   
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll('${','').replaceAll('}','')} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).emojis;`);
      LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`, "Guild Emojis")
        }
        this.trigger("Action");

}
LiteGraph.registerNodeType("Variable Action (4)/Get Guild Emojis", guildemojis );
function guildowner(){
  this.addProperty("Guild ID","");
  this.addProperty("Save to variable","");
   this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
  this.addOutput("Action", LiteGraph.EVENT);
  this.addInput("Trigger", LiteGraph.ACTION);
  this.widgets_up = true; 
}
guildowner.title = "Get Guild Owner";
guildowner.prototype.onAction = function()
{
  
      this.setOutputData(0, this.getInputData(0));  




      if(this.getOutputData(0)){
 
 
      appendFileSync(this.getInputData(0),`\nawait client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).fetchOwner().then(owner=>{
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll('${','').replaceAll('}','')} = client.users.cache.get(owner.id);
      })`);
    LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`, "User",[{name:"id",value:"id"},{name:"name",value:"username"},{name:"tag",value:"tag"},{name:"discriminator",value:"discriminator"},{name:"last message id",value:"lastMessageId"}]) 
      }
      this.trigger("Action");

}
LiteGraph.registerNodeType("Variable Action (4)/Get Guild Owner", guildowner );


function guildicon(){
  this.addProperty("Guild ID","");
  this.addProperty("", "<div class='property'><span class='property_name'>Dynamic?</span><input id='Dynamic?' type='checkbox' checked='true'></div>","html");
  this.addProperty("Save to variable","");
   this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
  this.addOutput("Action", LiteGraph.EVENT);
  this.addInput("Trigger", LiteGraph.ACTION);
  this.widgets_up = true; 
}
guildicon.title = "Get Guild Icon";
guildicon.prototype.onAction = function()
{
  
      this.setOutputData(0, this.getInputData(0));  




      if(this.getOutputData(0)){
 
 
      appendFileSync(this.getInputData(0),`
      ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll('${','').replaceAll('}','')} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).iconURL({dynamic:\`${this.actions["Dynamic?"]}\`})`);
    LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`, "Icon")
      }
      this.trigger("Action");

}
LiteGraph.registerNodeType("Variable Action (4)/Get Guild Icon", guildicon );

function guildruleschannel(){
  this.addProperty("Guild ID","");
  this.addProperty("Save to variable","");
   this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
  this.addOutput("Action", LiteGraph.EVENT);
  this.addInput("Trigger", LiteGraph.ACTION);
  this.widgets_up = true; 
  this.size = [205,27]
}
guildruleschannel.title = "Get Guild Rules Channel";
guildruleschannel.prototype.onAction = function()
{
  
      this.setOutputData(0, this.getInputData(0));  




      if(this.getOutputData(0)){
 
 
      appendFileSync(this.getInputData(0),`
      ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll('${','').replaceAll('}','')} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).rulesChannel;`);
    LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`, "Channel",[{name:"id",value:"id"},{name:"name",value:"name"},{name:"guild id",value:"guild.id"},{name:"category id",value:"parent.id"}])  
      }
      this.trigger("Action");

}
LiteGraph.registerNodeType("Variable Action (4)/Get Guild Rules Channel", guildruleschannel );

function guildverified(){
  this.addProperty("Guild ID","");
  this.addOutput("true", LiteGraph.EVENT);
  this.addOutput("false", LiteGraph.EVENT);
  this.addInput("Trigger", LiteGraph.ACTION);
  this.widgets_up = true;
}
guildverified.title = "Is Verified?";
guildverified.prototype.onAction = function()
{
  
      this.setOutputData(0, this.getInputData(0));  
      this.setOutputData(1, this.getInputData(0));  




      if(this.getOutputData(0)){
 
 
      appendFileSync(this.getInputData(0),`\nif(client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).verified){`);
      }
      this.trigger("true");
      appendFileSync(this.getInputData(0),`\n}else{`);
      this.trigger("false")
      appendFileSync(this.getInputData(0),`\n}`);
}
LiteGraph.registerNodeType("Variable Action (4)/Is Verified?", guildverified );

    function chcr()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Channel", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size = [200,27];
        this.widgets_up = true;
    }
    
    //name to show
    chcr.title = "Get Create Date Channel";
    
    //function to call when the node is executed
    chcr.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        


        
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).createdAt;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Date') 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (3)/Get Create Date Channel",chcr );


    function chmem()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild", "${dbwVars.CommandGuild?.id}");     this.addProperty("Channel", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size = [180,27];
        this.widgets_up = true;
    }
    
    //name to show
    chmem.title = "Get Channel Members";
    
    //function to call when the node is executed
    chmem.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        


        
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild"])}\`).channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).members;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Number') 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (3)/Get Channel Members",chmem );


    function chpr()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild", "${dbwVars.CommandGuild?.id}");     this.addProperty("Channel", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size = [170,27];
        this.widgets_up = true;
    }
    
    //name to show
    chpr.title = "Get Channel Parent";
    
    //function to call when the node is executed
    chpr.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        


        
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild"])}\`).channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).parent;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Channel",[{name:"id",value:"id"},{name:"name",value:"name"},{name:"guild id",value:"guild.id"},{name:"category id",value:"parent.id"}])  
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (3)/Get Channel Parent",chpr );


    function chpos()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild", "${dbwVars.CommandGuild?.id}");     this.addProperty("Channel", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size = [170,27];
        this.widgets_up = true;
    }
    
    //name to show
    chpos.title = "Get Channel Pos";
    
    //function to call when the node is executed
    chpos.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        


        
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild"])}\`).channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).position;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Number') 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (3)/Get Channel Pos",chpos );


    function chname()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Guild", "${dbwVars.CommandGuild?.id}");     this.addProperty("Channel", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size = [170,27];
        this.widgets_up = true;
    }
    
    //name to show
    chname.title = "Get Channel Name";
    
    //function to call when the node is executed
    chname.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        


        
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild"])}\`).channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).name;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Channel",[{name:"id",value:"id"},{name:"name",value:"name"},{name:"guild id",value:"guild.id"},{name:"category id",value:"parent.id"}])  
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (3)/Get Channel Name",chname );


    function chtype()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Channel", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size = [170,27];
        this.widgets_up = true;
    }
    
    //name to show
    chtype.title = "Get Channel Type";
    
    //function to call when the node is executed
    chtype.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        


        
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).type;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Channel Type') 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (3)/Get Channel Type",chtype );




    function chvc()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Member ID", "");
        this.addProperty("Guild ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size = [170,27];
        this.widgets_up = true;
    }
    
    //name to show
    chvc.title = "Get Voice Channel";
    
    //function to call when the node is executed
    chvc.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        


        
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.guilds.cache.get(\`${LiteGraph.parse(this.properties["Guild ID"])}\`).members.cache.get(\`${this.properties["Member ID"]}\`).voice.channel;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Channel",[{name:"id",value:"id"},{name:"name",value:"name"},{name:"guild id",value:"guild.id"},{name:"category id",value:"parent.id"}])  
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (3)/Get Voice Channel",chvc );




    function mc()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Channel", "");
        this.addProperty("Message ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size = [180,27];
        this.widgets_up = true;
    }
    
    //name to show
    mc.title = "Get Message Content";
    
    //function to call when the node is executed
    mc.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        


        
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).messages.cache.get(\`${LiteGraph.parse(this.properties["Message ID"])}\`).content;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Message Content') 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (3)/Get Message Content",mc );



    function mcr()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Channel", "");
        this.addProperty("Message ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size = [180,27];
        this.widgets_up = true;
    }
    
    //name to show
    mcr.title = "Get Message Create";
    
    //function to call when the node is executed
    mcr.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        


        
        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).messages.cache.get(\`${LiteGraph.parse(this.properties["Message ID"])}\`).createdAt;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Date') 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (3)/Get Message Create",mcr );



    function mer()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Channel", "");
        this.addProperty("Message ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size = [170,27];
        this.widgets_up = true;
    }
    
    //name to show
    mer.title = "Get Message Edit";
    
    //function to call when the node is executed
    mer.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        


        
        appendFileSync(this.getInputData(0),`\n${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("global.","").replaceAll("}","")} = await client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).messages.cache.get(\`${LiteGraph.parse(this.properties["Message ID"])}\`).editedAt;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Date') 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (3)/Get Message Edit",mer );


    function ma()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Channel", "");
        this.addProperty("Message ID", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size = [180,27];
        this.widgets_up = true;
    }
    
    //name to show
    ma.title = "Get Message Author";
    
    //function to call when the node is executed
    ma.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        


        
        appendFileSync(this.getInputData(0),`\n${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("global.","").replaceAll("}","")} = await client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).messages.cache.get(\`${LiteGraph.parse(this.properties["Message ID"])}\`).author;`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'User',[{name:"id",value:"id"},{name:"name",value:"username"},{name:"tag",value:"tag"},{name:"discriminator",value:"discriminator"},{name:"last message id",value:"lastMessageId"}]) 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (3)/Get Message Author",ma );


    function msgmentions()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Name", "save as");
        this.addProperty("Type", "<option selected='selected'>User</option><option>Role</option><option>Channel</option>","dropdown");
        this.addProperty("Channel", "");
        this.addProperty("Message ID", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size = [190,27];
        this.widgets_up = true;
    }
    
    //name to show
    msgmentions.title = "Get Message Mentions";
    
    //function to call when the node is executed
    msgmentions.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        


        
        appendFileSync(this.getInputData(0),`\nif(String(\`${LiteGraph.parse_dropdown(this.properties["Type"])}\`).toLowerCase() =='user'){
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).messages.cache.get(\`${LiteGraph.parse(this.properties["Message ID"])}\`).mentions.users.toJSON();
        }else if(String(\`${LiteGraph.parse_dropdown(this.properties["Type"])}\`).toLowerCase() =='role'){
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).messages.cache.get(\`${LiteGraph.parse(this.properties["Message ID"])}\`).mentions.roles.toJSON();
        }else if(String(\`${LiteGraph.parse_dropdown(this.properties["Type"])}\`).toLowerCase() =='channel'){
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).messages.cache.get(\`${LiteGraph.parse(this.properties["Message ID"])}\`).mentions.channels.toJSON();
        }`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Array',[{name:"size",value:"length"}]) 
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action (3)/Get Message Mentions",msgmentions );


    function push()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Name", "array variable");
        this.addProperty("Value", "");
        this.widgets_up = true;
    }
    
    //name to show
    push.title = "Push Array";
    
    //function to call when the node is executed
    push.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
                    appendFileSync(this.getInputData(0),`\nawait ${LiteGraph.parse(this.properties["Name"]).replaceAll('${',"").replaceAll("}","")}.push(`+"client.parseString(`"+`${LiteGraph.parse(this.properties["Value"])}`+"`)"+`);`);
            }    
        this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action/Push Array", push );



    function arraysort()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Name", "array variable");
        this.addProperty("Method", "<option selected='selected'>Ascendent</option><option>Descendent</option>","dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    arraysort.title = "Sort Array";
    
    //function to call when the node is executed
    arraysort.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
                if(LiteGraph.parse_dropdown(this.properties["Method"]).toLowerCase() == 'ascendent'){
                appendFileSync(this.getInputData(0),`\n${LiteGraph.parse(this.properties["Name"]).replaceAll("${","").replaceAll("}","")}.sort((a,b)=>a-b)`);
                }else if(LiteGraph.parse_dropdown(this.properties["Method"]).toLowerCase() == 'descendent'){
                    appendFileSync(this.getInputData(0),`\n${LiteGraph.parse(this.properties["Name"]).replaceAll("${","").replaceAll("}","")}.sort((a,b)=>b-a)`);
                }
            }    
        this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action/Sort Array", arraysort );


    function arrayremove()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Name", "array variable to remove an element from");
        this.addProperty("Element's value", "");
        this.widgets_up = true;
    }
    
    //name to show
    arrayremove.title = "Remove Array";
    
    //function to call when the node is executed
    arrayremove.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
               
                appendFileSync(this.getInputData(0),`\n${LiteGraph.parse(this.properties["Name"]).replaceAll("${","").replaceAll("}","")}.splice(${LiteGraph.parse(this.properties["Name"]).replaceAll("${","").replaceAll("}","")}.indexOf(\`${LiteGraph.parse(this.properties["Element's value"])}\`),1)`);
                
            }    
        this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action/Remove Array", arrayremove );



    function fe(){
        this.addProperty("Array","array variable");
        this.addProperty("Save to variable", "save element with name");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addOutput("end", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    fe.title = "Foreach Array";
    fe.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  
            this.setOutputData(1, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`\nfor(let i=0; i<${this.properties["Array"].replaceAll('${',"").replaceAll("}","")}.length;i++){
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"]} = await ${this.properties["Array"].replaceAll('${',"").replaceAll("}","")}[i];
                `);
                LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Array Element') 
        
            }
            this.trigger("Action");
            appendFileSync(this.getInputData(0),`\n}`);
            this.trigger("end")
    }
    LiteGraph.registerNodeType("Variable Action/Foreach Array", fe );

    function arrl(){
        this.addProperty("Array","");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addOutput("Action", LiteGraph.EVENT);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.widgets_up = true; 
    }
    arrl.title = "Get Array Size";
    arrl.prototype.onAction = function()
    {
        
            this.setOutputData(0, this.getInputData(0));  



  
            if(this.getOutputData(0)){
       
       
            appendFileSync(this.getInputData(0),`
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll('${',"").replaceAll("}","")} = await ${this.properties["Array"].replaceAll('${',"").replaceAll("}","")}.length`);
                LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Array Size') 
            }
            this.trigger("Action");
    }
    LiteGraph.registerNodeType("Variable Action/Get Array Size", arrl );


    function reactionlistener()
    {
        let that = this;
        this.emojis = []
        this.addProperty("Message ID", "message to listen to");
        this.addProperty("Channel", "message channel");
        this.addProperty("Seconds", "type 0 or infinite or leave empty if you want to listen for reaction forever");
        this.addProperty("", "<div class='property'><span class='property_name'>Add reactions?</span><input id='Add reactions?' type='checkbox' checked='true'></div>","html");
        this.addProperty("Save emoji", "reactEmoji");
        this.addProperty("Save user", "reactUser");
        this.addProperty("Save message", "reactMsg");
        this.addProperty("Save users", "reacts");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addInput("Trigger", LiteGraph.ACTION);
        this.emoji = this.addWidget("text","", "emoji name(unicode)");
        this.button = this.addWidget("button","Add Emoji", "", function(v){  if(!that.emojis.includes(that.emoji.value)&& that.emoji.value.trim() !=""){
            that.addOutput(that.emoji.value,LiteGraph.ACTION)
            that.emojis.push(that.emoji.value)
        }});
        this.button1 = this.addWidget("button","Delete Emoji", "", function(v){that.removeOutput(that.outputs.length-1);
             that.emojis.pop();});
        this.size = [210, 102];
        this.flags = { horizontal: true, render_box: false };
        this.constructor.collapsable = false;
    }
    
    //name to show
    reactionlistener.title = "Reactions Listener";
    
    //function to call when the node is executed
    reactionlistener.prototype.onAction = function()
    {
           LiteGraph.save_var(this.properties["Save emoji"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Emoji')
           LiteGraph.save_var(this.properties["Save user"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'User',[{name:"id",value:"id"},{name:"name",value:"username"},{name:"tag",value:"tag"},{name:"discriminator",value:"discriminator"},{name:"last message id",value:"lastMessageId"}]) 
           LiteGraph.save_var(this.properties["Save message"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Message",[{name:"id",value:"id"},{name:"author id",value:"author.id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}])
           LiteGraph.save_var(this.properties["Save users"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Array',[{name:"size",value:"length"}])
        for(let i =0; i<this.emojis.length; i++){
        this.setOutputData(i, this.getInputData(0));  




        if(this.getOutputData(0)){
        
        

                    if(this.properties["Seconds"].toLowerCase() == ("infinite"||"0"||"")&&`${this.actions["Add reactions?"]}`=='true'){
                        appendFileSync(this.getInputData(0),`
                       \nawait client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).messages.cache.get(`+"`"+LiteGraph.parse(this.properties["Message ID"])+"`"+`).react(\`${this.emojis[i]}\`)
                        await client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).messages.cache.get(`+"`"+LiteGraph.parse(this.properties["Message ID"])+"`"+`).createReactionCollector({filter:(reaction, user) => reaction.emoji.name == \`${this.emojis[i]}\` || reaction.emoji.id == \`${this.emojis[i]}\` && !user.bot}).on('collect',async (r,u)=>{ 
                           ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save emoji"]} = r.emoji.name; 
                           ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save user"]} = u; 
                           ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message"]} = r.message;
                           ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save users"]} = r.users.cache.toJSON();
                        `);
                    }
                    if(this.properties["Seconds"] != ("infinite"||"0"||"")&&`${this.actions["Add reactions?"]}`=='true'){
                        appendFileSync(this.getInputData(0),`
                       \nawait client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).messages.cache.get(`+"`"+LiteGraph.parse(this.properties["Message ID"])+"`"+`).react(\`${this.emojis[i]}\`)
                        await client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).messages.cache.get(`+"`"+LiteGraph.parse(this.properties["Message ID"])+"`"+`).createReactionCollector({filter:(reaction, user) => reaction.emoji.name == \`${this.emojis[i]}\` || reaction.emoji.id == \`${this.emojis[i]}\` && !user.bot, time: Number(\`${LiteGraph.parse(this.properties["Seconds"])}\`) * 1000, errors: ["time"]}).on('collect',async (r,u)=>{
                           ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save emoji"]} = r.emoji.name; 
                           ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save user"]} = u; 
                           ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message"]} = r.message;
                           ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save users"]} = r.users.cache.toJSON(); 
                         `);
                    }
                    if(this.properties["Seconds"].toLowerCase() == ("infinite"||"0"||"")&&`${this.actions["Add reactions?"]}`=='false'){
                        appendFileSync(this.getInputData(0),`
                        await client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).messages.cache.get(`+"`"+LiteGraph.parse(this.properties["Message ID"])+"`"+`).createReactionCollector({filter:(reaction, user) => reaction.emoji.name == \`${this.emojis[i]}\` || reaction.emoji.id == \`${this.emojis[i]}\` && !user.bot}).on('collect',async (r,u)=>{
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save emoji"]} = r.emoji.name; 
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save user"]} = u; 
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message"]} = r.message; 
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save users"]} = r.users.cache.toJSON(); 
                        `);
                    }
                    if(this.properties["Seconds"] != ("infinite"||"0"||"")&&`${this.actions["Add reactions?"]}`=='false'){
                        appendFileSync(this.getInputData(0),`
                        await client.channels.cache.find(ch=>ch.name==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`|| ch.id==`+"`"+`${LiteGraph.parse(this.properties["Channel"])}`+"`"+`).messages.cache.get(`+"`"+LiteGraph.parse(this.properties["Message ID"])+"`"+`).createReactionCollector({filter:(reaction, user) => reaction.emoji.name == \`${this.emojis[i]}\` || reaction.emoji.id == \`${this.emojis[i]}\` && !user.bot, time: Number(\`${LiteGraph.parse(this.properties["Seconds"])}\`) * 1000, errors: ["time"]}).on('collect',async (r,u)=>{
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save emoji"]} = r.emoji.name; 
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save user"]} = u; 
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message"]} = r.message;
                            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save users"]} = r.users.cache.toJSON(); 
                        `);
                    }
                       
                        this.trigger(this.emojis[i]);
                        appendFileSync(this.getInputData(0),` });`);
              
        
       
    }
    }
   
};
LiteGraph.registerNodeType("Reactions/Reactions Listener",reactionlistener );




    function memberban()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save guild to variable", "banguild");
        this.addProperty("Save user to variable", "banuser");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    memberban.title = "Member Ban";

    //function to call when the node is executed
    memberban.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\guildBanAdd.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\guildBanAdd.js`, `
            
           
            module.exports = async (Discord, client, ban) => {
                if(!client.servers.get(ban.guild.id)){
                    client.servers.set(ban.guild.id,{id:ban.guild.id})
                }
                let server = client.servers.get(ban.guild.id)
                let temp = {}
                let dbwVars;
                let message;
                if(client.message){
                    message = client.message;
                    if(client.dbwVars){
                        dbwVars = client.dbwVars;
                    }
                }
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save guild to variable"].replaceAll("${","").replaceAll("}","")} = ban.guild;
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save user to variable"].replaceAll("${","").replaceAll("}","")} = ban.user;
            `);
            LiteGraph.save_var(this.properties["Save user to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'User',[{name:"id",value:"id"},{name:"name",value:"username"},{name:"tag",value:"tag"},{name:"discriminator",value:"discriminator"},{name:"last message id",value:"lastMessageId"}]) 
            LiteGraph.save_var(this.properties["Save guild to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Guild',[{name:"id",value:"id"},{name:"name",value:"name"},{name:"owner id",value:"ownerId"},{name:"member count",value:"memberCount"}]) 
            }
    }
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Member Ban", memberban );

    

    

    function memberunban()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save guild to variable", "unbanguild");
        this.addProperty("Save user to variable", "unbanuser");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    memberunban.title = "Member Unban";

    //function to call when the node is executed
    memberunban.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\guildBanRemove.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\guildBanRemove.js`, `
            
           
            module.exports = async (Discord, client, ban) => {
                if(!client.servers.get(ban.guild.id)){
                    client.servers.set(ban.guild.id,{id:ban.guild.id})
                }
                let server = client.servers.get(ban.guild.id)
                let temp = {}
                let dbwVars;
                let message;
                if(client.message){
                    message = client.message;
                    if(client.dbwVars){
                        dbwVars = client.dbwVars;
                    }
                }
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save guild to variable"].replaceAll("${","").replaceAll("}","")} = ban.guild;
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save user to variable"].replaceAll("${","").replaceAll("}","")} = ban.user;
            `);
            LiteGraph.save_var(this.properties["Save user to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Member',[{name:"id",value:"id"},{name:"name",value:"username"}])  
            LiteGraph.save_var(this.properties["Save guild to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Guild',[{name:"id",value:"id"},{name:"name",value:"name"},{name:"owner id",value:"ownerId"},{name:"member count",value:"memberCount"}]) 
            }
    }
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Member Unban", memberunban );

    



    function memberleave()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save to variable", "leavemember");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    memberleave.title = "Member Leave";

    //function to call when the node is executed
    memberleave.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\memberLeave.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\memberLeave.js`, `
            
           
            module.exports = async (Discord, client, member) => {
                if(!client.servers.get(member.guild.id)){
                    client.servers.set(member.guild.id,{id:member.guild.id})
                }
                let server = client.servers.get(member.guild.id)
                let temp = {}
                let dbwVars;
                let message;
                if(client.message){
                    message = client.message;
                    if(client.dbwVars){
                        dbwVars = client.dbwVars;
                    }
                }
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = member;
            `);
            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Member',[{name:"id",value:"id"},{name:"name",value:"displayName"},{name:"tag",value:"user.tag"},{name:"discriminator",value:"user.discriminator"},{name:"guild id",value:"guild.id"},{name:"last message id",value:"lastMessageId"}]) 
            }
    }
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Member Leave", memberleave );

    function memberkick()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save to variable", "kickmember");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    memberkick.title = "Member Kick";

    //function to call when the node is executed
    memberkick.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\memberKick.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\memberKick.js`, `
            
           
            module.exports = async (Discord, client, member) => {
                if(!client.servers.get(member.guild.id)){
                    client.servers.set(member.guild.id,{id:member.guild.id})
                }
                let server = client.servers.get(member.guild.id)
                let temp = {}
                let dbwVars;
                let message;
                if(client.message){
                    message = client.message;
                    if(client.dbwVars){
                        dbwVars = client.dbwVars;
                    }
                }
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = member;
            `);
            LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Member',[{name:"id",value:"id"},{name:"name",value:"displayName"},{name:"tag",value:"user.tag"},{name:"discriminator",value:"user.discriminator"},{name:"guild id",value:"guild.id"},{name:"last message id",value:"lastMessageId"}]) 
            }
    }
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Member Kick", memberkick );



    function messageevent()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save to variable", "message");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    messageevent.title = "Message";

    //function to call when the node is executed
    messageevent.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\messageCreate.js`);
       
        if(this.getOutputData(0)){
        writeFileSync( `${LiteGraph.botpath}\\events\\messageCreate.js`, `\nconst fs = require('fs');
        
        let dbwVars = {};
        module.exports = async (Discord, client, message) => {
            if(message.author.id == client.user.id) return;
            let server
            let temp = {}
            message.author.lastMessage = message
            message.author.lastMessageId = message.id
            message.member ? message.member.lastMessage = message:undefined
            message.member ? message.member.lastMessageId = message.id:undefined
            if(message.guild){
                if(!client.servers.get(message.guild.id)){
                    client.servers.set(message.guild.id,{id:message.guild.id})
                }
                server = client.servers.get(message.guild.id)
            }else{
                if(!client.servers.get(message.channel.id)){
                    client.servers.set(message.channel.id,{id:message.channel.id})
                }
                server = client.servers.get(message.channel.id)
            }
            
    let args = null;
    if(message.guild){
      if(client.userdata.fetch(`+"`"+"${message.guild.id}prefix"+"`"+`)){
    args = message.content.slice(client.userdata.fetch(`+"`"+"${message.guild.id}prefix"+"`"+`).length).trim().split(/ +/g);
      }else{
        args = message.content.slice(client.prefix.length).trim().split(/ +/g);
      }
  }else{
    if(client.userdata.fetch(`+"`"+"${message.author.id}prefix"+"`"+`)){
    args = message.content.slice(client.userdata.fetch(`+"`"+"${message.author.id}prefix"+"`"+`).length).trim().split(/ +/g);
      }else{
        args = message.content.slice(client.prefix.length).trim().split(/ +/g);
      }
  }
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll("${","").replaceAll("}","")} = message;
            dbwVars["CommandAuthor"] = message.author;
            dbwVars["CommandChannel"] = message.channel;
            dbwVars["CommandGuild"] = message.guild;
            dbwVars["Commands"] = client.commands.toJSON();
            dbwVars["SlashCommands"] = client.slashcommands.toJSON();\ndbwVars["ContextMenus"] = client.contextmenus.toJSON();
            dbwVars["Bot"] = client.user;
            try{
                dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.guild.id}prefix\`)||client.prefix
            }catch{
                dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.channel.id}prefix\`)||client.prefix
            }
            client.message = message;
            client.dbwVars = dbwVars;
            `);
        LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Message",[{name:"id",value:"id"},{name:"author id",value:"author.id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}]);
        }
    }
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Message", messageevent );
    function msgupdate()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save old state to variable", "oldmsgstate");
        this.addProperty("Save new state to variable", "newmsgstate");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    msgupdate.title = "Message Update";

    //function to call when the node is executed
    msgupdate.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\messageUpdate.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\messageUpdate.js`, `
            
           
            module.exports = async (Discord, client, oldMessage, newMessage) => {
                let server
                let temp = {}
                if(newMessage.guild){
                    if(!client.servers.get(newMessage.guild.id)){
                        client.servers.set(newMessage.guild.id,{id:newMessage.guild.id})
                    }
                    server = client.servers.get(newMessage.guild.id)
                }else{
                    if(!client.servers.get(newMessage.channel.id)){
                        client.servers.set(newMessage.channel.id,{id:newMessage.channel.id})
                    }
                    server = client.servers.get(newMessage.channel.id)
                }
                let dbwVars = {};
                    dbwVars["CommandAuthor"] = newMessage.author;
                    dbwVars["CommandChannel"] = newMessage.channel;
                    dbwVars["CommandGuild"] = newMessage.guild;
                    dbwVars["Commands"] = client.commands.toJSON();
                    dbwVars["SlashCommands"] = client.slashcommands.toJSON();\ndbwVars["ContextMenus"] = client.contextmenus.toJSON();
                    dbwVars["Bot"] = client.user;
                    try{
                        dbwVars["Bot"].prefix = client.userdata.fetch(\`\${newMessage.guild.id}prefix\`)||client.prefix
                    }catch{
                        dbwVars["Bot"].prefix = client.userdata.fetch(\`\${newMessage.channel.id}prefix\`)||client.prefix
                    }
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save old state to variable"].replaceAll("${","").replaceAll("}","")} = oldMessage;
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save new state to variable"].replaceAll("${","").replaceAll("}","")} = newMessage;
                
            `);
            LiteGraph.save_var(this.properties["Save old state to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Message',[{name:"id",value:"id"},{name:"author id",value:"author.id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}])
            LiteGraph.save_var(this.properties["Save new state to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Message',[{name:"id",value:"id"},{name:"author id",value:"author.id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}])
            }
    }
    
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Message Update", msgupdate );


    function reactionadd()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save emoji", "reactEmoji");
        this.addProperty("Save user", "reactUser");
        this.addProperty("Save message", "reactMsg");
        this.addProperty("Save users", "reactUsers");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    reactionadd.title = "Reaction Add";

    //function to call when the node is executed
    reactionadd.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\messageReactionAdd.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\messageReactionAdd.js`, `
            
           
            module.exports = async (Discord, client, r, u) => {
                let server
                let temp = {}
                if(r.message.guild){
                    if(!client.servers.get(r.message.guild.id)){
                        client.servers.set(r.message.guild.id,{id:r.message.guild.id})
                    }
                    server = client.servers.get(r.message.guild.id)
                }else{
                    if(!client.servers.get(r.message.channel.id)){
                        client.servers.set(r.message.channel.id,{id:r.message.channel.id})
                    }
                    server = client.servers.get(r.message.channel.id)
                }
                let dbwVars;
                let message;
                if(client.message){
                    message = client.message;
                    if(client.dbwVars){
                        dbwVars = client.dbwVars;
                    }
                }
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save emoji"]} = r.emoji.name; 
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save user"]} = u;
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message"]} = r.message;
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save users"]} = r.users.cache.toJSON(); 
                
            `);
            if(this.properties["Save emoji"].trim()){
                LiteGraph.save_var(this.properties["Save emoji"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Emoji') 
            }
            if(this.properties["Save user"].trim()){
                LiteGraph.save_var(this.properties["Save user"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'User',[{name:"id",value:"id"},{name:"name",value:"username"},{name:"tag",value:"tag"},{name:"discriminator",value:"discriminator"},{name:"last message id",value:"lastMessageId"}])  
            }
            if(this.properties["Save message"].trim()){
                LiteGraph.save_var(this.properties["Save message"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Message",[{name:"id",value:"id"},{name:"author id",value:"author.id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}])
            }
            if(this.properties["Save users"].trim()){
                LiteGraph.save_var(this.properties["Save users"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Array',[{name:"size",value:"length"}]) 
            }
            }
    }
    
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Reaction Add", reactionadd );


    function reactionrem()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save emoji", "remReactEmoji");
        this.addProperty("Save user", "remReactUser");
        this.addProperty("Save message", "remReactMsg");
        this.addProperty("Save users", "remReactUsers");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.size[170,27]
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    reactionrem.title = "Reaction Remove";

    //function to call when the node is executed
    reactionrem.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\messageReactionRemove.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\messageReactionRemove.js`, `
            
           
            module.exports = async (Discord, client, reaction, user) => {
                let server
                let temp = {}
                if(reaction.message.guild){
                    if(!client.servers.get(reaction.message.guild.id)){
                        client.servers.set(reaction.message.guild.id,{id:reaction.message.guild.id})
                    }
                    server = client.servers.get(reaction.message.guild.id)
                }else{
                    if(!client.servers.get(reaction.message.channel.id)){
                        client.servers.set(reaction.message.channel.id,{id:reaction.message.channel.id})
                    }
                    server = client.servers.get(reaction.message.channel.id)
                }
                let dbwVars;
                let message;
                if(client.message){
                    message = client.message;
                    if(client.dbwVars){
                        dbwVars = client.dbwVars;
                    }
                }
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save emoji"]} = reaction.emoji.name; 
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save user"]} = user;
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message"]} = reaction.message;
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save users"]} = reaction.users.cache.toJSON(); 
                
            `);
            if(this.properties["Save emoji"].trim()){
                LiteGraph.save_var(this.properties["Save emoji"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Emoji') 
            }
            if(this.properties["Save user"].trim()){
                LiteGraph.save_var(this.properties["Save user"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'User',[{name:"id",value:"id"},{name:"name",value:"username"},{name:"tag",value:"tag"},{name:"discriminator",value:"discriminator"},{name:"last message id",value:"lastMessageId"}])  
            }
            if(this.properties["Save message"].trim()){
                LiteGraph.save_var(this.properties["Save message"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Message",[{name:"id",value:"id"},{name:"author id",value:"author.id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}])
            }
            if(this.properties["Save users"].trim()){
                LiteGraph.save_var(this.properties["Save users"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Array',[{name:"size",value:"length"}]) 
            }
            }
    }
    
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Reaction Remove", reactionrem );

   
    function speakevent()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save bool to variable", "isspeaking");
        this.addProperty("Save member to variable", "vcmember");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    speakevent.title = "Speak";

    //function to call when the node is executed
    speakevent.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\guildMemberSpeaking.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\guildMemberSpeaking.js`, `
            
           
            module.exports = async (Discord, client, member, speaking) => {
                if(!client.servers.get(member.guild.id)){
                    client.servers.set(member.guild.id,{id:member.guild.id})
                }
                let server = client.servers.get(member.guild.id)
                let temp = {}
                let dbwVars;
                let message;
                if(client.message){
                    message = client.message;
                    if(client.dbwVars){
                        dbwVars = client.dbwVars;
                    }
                }
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save member to variable"].replaceAll("${","").replaceAll("}","")} = member;
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save bool to variable"].replaceAll("${","").replaceAll("}","")} = speaking;
                
            `);
           LiteGraph.save_var(this.properties["Save member to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Member',[{name:"id",value:"id"},{name:"name",value:"displayName"},{name:"tag",value:"user.tag"},{name:"discriminator",value:"user.discriminator"},{name:"guild id",value:"guild.id"},{name:"last message id",value:"lastMessageId"}]) 
           LiteGraph.save_var(this.properties["Save bool to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Variable')
            }
    }
    
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Speak", speakevent );

    function songchanged()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save message to variable", "nextSongMessage");
        this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    songchanged.title = "Song Changed";

    //function to call when the node is executed
    songchanged.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\nextSong.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\nextSong.js`, `
            module.exports = async (Discord, client, message) => {
                let server
                let temp = {}
                message.author.lastMessage = message
                message.author.lastMessageId = message.id
                message.member ? message.member.lastMessage = message:undefined
                message.member ? message.member.lastMessageId = message.id:undefined
                if(message.guild){
                    if(!client.servers.get(message.guild.id)){
                        client.servers.set(message.guild.id,{id:message.guild.id})
                    }
                    server = client.servers.get(message.guild.id)
                }else{
                    if(!client.servers.get(message.channel.id)){
                        client.servers.set(message.channel.id,{id:message.channel.id})
                    }
                    server = client.servers.get(message.channel.id)
                }
                let dbwVars = {};
                dbwVars["CommandAuthor"] = message.author;
                dbwVars["CommandChannel"] = message.channel;
                dbwVars["CommandGuild"] = message.guild;
                dbwVars["Commands"] = client.commands.toJSON();
                dbwVars["SlashCommands"] = client.slashcommands.toJSON();\ndbwVars["ContextMenus"] = client.contextmenus.toJSON();
                dbwVars["Bot"] = client.user;
                try{
                    dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.guild.id}prefix\`)||client.prefix
                }catch{
                    dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.channel.id}prefix\`)||client.prefix
                }
                client.message = message;
                client.dbwVars = dbwVars;
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"]} = message;`);
            }
            LiteGraph.save_var(this.properties["Save message to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Message",[{name:"id",value:"id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}])
    }
    
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Song Changed", songchanged );
    function songend()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save message to variable", "endSongMessage");
        this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    songend.title = "Song End";

    //function to call when the node is executed
    songend.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\endSong.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\endSong.js`, `
            module.exports = async (Discord, client, message) => {
                let server
                let temp = {}
                message.author.lastMessage = message
                message.author.lastMessageId = message.id
                message.member ? message.member.lastMessage = message:undefined
                message.member ? message.member.lastMessageId = message.id:undefined
                if(message.guild){
                    if(!client.servers.get(message.guild.id)){
                        client.servers.set(message.guild.id,{id:message.guild.id})
                    }
                    server = client.servers.get(message.guild.id)
                }else{
                    if(!client.servers.get(message.channel.id)){
                        client.servers.set(message.channel.id,{id:message.channel.id})
                    }
                    server = client.servers.get(message.channel.id)
                }
                let dbwVars = {};
                dbwVars["CommandAuthor"] = message.author;
                dbwVars["CommandChannel"] = message.channel;
                dbwVars["CommandGuild"] = message.guild;
                dbwVars["Commands"] = client.commands.toJSON();
                dbwVars["SlashCommands"] = client.slashcommands.toJSON();\ndbwVars["ContextMenus"] = client.contextmenus.toJSON();
                dbwVars["Bot"] = client.user;
                try{
                    dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.guild.id}prefix\`)||client.prefix
                }catch{
                    dbwVars["Bot"].prefix = client.userdata.fetch(\`\${message.channel.id}prefix\`)||client.prefix
                }
                client.message = message;
                client.dbwVars = dbwVars;
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save message to variable"]} = message;`);
            }
            LiteGraph.save_var(this.properties["Save message to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,"Message",[{name:"id",value:"id"},{name:"content",value:"content"},{name:"guild id",value:"guild.id"},{name:"channel id",value:"channel.id"}])
    }
    
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Song End", songend );

    function vcupdate()
    {
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Save old state to variable", "oldvcstate");
        this.addProperty("Save new state to variable", "newvcstate");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
        this.once = 0;
    }
    //name to show
    vcupdate.title = "Voice Update";

    //function to call when the node is executed
    vcupdate.prototype.onExecute = function()
    {
        if(this.isOutputConnected(0)){
        this.setOutputData( 0, `${LiteGraph.botpath}\\events\\voiceStateUpdate.js`);
       
        if(this.getOutputData(0)){
            writeFileSync( `${LiteGraph.botpath}\\events\\voiceStateUpdate.js`, `
            
           
            module.exports = async (Discord, client, oldState, newState) => {
                if(!client.servers.get(newState.guild.id)){
                    client.servers.set(newState.guild.id,{id:newState.guild.id})
                }
                let server = client.servers.get(newState.guild.id)
                let temp = {}
                let dbwVars;
                let message;
                if(client.message){
                    message = client.message;
                    if(client.dbwVars){
                        dbwVars = client.dbwVars;
                    }
                }
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save old state to variable"].replaceAll("${","").replaceAll("}","")} = oldState;
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save new state to variable"].replaceAll("${","").replaceAll("}","")} = newState;
                
            `);
           LiteGraph.save_var(this.properties["Save old state to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Voice State', [{name:'member id', value:"member.id"},{name:"channel id", value:"channel.id"},{name:"guild id", value:"guild.id"}])
           LiteGraph.save_var(this.properties["Save new state to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Voice State', [{name:'member id', value:"member.id"},{name:"channel id", value:"channel.id"},{name:"guild id", value:"guild.id"}])
            }
    }
    
    this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Events/Voice Update", vcupdate );

    


    
    function createvariable()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Name", "save as");
        this.addProperty("Parameter", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.addProperty("",`<div class="card">
        <div class="container">
        <h2 >Parameters</h2>
        <h3 >if someone types [prefix][command name] hello everyone</h3>
        <h3 >then "hello" is parameter 0 and "everyone" is parameter 1.</h3>
        <h3 style="font-weight: 600; margin-top:10px">Note: use "all" to get everything after the command, 0,2 to take everything from the first word to the third word or 0+ to take everything after the first word</h3>
        </div>
        </div>`,"list")
        this.widgets_up = true;
    }
    
    //name to show
    createvariable.title = "Store Value";
    
    //function to call when the node is executed
    createvariable.prototype.onAction = function()
    {
         
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        

        if(this.properties["Parameter"].toLowerCase().includes(",")){
            appendFileSync(this.getInputData(0),`
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = client.parseString(await args.slice(${this.properties["Parameter"].replaceAll("${","").replaceAll("}","")}).join(" "));`);
            }else if(this.properties["Parameter"].toLowerCase().includes("+")){
                    appendFileSync(this.getInputData(0),`
                    ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = client.parseString(await args.slice(${this.properties["Parameter"].replaceAll("${","").replaceAll("}","").replaceAll("+","")},args.length).join(" "));`);
            }
        else if(this.properties["Parameter"].toLowerCase() == "all"){
                appendFileSync(this.getInputData(0),`
                ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.parseString(args.join(" "));`);
        }else{
            appendFileSync(this.getInputData(0),`
            ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await client.parseString(args[${this.properties["Parameter"].replaceAll("${","").replaceAll("}","")}]);`);
        }       
            }
            LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Store Value')
            this.trigger("Action");
    
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action/Store Value", createvariable );

    function editvariable()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Value", "variable to edit");
        this.addProperty("Method", "<option selected='selected'>Add</option><option>Subtract</option><option>Multiply</option><option>Divide</option><option>Equal</option><option>Modulus</option>","dropdown");
        this.addProperty("Second Value", "");
        this.addProperty("Save to variable", "");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    editvariable.title = "Edit Value";
    
    //function to call when the node is executed
    editvariable.prototype.onAction = function()
    {
      
            this.setOutputData(0, this.getInputData(0));  




             
            if(this.getOutputData(0)){
        


    if(LiteGraph.parse_dropdown(this.properties["Method"].replace("Add","+").replace("Subtract","-").replace("Multiply","*").replace("Divide","/").replace("Modulus","%").replace("Equal","="))!='='){
        appendFileSync(this.getInputData(0),`\n${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll('${','').replaceAll('}','')} = await client.parseString(\`${LiteGraph.parse(this.properties["Value"])}\`) ${LiteGraph.parse_dropdown(this.properties["Method"].replace("Add","+").replace("Subtract","-").replace("Multiply","*").replace("Divide","/").replace("Modulus","%").replace("Equal","="))} await client.parseString(\`${LiteGraph.parse(this.properties["Second Value"])}\`)`)
    }else{
        appendFileSync(this.getInputData(0),`\n${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${this.properties["Save to variable"].replaceAll('${','').replaceAll('}','')} = await client.parseString(\`${LiteGraph.parse(this.properties["Second Value"])}\`)`)
    }
        LiteGraph.save_var(this.properties["Save to variable"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Variable')
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action/Edit Value", editvariable );

    function randomvalue()
    {
        
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.addProperty("Min", "");
        this.addProperty("Max", "");
        this.addProperty("Name", "save as");
         this.addProperty("Variable Type", "<option selected='selected'>temp</option><option>server</option><option>global</option>", "dropdown");
        this.widgets_up = true;
    }
    
    //name to show
    randomvalue.title = "Random Value";
    
    //function to call when the node is executed
    randomvalue.prototype.onAction = function()
    {
          
            this.setOutputData(0, this.getInputData(0));  



  
           
            if(this.getOutputData(0)){
        



        appendFileSync(this.getInputData(0),`
        ${LiteGraph.parse_dropdown(this.properties["Variable Type"])}.${(this.properties["Name"]).replaceAll("${","").replaceAll("}","")} = await Math.floor(Math.random() * (Number(\`${this.properties["Max"]}\`) - Number(\`${this.properties["Min"]}\`) + 1)) + Number(\`${this.properties["Min"]}\`);`);
        LiteGraph.save_var(this.properties["Name"],`${LiteGraph.parse_dropdown(this.properties["Variable Type"])}`,'Random Value')
            }
            this.trigger("Action");
    }
    
    //register in the system
    LiteGraph.registerNodeType("Variable Action/Random Value",randomvalue );
    //register in the system
    function Sequencer() {
        let that = this;
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addInput("Trigger", LiteGraph.ACTION);
        this.addOutput("Action", LiteGraph.EVENT);
        this.button = this.addWidget("button","Add Trigger", "", function(v){that.addInput("Trigger",LiteGraph.ACTION)});
        this.button = this.addWidget("button","Delete Trigger", "", function(v){that.removeInput(that.inputs.length - 1)});
        this.size = [210, 102];
        this.flags = { horizontal: true, render_box: false };
        this.constructor.collapsable = false;
    }

    Sequencer.title = "Sequencer";
    Sequencer.prototype.onAction = async function(action, param) {
            this.setOutputData(0, this.getInputData(this.inputs.indexOf(action)));     
             this.trigger("Action");
        
    };

    LiteGraph.registerNodeType("Utilities/Sequencer", Sequencer);

})(this);

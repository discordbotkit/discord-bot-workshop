const fs = require('fs');
const winston = require("winston");
const path = require('path');
const logger = winston.createLogger({
    level: "info",
    format: winston.format.json(),
    defaultMeta: { service: "user-service" },
    transports: [
        new winston.transports.File({
            filename: path.join(__dirname+'../../..', "/errors.log"),
            level: "error",
        }),
    ],
});
module.exports = (Discord, client, message) => {
    const { prefix, token } = require('../../config.json');
    if(!message.content.startsWith(prefix) || message.author.bot) return;

    const args = message.content.slice(prefix.length).trim().split(/ +/g);
    const cmd = args.shift().toLowerCase();

    const command = client.commands.get(cmd);

    try{
        command.execute(client, message, args, Discord);
    }catch(error){
        logger.log({
            level: "error",
            message: "Command: " + "[" + message.content + "] executed by ["+message.author.tag+"] " + error.stack,
        });
    }


}